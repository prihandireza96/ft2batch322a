﻿using Microsoft.EntityFrameworkCore;
using Med322a.DataModels;
using Med322a.ViewModels;

namespace Med322a.DataAccess
{
    public class DAAkses
    {
		private readonly DB_MEDContext db;
		private VMResponse response = new VMResponse();

		public DAAkses(DB_MEDContext _db)
		{
			db = _db;
		}

		private VMMRole? GetById(long id)
		{
			return (
					from mr in db.MRoles
					where mr.Id == id && mr.IsDelete == false
					select new VMMRole
					{
						Id = mr.Id,
						Code = mr.Code,
						Name = mr.Name,

						IsDelete = mr.IsDelete,

						CreatedBy = mr.CreatedBy,
						CreatedOn = mr.CreatedOn,
						ModifiedBy = mr.ModifiedBy,
						ModifiedOn = mr.ModifiedOn,
						DeletedBy = mr.DeletedBy,
						DeletedOn = mr.DeletedOn,
					}
				).FirstOrDefault();
		}


		public VMResponse GetAll()
		{
			try 
			{
				List<VMMRole> dataAkses = (
					from mr in db.MRoles
					where mr.IsDelete == false
					select new VMMRole
					{
						Id = mr.Id,
						Code = mr.Code,
						Name = mr.Name,

						IsDelete = mr.IsDelete,

						CreatedBy = mr.CreatedBy,
						CreatedOn = mr.CreatedOn,
						ModifiedBy = mr.ModifiedBy,
						ModifiedOn = mr.ModifiedOn,
						DeletedBy = mr.DeletedBy,
						DeletedOn = mr.DeletedOn,
					}
				).ToList();
					if (dataAkses.Count < 1)
					{
						response.message = "Role table has no data!";
						response.success = false;
						return response;
					}

				response.data = dataAkses;
				response.message = "Role data successsfully fetched!";
			}
			catch(Exception ex) 
			{
				response.message = ex.Message;
				response.success = false;
			}
			return response;
		}

		public VMResponse Get(long id)
		{
			try
			{
				if (id < 1)
				{
					response.success = false;
					response.message = "No Role ID was submitted!";

					return response;
				}

				response.data = GetById(id);

				if (response.data == null)
				{
					response.message = $"Role data with ID:{id} is not exist!";
					response.success = false;
				}
				else
					response.message = $"Role data with ID:{id} is successfully fetched!";
			}
			catch (Exception ex)
			{
				response.message = ex.Message;
				response.success = false;
			}
			return response;
		}


		public VMResponse CreateUpdate(VMMRole inputrole)
		{
			try
			{

				MRole data = new MRole();

				data.Name = inputrole.Name;
				data.Code = inputrole.Code;

				//Check if its new or updated data
				if (inputrole.Id < 1)
				{

					data.CreatedBy = inputrole.CreatedBy;
					data.CreatedOn = DateTime.Now;

					db.Add(data);

					response.message = "New Role data successfully Inserted!";
				}
				else
				{
					VMMRole? akses = GetById(inputrole.Id);

					//Check if data exist
					if (akses == null)
					{
						response.success = false;
						response.message = "Role data doesn't exist!";

						return response;
					}

					else
					{
						data.Id = akses.Id;

						data.CreatedBy = akses.CreatedBy;
						data.CreatedOn = akses.CreatedOn;

						data.ModifiedBy = inputrole.ModifiedBy;
						data.ModifiedOn = DateTime.Now;

						db.Update(data);
						response.message = "Role Data Successfully Updated!";
					}
				}

				db.SaveChanges();

				response.data = data;
			}
			catch (Exception ex)
			{
				response.message = ex.Message;
				response.success = false;
			}
			return response;
		}


		public VMResponse Delete(long id, long userId)
		{
			try
			{
				if (id < 1)
				{
					response.success = false;
					response.message = "No Role ID was submitted!";
					return response;
				}

				MRole data = new MRole();
				VMMRole? role = GetById(id);


				data.Id = role.Id;
				data.Name = role.Name;
				data.Code = role.Code;

				//proses soft-delete
				data.IsDelete = true;
				data.DeletedBy = userId;
				data.DeletedOn = DateTime.Now;

				data.CreatedBy = role.CreatedBy;
				data.CreatedOn = role.CreatedOn;
				data.ModifiedBy = role.ModifiedBy;
				data.ModifiedOn = role.ModifiedOn;

				db.Update(data);
				db.SaveChanges();

				response.data = data;
				response.message = "Role data successfully Deleted!";
			}
			catch (Exception ex)
			{
				response.message = ex.Message;
				response.success = false;
			}
			return response;
		}


		public VMResponse GetByFilter(string filter)
		{
			try
			{
				List<VMMRole> role = (
			   from mr in db.MRoles
			   where mr.IsDelete == false &&
			   mr.Name.Contains(filter)
			   select new VMMRole
			   {
				   Id = mr.Id,
				   Name = mr.Name,
				   Code = mr.Code,
				   
				   IsDelete = mr.IsDelete,

				   CreatedBy = mr.CreatedBy,
				   CreatedOn = mr.CreatedOn,
				   ModifiedBy = mr.ModifiedBy,
				   ModifiedOn = mr.ModifiedOn,
			   }
			   ).ToList();

				response.message = (role.Count > 0)
					? "Role data successfully fetched!"
					: "No Role found!";

				response.success = (role.Count > 0) ? true : false;
				response.data = role;
			}
			catch (Exception ex)
			{
				response.success = false;
				response.message = "Failed to fetched Role data!" + ex.Message;
			}
			return response;
		}

	}
}