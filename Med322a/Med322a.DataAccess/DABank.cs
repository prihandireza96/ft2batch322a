﻿using Med322a.DataModels;
using Med322a.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Med322a.DataAccess
{
	public class DABank
	{
		private readonly DB_MEDContext db;
		private VMResponse response = new VMResponse();
		public DABank(DB_MEDContext _db) { db = _db; }

		private VMMBank? GetByID(long id)
		{
			return (
				from B in db.MBanks
				where B.IsDelete == false
					&& B.Id == id
				select new VMMBank
				{
					Id = B.Id,
					Name = B.Name,
					VaCode = B.VaCode,

					CreatedBy = B.CreatedBy,
					CreatedOn = B.CreatedOn,
					ModifiedBy = B.ModifiedBy,
					ModifiedOn = B.ModifiedOn,
					DeletedBy = B.DeletedBy,
					DeletedOn = B.DeletedOn,

					IsDelete = B.IsDelete
				}).FirstOrDefault();
		}
		public VMResponse GetAll()
		{
			try
			{
				List<VMMBank> data = (
					from B in db.MBanks
						//join U in db.MUsers
						//	on B.ModifiedBy equals U.Id
						//join Bd in db.MBiodata
						//	on U.BiodataId equals Bd.Id
					where B.IsDelete == false
					//	 || B.ModifiedBy == null
					select new VMMBank
					{
						Id = B.Id,
						Name = B.Name,
						VaCode = B.VaCode,

						CreatedBy = B.CreatedBy,
						CreatedOn = B.CreatedOn,
						ModifiedBy = B.ModifiedBy,
						//ModifiedName = Bd.Fullname,
						ModifiedOn = B.ModifiedOn,
						DeletedBy = B.DeletedBy,
						DeletedOn = B.DeletedOn,

						IsDelete = B.IsDelete,
					}).ToList();
				if(data.Count < 1)
				{
					response.message = "No Data Bank!";
					response.success = false;
					return response;
				}
				response.data =  data;
			}
			catch(Exception ex)
			{
				response.message = ex.Message;
				response.success = false;
			}
			return response;
		}
		public VMResponse Get(long id)
		{
			try
			{
				if(id < 1)
				{
					response.success = false;
					response.message = "No Data Bank Was Found!";
					return response;
				}
				response.data = GetByID(id);

				if(response.data == null)
				{
					response.message = $"No Data Bank With ID => {id}";
					response.success = false;
				}
				else
				{
					response.message = "Succesfully Fetched!";
				}
			}
			catch(Exception ex)
			{
				response.message = ex.Message;
				response.success = false;
			}
			return response;
		}
		public VMResponse CreateUpdate(VMMBank iBank)
		{
			try
			{
				MBank data = new MBank();

				data.Name = iBank.Name;
				data.VaCode = iBank.VaCode;

				if(iBank.Id < 1)
				{
					data.CreatedBy = iBank.CreatedBy;
					data.CreatedOn = DateTime.Now;

					db.Add(data);
					response.message = "New Bank Succesfully Inserted!";
				}
				else
				{
					VMMBank? bank = GetByID(iBank.Id);
					if(bank == null){
						response.success = false;
						response.message = "Bank Not Exist!";
						return response;
					}
					else{
						data.Id = bank.Id;
						data.Name = iBank.Name;
						data.VaCode = iBank.VaCode;

						data.CreatedBy = bank.CreatedBy;
						data.CreatedOn = bank.CreatedOn;

						data.ModifiedBy = iBank.ModifiedBy;
						data.ModifiedOn = DateTime.Now;

						db.Update(data);
						response.message = "Update Successfully!";
					}
				}
				db.SaveChanges();
				response.data = data;
			}
			catch (Exception ex) {
				response.success = false;
				response.message = ex.Message;
			}
			return response;
		}
		public VMResponse Delete(long id, int bankID)
		{
			try
			{
				if(id < 1)
				{
					response.success = false;
					response.message = "Bank Data Not EXIST!!!";
					return response;
				}

				MBank data = new MBank();
				VMMBank? bank = GetByID(id);

				if (bank == null)
				{
					response.success = false;
					response.message = "Bank Data Not EXIST!!!";
					return response;
				}				
				data.Id = bank.Id;
				data.Name = bank.Name;
				data.VaCode = bank.VaCode;

				data.CreatedBy = bank.CreatedBy;
				data.CreatedOn = bank.CreatedOn;
				data.ModifiedBy = bank.ModifiedBy;
				data.ModifiedOn = bank.ModifiedOn;

				data.DeletedBy = bankID;
				data.DeletedOn = DateTime.Now;

				data.IsDelete = true;

				db.Update(data);
				db.SaveChanges();

				response.data = data;
				response.message = "New Bank data has successfully deleted";
			}
			catch (Exception ex)
			{
				response.success = false;
				response.message = ex.Message;
			}
			return response;
		}
		public VMResponse GetByFilter(string filter)
		{
			try
			{
				List<VMMBank> data = (
					from B in db.MBanks
					where B.IsDelete == false
						&& B.Name.Contains(filter)
					select new VMMBank
					{
						Id = B.Id,
						Name = B.Name,
						VaCode = B.VaCode,

						CreatedBy = B.CreatedBy,
						CreatedOn = B.CreatedOn,
						ModifiedBy = B.ModifiedBy,
						ModifiedOn = B.ModifiedOn,
						DeletedBy = B.DeletedBy,
						DeletedOn = B.DeletedOn,

						IsDelete = B.IsDelete,
					}).ToList();
				if (data.Count >= 1)
				{
					response.data = data;
					response.message = "Successfully!";
				}
			}
			catch (Exception ex)
			{
				response.message = ex.Message;
				response.success = false;
			}
			return response;
		}
        public VMResponse GetByName(string nBank)
        {
            try
            {
                if (string.IsNullOrEmpty(nBank))
                {
                    response.success = false;
                    response.message = "No User Data was fetched";
                    return response;
                }
                VMMBank? data = (
                    from B in db.MBanks
                    where B.IsDelete == false
                        && B.Name == nBank
                    select new VMMBank
                    {
                        Id = B.Id,
                        Name = B.Name,
                        VaCode = B.VaCode,

                        CreatedBy = B.CreatedBy,
                        CreatedOn = B.CreatedOn,
                        ModifiedBy = B.ModifiedBy,
                        ModifiedOn = B.ModifiedOn,
                        DeletedBy = B.DeletedBy,
                        DeletedOn = B.DeletedOn,

                        IsDelete = B.IsDelete,
                    }).FirstOrDefault();
				response.data = data;

                if (response.data != null)
                {
                    response.message = "Successfully!";
                }
				else
				{
					response.message = $"No Bank Data With Bank Name {nBank}";
					response.success = false;
				}
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
            //throw new NotImplementedException();
        }
    }
}
