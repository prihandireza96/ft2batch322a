﻿using Med322a.DataModels;
using Med322a.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Med322a.DataAccess
{
	public class DABayar
	{
		private readonly DB_MEDContext db;
		private VMResponse response = new VMResponse();
		public DABayar(DB_MEDContext _db) { db = _db; }
		private VMMPaymentMethod? GetByID(long id)
		{
		return (
			from B in db.MPaymentMethods
			where B.IsDelete == false
				&& B.Id == id
			select new VMMPaymentMethod
			{
				Id = B.Id,
				Name = B.Name,

				CreatedBy = B.CreatedBy,
				CreatedOn = B.CreatedOn,
				ModifiedBy = B.ModifiedBy,
				ModifiedOn = B.ModifiedOn,
				DeletedBy = B.DeletedBy,
				DeletedOn = B.DeletedOn,

				IsDelete = B.IsDelete
			}).FirstOrDefault();
		}
		public VMResponse GetAll()
		{
			try
			{
				List<VMMPaymentMethod> data = (
					from B in db.MPaymentMethods
					where B.IsDelete == false
					select new VMMPaymentMethod
					{
						Id = B.Id,
						Name = B.Name,

						CreatedBy = B.CreatedBy,
						CreatedOn = B.CreatedOn,
						ModifiedBy = B.ModifiedBy,
						ModifiedOn = B.ModifiedOn,
						DeletedBy = B.DeletedBy,
						DeletedOn = B.DeletedOn,

						IsDelete = B.IsDelete,
					}).ToList();
				if (data.Count < 1)
				{
					response.message = "No Data Bank!";
					response.success = false;
					return response;
				}
				response.data = data;
			}
			catch (Exception ex)
			{
				response.message = ex.Message;
				response.success = false;
			}
			return response;
		}
		public VMResponse Get(long id)
		{
			try
			{
				if (id < 1)
				{
					response.success = false;
					response.message = "No Data Was Found!";
					return response;
				}
				response.data = GetByID(id);

				if (response.data == null)
				{
					response.message = $"No Data Payment With ID => {id}";
					response.success = false;
				}
				else
				{
					response.message = "Succesfully Fetched!";
				}
			}
			catch (Exception ex)
			{
				response.message = ex.Message;
				response.success = false;
			}
			return response;
		}
		public VMResponse CreateUpdate(VMMPaymentMethod bayar)
		{
			try
			{
				MPaymentMethod data = new MPaymentMethod();

				data.Name = bayar.Name;

				if (bayar.Id < 1)
				{
					data.CreatedBy = bayar.CreatedBy;
					data.CreatedOn = DateTime.Now;

					db.Add(data);
					response.message = "New Payment Method Succesfully Inserted!";
				}
				else
				{
					VMMPaymentMethod? pay = GetByID(bayar.Id);
					if (pay == null)
					{
						response.success = false;
						response.message = "Payment Method Not Exist!";
						return response;
					}
					else
					{
						data.Id = pay.Id;
						data.Name = bayar.Name;

						data.CreatedBy = pay.CreatedBy;
						data.CreatedOn = pay.CreatedOn;

						data.ModifiedBy = bayar.ModifiedBy;
						data.ModifiedOn = DateTime.Now;

						db.Update(data);
						response.message = "Update Successfully!";
					}
				}
				db.SaveChanges();
				response.data = data;
			}
			catch (Exception ex)
			{
				response.success = false;
				response.message = ex.Message;
			}
			return response;
		}
		public VMResponse Delete(long id, int DelID)
		{
			try
			{
				if (id < 1)
				{
					response.success = false;
					response.message = "Payment Data Not EXIST!!!";
					return response;
				}

				MPaymentMethod data = new MPaymentMethod();
				VMMPaymentMethod? iPay = GetByID(id);

				if (iPay == null)
				{
					response.success = false;
					response.message = "Payment Method Not EXIST!!!";
					return response;
				}
				data.Id = iPay.Id;
				data.Name = iPay.Name;;

				data.CreatedBy = iPay.CreatedBy;
				data.CreatedOn = iPay.CreatedOn;
				data.ModifiedBy = iPay.ModifiedBy;
				data.ModifiedOn = iPay.ModifiedOn;

				data.DeletedBy = DelID;
				data.DeletedOn = DateTime.Now;

				data.IsDelete = true;

				db.Update(data);
				db.SaveChanges();

				response.data = data;
				response.message = "New Payment Method has successfully deleted";
			}
			catch (Exception ex)
			{
				response.success = false;
				response.message = ex.Message;
			}
			return response;
		}

        public VMResponse GetByFilter(string filter)
        {
            try
            {
                    List<VMMPaymentMethod> data = (
                        from P in db.MPaymentMethods
                        where P.IsDelete == false
                            && P.Name.Contains(filter)
                        select new VMMPaymentMethod
                        {
                            Id = P.Id,
                            Name = P.Name,

                            CreatedBy = P.CreatedBy,
                            CreatedOn = P.CreatedOn,
                            ModifiedBy = P.ModifiedBy,
                            ModifiedOn = P.ModifiedOn,
                            DeletedBy = P.DeletedBy,
                            DeletedOn = P.DeletedOn,

                            IsDelete = P.IsDelete,
                        }).ToList();
				if(data.Count >= 1)
				{       
					response.data = data;
                    response.message = "Successfully!";
				}
                else
                {
                    response.message = $"No Bank Data With Bank Name {filter}";
                    response.success = false;
                }
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }
        public VMResponse GetName(string filter)
        {
            try
            {
                if (string.IsNullOrEmpty(filter))
                {
                    response.success = false;
                    response.message = "No Data Was Fetch !!!";
                    return response;
                }
                else
                {
                    VMMPaymentMethod? data = (
                        from P in db.MPaymentMethods
                        where P.IsDelete == false
                            && P.Name.Contains(filter)
                        select new VMMPaymentMethod
                        {
                            Id = P.Id,
                            Name = P.Name,

                            CreatedBy = P.CreatedBy,
                            CreatedOn = P.CreatedOn,
                            ModifiedBy = P.ModifiedBy,
                            ModifiedOn = P.ModifiedOn,
                            DeletedBy = P.DeletedBy,
                            DeletedOn = P.DeletedOn,

                            IsDelete = P.IsDelete,
                        }).FirstOrDefault();
                    response.data = data;
                    if (response.data != null)
                    {
                        response.message = "Successfully!";
                    }
                    else
                    {
                        response.message = $"No Bank Data With Bank Name {filter}";
                        response.success = false;
                    }
                }
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }
    }
}
