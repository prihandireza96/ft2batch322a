﻿using Med322a.DataModels;
using Med322a.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Med322a.DataAccess
{
    public class DABuatJanji
    {
        private readonly DB_MEDContext db;
        private VMResponse response = new VMResponse();

        public DABuatJanji(DB_MEDContext _db) { db = _db; }

        public VMResponse GetAll()
        {
            try {
                List<VMBuatJanji?> data = (
                    from ta in db.TAppointments
                    join tdo in db.TDoctorOffices
                    on ta.Id equals tdo.DoctorId
                    where ta.IsDelete == false
                    select new VMBuatJanji
                    {
                        Id = ta.Id,
                        CustomerId = ta.CustomerId,
                        DoctorOfficeId = ta.DoctorOfficeId,
                        DoctorOfficeScheduleId = ta.DoctorOfficeScheduleId,
                        DoctorOfficeTreatmentId = ta.DoctorOfficeTreatmentId,
                        AppointmentDate = ta.AppointmentDate,

                        MedicalFacilityId = tdo.MedicalFacilityId,


                        CreatedBy = ta.CreatedBy,
                        CreatedOn  = ta.CreatedOn,
                        ModifiedBy = ta.ModifiedBy,
                        ModifiedOn = ta.ModifiedOn,
                        DeletedBy = ta.DeletedBy,
                        DeletedOn = ta.DeletedOn
                    }).ToList();

                if (data.Count < 0 ) 
                {
                    response.message = "Belum ada Janji yang dibuat!";
                    response.success = false;
                    return response;
                }

                response.data = data;
                response.message = "Janji data successsfully fetched!";
            }
            catch(Exception ex) 
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }

        public VMResponse Get(long id)
        {
            try
            {
                if (id < 1)
                {
                    response.success = false;
                    response.message = "Janji ID was submitted!";

                    return response;
                }

                response.data = GetById(id);

                if (response.data == null)
                {
                    response.message = $"Janji data with ID:{id} is not exist!";
                    response.success = false;
                }
                else
                    response.message = $"Janji data with ID:{id} is successfully fetched!";
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }

        private VMBuatJanji? GetById(long id)
        {
            return (
                     from ta in db.TAppointments
                     where ta.Id == id && ta.IsDelete == false
                     select new VMBuatJanji
                     {
                         Id = ta.Id,
                         CustomerId = ta.CustomerId,
                         DoctorOfficeId = ta.DoctorOfficeId,
                         DoctorOfficeScheduleId = ta.DoctorOfficeScheduleId,
                         DoctorOfficeTreatmentId = ta.DoctorOfficeTreatmentId,
                         AppointmentDate = ta.AppointmentDate,

                         CreatedBy = ta.CreatedBy,
                         CreatedOn = ta.CreatedOn,
                         ModifiedBy = ta.ModifiedBy,
                         ModifiedOn = ta.ModifiedOn,
                         DeletedBy = ta.DeletedBy,
                         DeletedOn = ta.DeletedOn
                     }
                ).FirstOrDefault();
        }

        //public VMResponse CreateUpdate(VMBuatJanji inputjanji)
        //{
        //    try
        //    {

        //        TAppointment data = new TAppointment();

        //        data.CustomerId = inputjanji.CustomerId;
        //        data.Code = inputrole.Code;

        //        //Check if its new or updated data
        //        if (inputrole.Id < 1)
        //        {

        //            data.CreatedBy = inputrole.CreatedBy;
        //            data.CreatedOn = DateTime.Now;

        //            db.Add(data);

        //            response.message = "New Role data successfully Inserted!";
        //        }
        //        else
        //        {
        //            VMMRole? akses = GetById(inputrole.Id);

        //            //Check if data exist
        //            if (akses == null)
        //            {
        //                response.success = false;
        //                response.message = "Role data doesn't exist!";

        //                return response;
        //            }

        //            else
        //            {
        //                data.Id = akses.Id;

        //                data.CreatedBy = akses.CreatedBy;
        //                data.CreatedOn = akses.CreatedOn;

        //                data.ModifiedBy = inputrole.ModifiedBy;
        //                data.ModifiedOn = DateTime.Now;

        //                db.Update(data);
        //                response.message = "Role Data Successfully Updated!";
        //            }
        //        }

        //        db.SaveChanges();

        //        response.data = data;
        //    }
        //    catch (Exception ex)
        //    {
        //        response.message = ex.Message;
        //        response.success = false;
        //    }
        //    return response;
        //}
    }
}
