﻿using Med322a.DataModels;
using Med322a.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Med322a.DataAccess
{
    public class DACancelAppointment
    {
        private readonly DB_MEDContext db;
        private VMResponse response = new VMResponse();

        public DACancelAppointment(DB_MEDContext db)
        {
            this.db = db;
        }

        public VMResponse? GetAll(int id)
        {
            try
            {
                List<VMCancelAppointment> data = (
                from ap in db.DMCancelAppointments.FromSqlRaw($"select * from vw_appointment where userid={id} and IsDelete=0").ToList()
                select new VMCancelAppointment
                {
                    userid = ap.userid,
                    parentBioId = ap.parentBioId,
                    biodataId = ap.biodataId,
                    customerId = ap.customerId,
                    Fullname = ap.Fullname,
                    Dob = ap.Dob,
                    Gender = ap.Gender,
                    AppointmentId = ap.appointment_id,
                    DoctorofficeId = ap.doctor_office_id,
                    DoctorId = ap.doctor_id,
                    DoctorName = ap.doctor_name,
                    DoctorSpec = ap.doctor_spec,
                    DoctorSchedule = ap.doctor_schedule,
                    doctortreatment = ap.doctortreatment,
                    slot = ap.slot,
                    MedicFacility = ap.medic_facility,
                    AppDate = ap.app_date,
                    IdxDay = ap.idx_day,
                    DoctortreatId = ap.doctor_treat_id,
                    treatment = ap.treatment,
                    CostumerRelationId = ap.CostumerRelationId,

                    //base properties
                    CreatedBy = ap.CreatedBy,
                    CreatedOn = ap.CreatedOn,
                    ModifiedBy = ap.ModifiedBy,
                    ModifiedOn = ap.ModifiedOn,
                    DeletedBy = ap.DeletedBy,
                    DeletedOn = ap.DeletedOn,
                    IsDelete = ap.IsDelete,
                }).ToList();
                response.data = (data.Count < 1) ? null : data;
                response.message = (response.data == null) ? "No Data" : "Data appointment berhasil diterima";
            }
            catch(Exception ex)
            {
                response.data = null;
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }

		public VMResponse GetByFilter(string filter, int Id)
		{
			try
			{
				List<VMCancelAppointment> data = (
					from pmd in db.DMCancelAppointments.FromSqlRaw($"select * from vw_appointment where IsDelete = 0").ToList()
					where pmd.userid == Id && (pmd.Fullname + pmd.doctor_name).ToLower().Contains(filter.ToLower())
					select new VMCancelAppointment
					{
						userid = pmd.userid,
						parentBioId = pmd.parentBioId,
						biodataId = pmd.biodataId,
						customerId = pmd.customerId,
						Fullname = pmd.Fullname,
						Dob = pmd.Dob,
						Gender = pmd.Gender,
						AppointmentId = pmd.appointment_id,
						DoctorofficeId = pmd.doctor_office_id,
                        doctortreatment = pmd.doctortreatment,
						DoctorId = pmd.doctor_id,
						DoctorName = pmd.doctor_name,
						DoctorSpec = pmd.doctor_spec,
						DoctorSchedule = pmd.doctor_schedule,
						slot = pmd.slot,
						MedicFacility = pmd.medic_facility,
						AppDate = pmd.app_date,
						IdxDay = pmd.idx_day,
						DoctortreatId = pmd.doctor_treat_id,
						treatment = pmd.treatment,
						CostumerRelationId = pmd.CostumerRelationId,

						//base properties
						CreatedBy = pmd.CreatedBy,
						CreatedOn = pmd.CreatedOn,
						ModifiedBy = pmd.ModifiedBy,
						ModifiedOn = pmd.ModifiedOn,
						DeletedBy = pmd.DeletedBy,
						DeletedOn = pmd.DeletedOn,
						IsDelete = pmd.IsDelete,
					}).ToList();

				response.success = true;
				response.data = data;
				return response;
			}
			catch (Exception ex)
			{
				response.message = ex.Message;
				response.success = false;
				response.data = null;
				return response;
			}
		}

        public VMResponse GetAppointmentId(int id)
        {
            try
            {
                VMCancelAppointment data = (
                from ca in db.DMCancelAppointments.FromSqlRaw($"select * from vw_appointment where appointment_id={id} and IsDelete=0").ToList()
                where ca.appointment_id == id
                select new VMCancelAppointment
                {
                    userid = ca.userid,
                    parentBioId = ca.parentBioId,
                    biodataId = ca.biodataId,
                    customerId = ca.customerId,
                    Fullname = ca.Fullname,
                    Dob = ca.Dob,
                    Gender = ca.Gender,
                    AppointmentId = ca.appointment_id,
                    DoctorofficeId = ca.doctor_office_id,
                    doctortreatment = ca.doctortreatment,
                    DoctorId = ca.doctor_id,
                    DoctorName = ca.doctor_name,
                    DoctorSpec = ca.doctor_spec,
                    DoctorSchedule = ca.doctor_schedule,
                    slot = ca.slot,
                    MedicFacility = ca.medic_facility,
                    AppDate = ca.app_date,
                    IdxDay = ca.idx_day,
                    DoctortreatId = ca.doctor_treat_id,
                    treatment = ca.treatment,
                    CostumerRelationId = ca.CostumerRelationId,

                    //base properties
                    CreatedBy = ca.CreatedBy,
                    CreatedOn = ca.CreatedOn,
                    ModifiedBy = ca.ModifiedBy,
                    ModifiedOn = ca.ModifiedOn,
                    DeletedBy = ca.DeletedBy,
                    DeletedOn = ca.DeletedOn,
                    IsDelete = ca.IsDelete,
                }).FirstOrDefault();

                response.data = data;
                response.message = "data successfully fetched!";
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }

        private VMCancelAppointment GetAppointmentId2(int id)
        {
            return (
                from ca in db.DMCancelAppointments.FromSqlRaw($"select * from vw_appointment where appointment_id={id} and IsDelete=0").ToList()
                where ca.appointment_id == id
                select new VMCancelAppointment
                {
                    userid = ca.userid,
                    parentBioId = ca.parentBioId,
                    biodataId = ca.biodataId,
                    customerId = ca.customerId,
                    Fullname = ca.Fullname,
                    Dob = ca.Dob,
                    Gender = ca.Gender,
                    AppointmentId = ca.appointment_id,
                    DoctorofficeId = ca.doctor_office_id,
                    doctortreatment = ca.doctortreatment,
                    DoctorId = ca.doctor_id,
                    DoctorName = ca.doctor_name,
                    DoctorSpec = ca.doctor_spec,
                    DoctorSchedule = ca.doctor_schedule,
                    slot = ca.slot,
                    MedicFacility = ca.medic_facility,
                    AppDate = ca.app_date,
                    IdxDay = ca.idx_day,
                    DoctortreatId = ca.doctor_treat_id,
                    treatment = ca.treatment,
                    CostumerRelationId = ca.CostumerRelationId,

                    //base properties
                    CreatedBy = ca.CreatedBy,
                    CreatedOn = ca.CreatedOn,
                    ModifiedBy = ca.ModifiedBy,
                    ModifiedOn = ca.ModifiedOn,
                    DeletedBy = ca.DeletedBy,
                    DeletedOn = ca.DeletedOn,
                    IsDelete = ca.IsDelete,
                }).FirstOrDefault();
        }

        public VMResponse Delete(int appoinid)
        {
            try
            {
                VMCancelAppointment? data = GetAppointmentId2(appoinid);

                TAppointmentCancellation customer = new TAppointmentCancellation();

                customer.AppointmentId = (int?)data.AppointmentId;

                customer.CreatedBy = data.userid;
                customer.CreatedOn = DateTime.Now;


                db.Add(customer);

                TAppointment newData = new TAppointment();

                newData.Id = data.AppointmentId;
                newData.CustomerId = data.customerId;
                newData.DoctorOfficeId = data.DoctorofficeId;
                newData.DoctorOfficeScheduleId = data.DoctorSchedule;
                newData.DoctorOfficeTreatmentId = data.DoctortreatId;
                newData.AppointmentDate = data.AppDate;

                newData.CreatedBy = (long)data.CreatedBy;
                newData.CreatedOn = data.CreatedOn;
                newData.ModifiedOn = data.ModifiedOn;
                newData.DeletedBy = data.userid;
                newData.DeletedOn = DateTime.Now;
                newData.IsDelete = true;

                db.Update(newData);

                // Simpan perubahan ke database
                db.SaveChanges();

                response.message = "Patient successfully updated!";
            }
            catch (Exception)
            {

                throw;
            }

            return response;
        }

    }
}
