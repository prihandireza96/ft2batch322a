﻿using Med322a.DataModels;
using Med322a.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Med322a.DataAccess
{
    public class DACariObat
    {
        private readonly DB_MEDContext db;
        private VMResponse response = new VMResponse ();
        public DACariObat(DB_MEDContext _db) { db = _db; }
        private VMCariObat? GetByID(long id)
        {
            return (
                from MI in db.MMedicalItems
                join MIC in db.MMedicalItemCategories
                    on MI.MedicalItemCategoryId equals MIC.Id
                join MIS in db.MMedicalItemSegmentations
                    on MI.MedicalItemSegmentationId equals MIS.Id
                where MI.IsDelete == false 
                    && MI.Id == id
                select new VMCariObat
                {
                    Id = MI.Id,
                    Name = MI.Name,

                    MedicalItemCategoryId = MI.MedicalItemCategoryId,
                    MedicalItemCategoryName = MIC.Name,
                    
                    Composition = MI.Composition,

                    MedicalItemSegmentationId = MI.MedicalItemSegmentationId,
                    MedicalItemSegmentationName = MIS.Name,

                    Manufacturer = MI.Manufacturer,
                    Indication = MI.Indication,
                    Dosage = MI.Dosage,
                    Directions = MI.Directions,
                    Contraindication = MI.Contraindication,
                    Caution = MI.Caution,
                    Packaging = MI.Packaging,
                    PriceMax = MI.PriceMax,
                    PriceMin = MI.PriceMin,
                    Image = MI.Image,
                    ImagePath = MI.ImagePath,

                    CreatedBy = MI.CreatedBy,
                    CreatedOn = MI.CreatedOn,
                    ModifiedBy = MI.ModifiedBy,
                    ModifiedOn = MI.ModifiedOn,
                    DeletedBy = MI.DeletedBy,
                    DeletedOn = MI.DeletedOn,
                    IsDelete = MI.IsDelete

                }).FirstOrDefault();
        }
        public VMResponse GetByFilter(string filter)
        {
            try
            {
                List<VMCariObat?> data = (
                    from MI in db.MMedicalItems
                    join MIC in db.MMedicalItemCategories
                        on MI.MedicalItemCategoryId equals MIC.Id
                    join MIS in db.MMedicalItemSegmentations
                        on MI.MedicalItemSegmentationId equals MIS.Id
                    where MI.IsDelete == false
                        && (MI.Name + MI.Indication).Contains(filter)
                    select new VMCariObat
                    {
                        Id = MI.Id,
                        Name = MI.Name,

                        MedicalItemCategoryId = MI.MedicalItemCategoryId,
                        MedicalItemCategoryName = MIC.Name,

                        Composition = MI.Composition,

                        MedicalItemSegmentationId = MI.MedicalItemSegmentationId,
                        MedicalItemSegmentationName = MIS.Name,

                        Manufacturer = MI.Manufacturer,
                        Indication = MI.Indication,
                        Dosage = MI.Dosage,
                        Directions = MI.Directions,
                        Contraindication = MI.Contraindication,
                        Caution = MI.Caution,
                        Packaging = MI.Packaging,
                        PriceMax = MI.PriceMax,
                        PriceMin = MI.PriceMin,
                        Image = MI.Image,
                        ImagePath = MI.ImagePath,

                        CreatedBy = MI.CreatedBy,
                        CreatedOn = MI.CreatedOn,
                        ModifiedBy = MI.ModifiedBy,
                        ModifiedOn = MI.ModifiedOn,
                        DeletedBy = MI.DeletedBy,
                        DeletedOn = MI.DeletedOn,
                        IsDelete = MI.IsDelete

                    }).ToList();
                if(data.Count >= 1)
                {
                    response.data = data;
                    response.message = "Fetched Medical Item Success !!!";
                }
                else
                {
                    response.message = "Fetched Medical Item Failed !!!";
                    response.success = false;
                }
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }
        public VMResponse GetAllCategory()
        {
            try
            {
                List<VMCatObat> data = (
                from M in db.MMedicalItemCategories
                where M.IsDelete == false
                select new VMCatObat
                {
                    Id = M.Id,
                    Name = M.Name,
                       
                    CreatedBy = M.CreatedBy,
                    CreatedOn = M.CreatedOn,
                    ModifiedBy = M.ModifiedBy,
                    ModifiedOn = M.ModifiedOn,
                    DeletedBy = M.DeletedBy,
                    DeletedOn = M.DeletedOn,
                    IsDelete = M.IsDelete

                }).ToList();
                response.data = data;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }
        public VMResponse GetAll()
        {
            try
            {
                List<VMCariObat?> data = (
                    from MI in db.MMedicalItems
                    join MIC in db.MMedicalItemCategories
                        on MI.MedicalItemCategoryId equals MIC.Id
                    join MIS in db.MMedicalItemSegmentations
                        on MI.MedicalItemSegmentationId equals MIS.Id
                    where MI.IsDelete == false
                    select new VMCariObat
                    {
                        Id = MI.Id,
                        Name = MI.Name,

                        MedicalItemCategoryId = MI.MedicalItemCategoryId,
                        MedicalItemCategoryName = MIC.Name,

                        Composition = MI.Composition,

                        MedicalItemSegmentationId = MI.MedicalItemSegmentationId,
                        MedicalItemSegmentationName = MIS.Name,

                        Manufacturer = MI.Manufacturer,
                        Indication = MI.Indication,
                        Dosage = MI.Dosage,
                        Directions = MI.Directions,
                        Contraindication = MI.Contraindication,
                        Caution = MI.Caution,
                        Packaging = MI.Packaging,
                        PriceMax = MI.PriceMax,
                        PriceMin = MI.PriceMin,
                        Image = MI.Image,
                        ImagePath = MI.ImagePath,

                        CreatedBy = MI.CreatedBy,
                        CreatedOn = MI.CreatedOn,
                        ModifiedBy = MI.ModifiedBy,
                        ModifiedOn = MI.ModifiedOn,
                        DeletedBy = MI.DeletedBy,
                        DeletedOn = MI.DeletedOn,
                        IsDelete = MI.IsDelete

                    }).ToList();
                if (data.Count >= 1)
                {
                    response.data = data;
                    response.message = "Fetched Medical Item Success !!!";
                }
                else
                {
                    response.message = "Fetched Medical Item Failed !!!";
                    response.success = false;
                }
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }
        public VMResponse Get(long id)
        {
            try
            {
                if(id < 1)
                {
                    response.success = false;
                    response.message = "No Medical Item ID was Found !!!";
                    return response;
                }
                response.data = GetByID(id);
                if (response.data == null)
                {
                    response.success = false;
                    response.message = "Not Found 404 !!!";
                    return response;
                }
                else
                    response.message = $"Medical item {id} was found !!!";
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }
        public VMResponse CreateUpdate(VMCariObat iObat)
        {
            try
            {
                MMedicalItem data = new MMedicalItem();

                data.Name = iObat.Name;
                data.MedicalItemCategoryId = iObat.MedicalItemCategoryId;
                data.Composition = iObat.Composition;
                data.MedicalItemSegmentationId = iObat.MedicalItemSegmentationId;
                data.Manufacturer = iObat.Manufacturer;
                data.Indication = iObat.Indication;
                data.Dosage = iObat.Dosage;
                data.Directions = iObat.Directions;
                data.Contraindication = iObat.Contraindication;
                data.Caution = iObat.Caution;
                data.Packaging = iObat.Packaging;
                data.PriceMax = iObat.PriceMax;
                data.PriceMin = iObat.PriceMin;
                //data.Image = IObat.Image;
                //data.ImagePath = IObat.ImagePath;
                if(iObat.Id < 1)
                {
                    data.CreatedBy = iObat.CreatedBy;
                    data.CreatedOn = DateTime.Now;

                    db.Add(data);
                    response.message = "New Medical Item Successfully Inserted :)";
                }
                else
                {
                    VMCariObat? Obat = GetByID(iObat.Id);
                    if (Obat == null)
                    {
                        response.success = false;
                        response.message = "Medical Item Not Exist!";
                        return response;
                    }
                    else {
                        data.Id = Obat.Id;
                        data.Name = iObat.Name;
                        data.MedicalItemCategoryId = iObat.MedicalItemCategoryId;
                        data.Composition = iObat.Composition;
                        data.MedicalItemSegmentationId = iObat.MedicalItemSegmentationId;
                        data.Manufacturer = iObat.Manufacturer;
                        data.Indication = iObat.Indication;
                        data.Dosage = iObat.Dosage;
                        data.Directions = iObat.Directions;
                        data.Contraindication = iObat.Contraindication;
                        data.Caution = iObat.Caution;
                        data.Packaging = iObat.Packaging;
                        data.PriceMax = iObat.PriceMax;
                        data.PriceMin = iObat.PriceMin;

                        data.CreatedBy = Obat.CreatedBy;
                        data.CreatedOn = Obat.CreatedOn;

                        data.ModifiedBy = iObat.ModifiedBy;
                        data.ModifiedOn = DateTime.Now;

                        db.Update(data);
                        response.message = "Update Success !";
                    }
                }
            }
            catch (Exception ex)
            {
                response.message= ex.Message;
                response.success = false;
            }
            return response;
        }

        public VMResponse Delete(long id, int delID)
        {
            try
            {
                if(id < 1)
                {
                    response.success = false;
                    response.message = "Medical Item Not Exist !!!";
                    return response;
                }

                MMedicalItem data = new MMedicalItem();
                VMCariObat? cariObat = GetByID(id);
                if(cariObat == null)
                {
                    response.success = false;
                    response.message = "Medical Item Not Esist";
                    return response;
                }
                else
                {
                    data.Id = cariObat.Id;
                    data.Name = cariObat.Name; 
                    data.MedicalItemCategoryId = cariObat.MedicalItemCategoryId;
                    data.Composition = cariObat.Composition;
                    data.MedicalItemSegmentationId = cariObat.MedicalItemSegmentationId;
                    data.Manufacturer = cariObat.Manufacturer;
                    data.Indication = cariObat.Indication;
                    data.Dosage = cariObat.Dosage;
                    data.Directions = cariObat.Directions;
                    data.Contraindication = cariObat.Contraindication;
                    data.Caution = cariObat.Caution;
                    data.Packaging = cariObat.Packaging;
                    data.PriceMax = cariObat.PriceMax;
                    data.PriceMin = cariObat.PriceMin;

                    data.CreatedBy = cariObat.CreatedBy;
                    data.CreatedOn = cariObat.CreatedOn;

                    data.ModifiedBy = cariObat.ModifiedBy;
                    data.ModifiedOn = cariObat.ModifiedOn;

                    data.DeletedBy = delID;
                    data.DeletedOn = DateTime.Now;

                    data.IsDelete = true;

                    db.Update(data);
                    db.SaveChanges();

                    response.data = data; ; ;
                    response.message = "New Medical Item Has Successfull Dekted";
                }

            }
            catch(Exception ex)
            {
                response.success = false;
                response.message = ex.Message;
            }
            return response;
        }
    }
}
