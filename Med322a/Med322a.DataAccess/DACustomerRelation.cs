﻿using Med322a.DataModels;
using Med322a.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Med322a.DataAccess
{
    public class DACustomerRelation
    {
        private readonly DB_MEDContext db;
        private VMResponse response = new VMResponse();

        public DACustomerRelation(DB_MEDContext db) => this.db = db;

        public VMResponse? GetAll()
        {
            try
            {
                List<VMCustomerRelation> relasiMember = (
                    from cr in db.MCustomerRelations
                    where cr.IsDelete == false
                    select new VMCustomerRelation
                    {
                        Id = cr.Id,
                        Name = cr.Name,
                        CreatedBy = cr.CreatedBy,
                        CreatedOn = cr.CreatedOn,
                        ModifiedBy = cr.ModifiedBy,
                        ModifiedOn = cr.ModifiedOn,
                        DeletedBy = cr.DeletedBy,
                        DeletedOn = cr.DeletedOn,
                        IsDelete = cr.IsDelete,
                    }).ToList();
                response.data = (relasiMember.Count < 1) ? null : relasiMember;
                response.message = (response.data == null) ? "No Data" : "Data relasi berhasil diterima";
            }catch (Exception ex)
            {
                response.data = null;
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }
    }
}
