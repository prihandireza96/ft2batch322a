﻿using Med322a.DataModels;
using Med322a.ViewModels;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using static System.Collections.Specialized.BitVector32;

namespace Med322a.DataAccess
{
	public class DADoctor
	{
		private readonly DB_MEDContext db;
		private VMResponse response = new VMResponse();

		public DADoctor(DB_MEDContext _db) => db = _db;

		private VMMSpecialization? GetByIdSpecialization(long id)
		{
			return (
				from s in db.MSpecializations
				where s.IsDelete == false && s.Id == id
				select new VMMSpecialization
				{
					Id = s.Id,
					Name = s.Name,

					CreatedBy = s.CreatedBy,
					CreatedOn = s.CreatedOn,
					ModifiedBy = s.ModifiedBy,
                    ModifiedByName = s.ModifiedByName,
					ModifiedOn = s.ModifiedOn,
					DeletedBy = s.DeletedBy,
					DeletedOn = s.DeletedOn,
					IsDelete = s.IsDelete
				}
			).FirstOrDefault();
		}

        private VMResponse CheckDuplicateSpecialization(string duplicate)
        {
            List<VMMSpecialization> data = new List<VMMSpecialization>();
            try
            {
                if (duplicate == null)
                {
                    response.success = false;
                    response.message = "No Doctor Specialization Name was submitted!";
                    return response;
                }

                data = (
                    from dsp in db.MSpecializations
                    where dsp.IsDelete == false && dsp.Name == duplicate
                    select new VMMSpecialization
                    {
                        Id = dsp.Id,
                        Name = dsp.Name,

                        CreatedBy = dsp.CreatedBy,
                        CreatedOn = dsp.CreatedOn,
                        ModifiedBy = dsp.ModifiedBy,
                        ModifiedOn = dsp.ModifiedOn,
                        DeletedBy = dsp.DeletedBy,
                        DeletedOn = dsp.DeletedOn,
                        IsDelete = dsp.IsDelete
                    }
                ).ToList();

                response.success = (data.Count > 0) ? false : true;
                response.message = (data.Count > 0) ? $"Doctor Specialization data '{duplicate}' already exist!" : $"Doctor Specialization data '{duplicate}' doesn't exist!";
                response.data = data;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }

        private List<VMDoctorProfile>? GetProfileRaw(long userId)
		{
			return (
				from u in db.MUsers
				join b in db.MBiodata on u.BiodataId equals b.Id
				join d in db.MDoctors on b.Id equals d.BiodataId
				join de in db.MDoctorEducations on d.Id equals de.DoctorId
				join cds in db.TCurrentDoctorSpecializations on d.Id equals cds.DoctorId
				join s in db.MSpecializations on cds.SpecializationId equals s.Id
				join dt in db.TDoctorTreatments on d.Id equals dt.DoctorId
				where u.Id == userId && u.IsDelete == false && b.IsDelete == false && d.IsDelete == false && 
				de.IsDelete == false && cds.IsDelete == false && s.IsDelete == false && dt.IsDelete == false
                select new VMDoctorProfile
				{
					UserId = u.Id,
					ImagePath = b.ImagePath,
					Fullname = b.Fullname,
					SpecializationName = s.Name,
					Str = d.Str,
					TreatmentName = dt.Name,
					InstitutionName = de.InstitutionName,
					Major = de.Major,
					EndYearEducation = de.EndYear
				}
			).ToList();
		}

        private List<VMVWDoctor>? GetDoctorRaw(VMVWDoctor search)
        {
            return (
                from d in db.MDoctors
                join b in db.MBiodata on d.BiodataId equals b.Id
                join u in db.MUsers on b.Id equals u.BiodataId
                join r in db.MRoles on u.RoleId equals r.Id
                join cds in db.TCurrentDoctorSpecializations on d.Id equals cds.DoctorId
                join s in db.MSpecializations on cds.SpecializationId equals s.Id
                join dof in db.TDoctorOffices on d.Id equals dof.DoctorId
                join mf in db.MMedicalFacilities on dof.MedicalFacilityId equals mf.Id
                join l in db.MLocations on mf.LocationId equals l.Id
                join dt in db.TDoctorTreatments on d.Id equals dt.DoctorId
                join mfc in db.MMedicalFacilityCategories on mf.MedicalFacilityCategoryId equals mfc.Id
                join dos in db.TDoctorOfficeSchedules on d.Id equals dos.DoctorId
                join mfs in db.MMedicalFacilitySchedules on mf.Id equals mfs.MedicalFacilityId
                where u.RoleId == 3 && d.IsDelete == false && 
                b.IsDelete == false && u.IsDelete == false && r.IsDelete == false && cds.IsDelete == false 
                && s.IsDelete == false && dof.IsDelete == false && mf.IsDelete == false && 
                l.IsDelete == false && dt.IsDelete == false && mfc.IsDelete == false && dos.IsDelete == false 
                && mfs.IsDelete == false
                select new VMVWDoctor
                {
                    DoctorId = d.Id,
                    ImagePath = b.ImagePath,
                    Fullname = b.Fullname,
                    SpecializationName = s.Name,
                    StartDate = dof.StartDate.ToString(),
                    EndDate = dof.EndDate.ToString(),
                    MedicalFacilityName = mf.Name,
                    LocationName = l.Name,
                    TreatmentName = dt.Name,
                    MedicalFacilityCategoryId = mfc.Id.ToString(),
                    Day = mfs.Day,
                    TimeScheduleStart = mfs.TimeScheduleStart,
                    TimeScheduleEnd = mfs.TimeScheduleEnd
                }
            ).ToList();
        }

		private List<VMDoctorDetail>? GetDetailRaw(long doctorId)
		{
			return (
				from d in db.MDoctors
				join dof in db.TDoctorOffices on d.Id equals dof.DoctorId
				join mf in db.MMedicalFacilities on dof.MedicalFacilityId equals mf.Id
				join mfc in db.MMedicalFacilityCategories on mf.MedicalFacilityCategoryId equals mfc.Id
				join l in db.MLocations on mf.LocationId equals l.Id
				join dot in db.TDoctorOfficeTreatments on dof.Id equals dot.DoctorOfficeId
				join dotp in db.TDoctorOfficeTreatmentPrices on dot.Id equals dotp.DoctorOfficeTreatmentId
                join dos in db.TDoctorOfficeSchedules on d.Id equals dos.DoctorId
                join mfs in db.MMedicalFacilitySchedules on mf.Id equals mfs.MedicalFacilityId 
                where d.Id == doctorId && d.IsDelete == false && dof.IsDelete == false && mf.IsDelete == false &&
				mfc.IsDelete == false && l.IsDelete == false && dot.IsDelete == false && dotp.IsDelete == false
                && mfs.IsDelete == false && dos.IsDelete == false
                select new VMDoctorDetail
				{
					DoctorId = d.Id,
                    MedicalFacilityId = mf.Id.ToString(),
					CurrentMedicalFacilityName = mf.Name,
					CurrentMedicalFacilityCategory = mfc.Name,
					FullAddress = mf.FullAddress,
					SpecializationName = dof.Specialization,
					StartDate = dof.StartDate,
					EndDate = dof.EndDate,
					ConsultationPrice = dotp.Price,
					AppointmentPriceStartFrom = dotp.PriceStartFrom,
					Day = mfs.Day,
					TimeScheduleStart = mfs.TimeScheduleStart,
					TimeScheduleEnd = mfs.TimeScheduleEnd,
                    MedicalFacilityCategoryId = mfc.Id.ToString()
				}
			).ToList();
		}

		private List<VMDoctorWorkHistory>? GetWorkHistory(long userId)
		{
			return (
				from dof in db.TDoctorOffices
				join mf in db.MMedicalFacilities on dof.MedicalFacilityId equals mf.Id
				join l in db.MLocations on mf.LocationId equals l.Id
				join d in db.MDoctors on dof.DoctorId equals d.Id
				join b in db.MBiodata on d.BiodataId equals b.Id
				join u in db.MUsers on b.Id equals u.BiodataId
				where u.Id == userId && u.IsDelete == false && dof.IsDelete == false && 
				mf.IsDelete == false && l.IsDelete == false && d.IsDelete == false && b.IsDelete == false
				select new VMDoctorWorkHistory
				{
					UserId = u.Id,
					MedicalFacilityName = mf.Name,
					LocationName = l.Name,
					MedicalFacilitySpecialization = dof.Specialization,
					StartDateWorkHistory = dof.StartDate,
					EndDateWorkHistory = dof.EndDate
				}
			).OrderByDescending(x => x.StartDateWorkHistory).ToList();
		}

		private List<VMDoctorAppointment>? GetAppointment(long userId)
		{
			return (
				from a in db.TAppointments
				join dco in db.TDoctorOffices on a.DoctorOfficeId equals dco.Id
				join d in db.MDoctors on dco.DoctorId equals d.Id
				join b in db.MBiodata on d.BiodataId equals b.Id
				join u in db.MUsers on b.Id equals u.BiodataId
				where u.IsDelete == false && a.IsDelete == false && dco.IsDelete == false && d.IsDelete == false && b.IsDelete == false && u.Id == userId
				select new VMDoctorAppointment
				{
					UserId = u.Id,
					AppointmentDate = a.AppointmentDate
				}
			).ToList();
		}

		private List<VMDoctorConsultation>? GetConsultation(long userId)
		{
			return (
				from ct in db.TCustomerChats
				join d in db.MDoctors on ct.DoctorId equals d.Id
				join b in db.MBiodata on d.BiodataId equals b.Id
				join u in db.MUsers on b.Id equals u.BiodataId
				where u.IsDelete == false && ct.IsDelete == false && d.IsDelete == false && b.IsDelete == false && u.Id == userId
				select new VMDoctorConsultation
				{
					UserId = u.Id,
					DoctorId = d.Id
				}
			).ToList();
		}

        private List<VMVWDoctor>? GetAllDoctorLocation()
        {
            return (
                from d in db.MDoctors
                join dof in db.TDoctorOffices on d.Id equals dof.DoctorId
                join mf in db.MMedicalFacilities on dof.MedicalFacilityId equals mf.Id
                join l in db.MLocations on mf.LocationId equals l.Id
                where d.IsDelete == false && dof.IsDelete == false && mf.IsDelete == false && l.IsDelete == false
                select new VMVWDoctor
                {
                    LocationName = l.Name
                }
            ).GroupBy(x => x.LocationName).Select(x => x.First()).ToList();
        }

        private List<VMVWDoctor>? GetAllDoctorSpecialization()
        {
            return (
                from d in db.MDoctors
                join dof in db.TDoctorOffices on d.Id equals dof.DoctorId
                where d.IsDelete == false && dof.IsDelete == false
                select new VMVWDoctor
                {
                    SpecializationName = dof.Specialization
                }
            ).GroupBy(x => x.SpecializationName).Select(x => x.First()).ToList();
        }

        public VMResponse GetAllSpecialization()
		{
			try
			{
				// Get all Doctor Specialization data
				List<VMMSpecialization> data = (
					from dsp in db.MSpecializations
                    join u in db.MUsers on dsp.ModifiedBy equals u.Id into jo
                    from j in jo.DefaultIfEmpty()
                    join b in db.MBiodata on j.BiodataId equals b.Id into jo2
                    from j2 in jo2.DefaultIfEmpty()
                    where dsp.IsDelete == false
                    select new VMMSpecialization
                    {
                        Id = dsp.Id,
                        Name = dsp.Name,

                        CreatedBy = dsp.CreatedBy,
                        CreatedOn = dsp.CreatedOn,
                        ModifiedBy = dsp.ModifiedBy,
                        ModifiedByName = (j2.Fullname ?? "-"),
                        ModifiedOn = dsp.ModifiedOn,
                        DeletedBy = dsp.DeletedBy,
                        DeletedOn = dsp.DeletedOn,
                        IsDelete = dsp.IsDelete
                    }
				).ToList();

				if (data.Count < 1)
				{
					response.message = "Doctor Specialization table has no data!";
					response.success = false;
				}
				else
				{
					response.data = data;
					response.message = "Doctor Specialization data successfully fetched!";
				}
			}
			catch (Exception ex)
			{
				response.message = ex.Message;
				response.success = false;
			}
			return response;
		}

        public VMResponse GetSpecialization(long id)
		{
			try
			{
				if (id < 1)
				{
					response.success = false;
					response.message = "No Doctor Specialization ID was submitted!";
					return response;
				}

				// Get all doctor specialization data
				response.data = GetByIdSpecialization(id);

				// Check if data is null
				if (response.data == null)
				{
					response.message = $"Doctor Specialization data with id = {id} is not exist!";
					response.success = false;
				}
				else
					response.message = $"Doctor Specialization data with id = {id} successfully fetched!";
			}
			catch (Exception ex)
			{
				response.message = ex.Message;
				response.success = false;
			}

			return response;
		}

		public VMResponse GetSpecialization(string filter)
		{
			try
			{
				// Filter Doctor Specialization data
				List<VMMSpecialization>? data = (
					from dsp in db.MSpecializations
					where dsp.IsDelete == false && (dsp.Name).Contains(filter)
					select new VMMSpecialization
					{
						Id = dsp.Id,
						Name = dsp.Name,

						CreatedBy = dsp.CreatedBy,
						CreatedOn = dsp.CreatedOn,
						ModifiedBy = dsp.ModifiedBy,
						ModifiedOn = dsp.ModifiedOn,
						DeletedBy = dsp.DeletedBy,
						DeletedOn = dsp.DeletedOn,
						IsDelete = dsp.IsDelete
					}
				).ToList();

				response.success = (data.Count > 0) ? true : false;
				response.message = (data.Count > 0) ? $"Doctor Specialization data successfully fetched data that contain word '{filter}'" : $"Doctor Specialization table has no data that contain word '{filter}'!";
				response.data = data;
			}
			catch (Exception ex)
			{
				response.message = ex.Message;
				response.success = false;
			}

			return response;
		}

		public VMResponse GetProfile(long userId)
		{
			try
			{
				if (userId < 1)
				{
					response.success = false;
					response.message = "No User ID was submitted!";
					return response;
				}

				// Get all doctor profile data
				List<VMDoctorProfile>? dataRaw = GetProfileRaw(userId);

				// Set data output
				VMDoctorProfile? data = new VMDoctorProfile();
				data.UserId = userId;

				// Get Doctor Biodata
				VMDoctorProfile? biodata = (
					from dr in dataRaw
					select new VMDoctorProfile
					{
						UserId = dr.UserId,
						ImagePath = dr.ImagePath,
						Fullname = dr.Fullname,
						SpecializationName = dr.SpecializationName,
						Str = dr.Str
					}
				).FirstOrDefault();

				// Insert biodata to data
				data.Fullname = biodata.Fullname;
				data.ImagePath = biodata.ImagePath;
				data.SpecializationName = biodata.SpecializationName;
				data.Str = biodata.Str??"0";

				// Get Doctor Appointment data
				List<VMDoctorAppointment>? doctorAppointments = GetAppointment(userId);

				// Insert Appointment to data
				data.AppointmentCount = (doctorAppointments == null) ? 0 : doctorAppointments.Count();

				// Get Doctor Consultation data
				List<VMDoctorConsultation>? doctorConsultations = GetConsultation(userId);

				// Insert Consultation to data
				data.ConsultationCount = (doctorConsultations == null) ? 0 : doctorConsultations.Count();

				// Get Doctor Work History data
				List<VMDoctorWorkHistory>? doctorWorkHistories = GetWorkHistory(userId);

				// Insert Work History to data
				data.MedicalFacilityName = string.Join(',', doctorWorkHistories.Select(x => x.MedicalFacilityName));
				data.LocationName = string.Join(',', doctorWorkHistories.Select(x => x.LocationName));
				data.MedicalFacilitySpecialization = string.Join(',', doctorWorkHistories.Select(x => x.MedicalFacilitySpecialization));
				var id = new CultureInfo("id-ID");
				data.StartYearWorkHistory = string.Join(',', doctorWorkHistories.Select(x => DateTime.Parse(x.StartDateWorkHistory.ToString(), id)));
				data.EndYearWorkHistory = string.Join(',', doctorWorkHistories.Select(x => DateTime.Parse(x.EndDateWorkHistory.ToString(), id)));

				// Get Doctor Treatment data
				List<VMDoctorProfile> treatment = (
					from dr in dataRaw
					select new VMDoctorProfile
					{
						UserId = dr.UserId,
						TreatmentName = dr.TreatmentName
					}
				).GroupBy(x => x.TreatmentName).Select(x => x.First()).ToList();

				// Insert treatment to data
				data.TreatmentName = string.Join(',', treatment.Select(x => x.TreatmentName));

				// Get Doctor Education data
				List<VMDoctorProfile> education = (
					from dr in dataRaw
					select new VMDoctorProfile
					{
						UserId = dr.UserId,
						InstitutionName = dr.InstitutionName,
						Major = dr.Major,
						EndYearEducation = dr.EndYearEducation
					}
				).GroupBy(x => x.EndYearEducation).Select(x => x.First()).OrderByDescending(x => x.EndYearEducation).ToList();

				// Insert education to data
				data.InstitutionName = string.Join(',', education.Select(x => x.InstitutionName));
				data.Major = string.Join(',', education.Select(x => x.Major));
				data.EndYearEducation = string.Join(',', education.Select(x => x.EndYearEducation));

				response.data = data;

				// Check if data is null
				if (response.data == null)
				{
					response.message = $"Doctor Profile data with User ID = {userId} is not exist!";
					response.success = false;
				}
				else
					response.message = $"Doctor Profile data with User ID = {userId} successfully fetched!";
			}
			catch (Exception ex)
			{
				response.message = ex.Message;
				response.success = false;
			}

			return response;
		}

        public VMResponse GetDetail(long doctorId)
        {
            try
            {
                if (doctorId < 1)
                {
                    response.success = false;
                    response.message = "No Doctor ID was submitted!";
                    return response;
                }

				List<VMDoctorProfile> doctorUserId = (
					from d in db.MDoctors
					join b in db.MBiodata on d.BiodataId equals b.Id
					join u in db.MUsers on b.Id equals u.BiodataId
					select new VMDoctorProfile
                    {
						UserId = u.Id,
						DoctorId = d.Id
					}
				).Where(x => x.DoctorId == doctorId).GroupBy(x => x.UserId).Select(x => x.First()).ToList();

				long userId = doctorUserId.Select(x => x.UserId).First();

                // Get all doctor profile data
                List<VMDoctorProfile>? dataBio = GetProfileRaw(userId);
                List<VMDoctorDetail>? dataDetail = GetDetailRaw(doctorId);

                // Set data output
                VMDoctorDetail? data = new VMDoctorDetail();
                data.DoctorId = doctorId;

                // Get Min StartYear
                List<VMDoctorDetail>? startYear = (
                    from dr in dataDetail
                    select new VMDoctorDetail
                    {
                        DoctorId = dr.DoctorId,
                        StartDate = dr.StartDate
                    }
                ).DistinctBy(x => new
                {
                    x.DoctorId,
                    x.StartDate
                }).OrderBy(x => x.StartDate).GroupBy(x => x.DoctorId).Select(x => x.First()).OrderBy(x => x.DoctorId).ToList();

                // Insert start date to data
                data.StartDate = startYear.Min(x => x.StartDate);

                // Calculate years experience
                int years;
                DateTime zeroTime = new DateTime(1, 1, 1);
                TimeSpan yearDiff = new TimeSpan();
                yearDiff = DateTime.Now - DateTime.Parse(data.StartDate.ToString());
                years = (zeroTime + yearDiff).Year - 1;
				data.YearExperience = years.ToString();

                // Filter doctor office that still ongoing
                dataDetail = dataDetail.Where(x => x.EndDate > DateTime.Now).ToList();

                // Get Doctor Biodata
                VMDoctorDetail? biodata = (
                    from db in dataBio
                    select new VMDoctorDetail
                    {
                        DoctorId = db.UserId,
                        ImagePath = db.ImagePath,
                        Fullname = db.Fullname,
                        SpecializationName = db.SpecializationName
                    }
                ).FirstOrDefault();

                // Insert biodata to data
                data.Fullname = biodata.Fullname;
                data.ImagePath = biodata.ImagePath;
                data.SpecializationName = biodata.SpecializationName;

                // Get Doctor Work History data
                List<VMDoctorWorkHistory>? doctorWorkHistories = GetWorkHistory(userId);

                // Insert Work History to data
                data.HistoryMedicalFacilityName = string.Join(',', doctorWorkHistories.Select(x => x.MedicalFacilityName));
                data.HistoryLocationName = string.Join(',', doctorWorkHistories.Select(x => x.LocationName));
                data.MedicalFacilitySpecialization = string.Join(',', doctorWorkHistories.Select(x => x.MedicalFacilitySpecialization));
                var id = new CultureInfo("id-ID");
                data.StartYearWorkHistory = string.Join(',', doctorWorkHistories.Select(x => DateTime.Parse(x.StartDateWorkHistory.ToString(), id)));
                data.EndYearWorkHistory = string.Join(',', doctorWorkHistories.Select(x => DateTime.Parse(x.EndDateWorkHistory.ToString(), id)));

                // Get Doctor Treatment data
                List<VMDoctorDetail> treatment = (
                    from db in dataBio
                    select new VMDoctorDetail
                    {
                        DoctorId = db.DoctorId,
                        TreatmentName = db.TreatmentName
                    }
                ).GroupBy(x => x.TreatmentName).Select(x => x.First()).ToList();

                // Insert treatment to data
                data.TreatmentName = string.Join(',', treatment.Select(x => x.TreatmentName));

                // Get Doctor Education data
                List<VMDoctorDetail> education = (
                    from db in dataBio
                    select new VMDoctorDetail
                    {
                        DoctorId = db.DoctorId,
                        InstitutionName = db.InstitutionName,
                        Major = db.Major,
                        EndYearEducation = db.EndYearEducation
                    }
                ).GroupBy(x => new
				{
					x.DoctorId,
					x.EndYearEducation
				}).Select(x => x.First()).OrderByDescending(x => x.EndYearEducation).ToList();

                // Insert education to data
                data.InstitutionName = string.Join(',', education.Select(x => x.InstitutionName));
                data.Major = string.Join(',', education.Select(x => x.Major));
                data.EndYearEducation = string.Join(',', education.Select(x => x.EndYearEducation));

                // Get Practice Location
                List<VMDoctorDetail> practiceLocation = (
                    from db in dataDetail
                    select new VMDoctorDetail
                    {
                        DoctorId = db.DoctorId,
                        MedicalFacilityId = db.MedicalFacilityId,
                        CurrentMedicalFacilityName = db.CurrentMedicalFacilityName,
                        CurrentMedicalFacilityCategory = db.CurrentMedicalFacilityCategory,
                        FullAddress = db.FullAddress
                    }
                ).GroupBy(x => new {
                    x.DoctorId,
                    x.MedicalFacilityId,
					x.CurrentMedicalFacilityName,
					x.CurrentMedicalFacilityCategory,
					x.FullAddress
                }).Select(x => x.First()).ToList();

                // Insert practice location to data
                data.MedicalFacilityId = string.Join(',', practiceLocation.Select(x => x.MedicalFacilityId));
                data.CurrentMedicalFacilityName = string.Join(',', practiceLocation.Select(x => x.CurrentMedicalFacilityName));
                data.CurrentMedicalFacilityCategory = string.Join(',', practiceLocation.Select(x => x.CurrentMedicalFacilityCategory));
                data.FullAddress = string.Join(',', practiceLocation.Select(x => x.FullAddress));

                // Get Starting Appointment Price
                List<VMDoctorDetail> allPrice = (
                    from db in dataDetail
                    select new VMDoctorDetail
                    {
                        DoctorId = db.DoctorId,
						ConsultationPrice = db.ConsultationPrice,
                        AppointmentPriceStartFrom = db.AppointmentPriceStartFrom
                    }
                ).GroupBy(x => new {
                    x.DoctorId,
					x.ConsultationPrice,
                    x.AppointmentPriceStartFrom
                }).Select(x => x.First()).ToList();

                // Insert doctor price to data
                data.ConsultationPrice = allPrice.Min(x => x.ConsultationPrice);
                data.AppointmentPriceStartFrom = allPrice.Min(x => x.AppointmentPriceStartFrom);

                // Get Practice Schedule
                List<VMDoctorDetail> scheduleRaw = (
                    from db in dataDetail
                    select new VMDoctorDetail
                    {
                        DoctorId = db.DoctorId,
						CurrentMedicalFacilityName = db.CurrentMedicalFacilityName,
                        Day = db.Day,
                        TimeScheduleStart = db.TimeScheduleStart,
                        TimeScheduleEnd = db.TimeScheduleEnd,
                        MedicalFacilityCategoryId = db.MedicalFacilityCategoryId
                    }
                ).DistinctBy(x => new {
                    x.DoctorId,
                    x.CurrentMedicalFacilityName,
                    x.Day,
                    x.TimeScheduleStart,
                    x.TimeScheduleEnd,
                    x.MedicalFacilityCategoryId
                }).ToList();

                List<VMDoctorDetail> practiceSchedule = (
                    from db in scheduleRaw
                    select new VMDoctorDetail
                    {
                        CurrentMedicalFacilityName = db.CurrentMedicalFacilityName,
                        Day = db.Day,
                        TimeScheduleStart = db.TimeScheduleStart,
                        TimeScheduleEnd = db.TimeScheduleEnd,
                        MedicalFacilityCategoryId = db.MedicalFacilityCategoryId
                    }
                ).GroupBy(x => x.CurrentMedicalFacilityName).Select(x => new VMDoctorDetail
                {
                    CurrentMedicalFacilityName = x.Key,
                    Day = String.Join("|", x.Select(y => y.Day)),
                    TimeScheduleStart = String.Join("|", x.Select(y => y.TimeScheduleStart)),
                    TimeScheduleEnd = String.Join("|", x.Select(y => y.TimeScheduleEnd)),
                    MedicalFacilityCategoryId = String.Join("|", x.Select(y => y.MedicalFacilityCategoryId))
                }).ToList();

                // Insert practice location to data
                data.Day = string.Join(',', practiceSchedule.Select(x => x.Day));
                data.TimeScheduleStart = string.Join(',', practiceSchedule.Select(x => x.TimeScheduleStart));
                data.TimeScheduleEnd = string.Join(',', practiceSchedule.Select(x => x.TimeScheduleEnd));
                data.MedicalFacilityCategoryId = string.Join(',', practiceSchedule.Select(x => x.MedicalFacilityCategoryId));

                response.data = data;

                // Check if data is null
                if (response.data == null)
                {
                    response.message = $"Doctor Detail data with Doctor ID = {doctorId} is not exist!";
                    response.success = false;
                }
                else
                    response.message = $"Doctor Detail data with Doctor ID = {doctorId} successfully fetched!";
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }

            return response;
        }

        public VMResponse SearchDoctor(VMVWDoctor search)
        {
            VMResponse response = new VMResponse();
            List<VMVWDoctor> data = new List<VMVWDoctor>();

            try
            {
                if (search.SpecializationName == null)
                {
                    throw new ArgumentNullException("Specialization Name cannnot be null!");
                }

                List<VMVWDoctor>? dataRaw = GetDoctorRaw(search);

                //Get Year Experience
                List<VMVWDoctor>? yearRaw = (
                    from dr in dataRaw
                    select new VMVWDoctor
                    {
                        DoctorId = dr.DoctorId,
                        StartDate = dr.StartDate,
                        EndDate = dr.EndDate
                    }
                ).GroupBy(x => new
                {
                    x.DoctorId,
                    x.StartDate,
                    x.EndDate
                }).Select(x => x.First()).ToList();

                // Get Min StartYear
                List<VMVWDoctor>? startYear = (
                    from dr in dataRaw
                    select new VMVWDoctor
                    {
                        DoctorId = dr.DoctorId,
                        StartDate = dr.StartDate
                    }
                ).DistinctBy(x => new
                {
                    x.DoctorId,
                    x.StartDate
                }).OrderBy(x => x.StartDate).GroupBy(x => x.DoctorId).Select(x => x.First()).OrderBy(x => x.DoctorId).ToList();

                dataRaw = dataRaw.Where(x => DateTime.Parse(x.EndDate) > DateTime.Now).ToList();

                // Get Biodata Raw
                List<VMVWDoctor>? biodataRaw = (
                    from dr in dataRaw
                    select new VMVWDoctor
                    {
                        DoctorId = dr.DoctorId,
                        Fullname = dr.Fullname,
                        ImagePath = dr.ImagePath,
                        SpecializationName = dr.SpecializationName,
                        Day = dr.Day,
                        TimeScheduleStart = dr.TimeScheduleStart,
                        TimeScheduleEnd = dr.TimeScheduleEnd,
                        MedicalFacilityCategoryId = dr.MedicalFacilityCategoryId
                    }
                )/*.GroupBy(x => x.DoctorId).Select(x => x.First())*/.DistinctBy(x => new
                {
                    x.DoctorId,
                    x.Fullname,
                    x.ImagePath,
                    x.SpecializationName,
                    x.Day,
                    x.TimeScheduleStart,
                    x.TimeScheduleEnd,
                    x.MedicalFacilityCategoryId
                }).ToList();

                //Get biodata
                List<VMVWDoctor>? biodata = (
                    from dr in biodataRaw
                    select new VMVWDoctor
                    {
                        DoctorId = dr.DoctorId,
                        Fullname = dr.Fullname,
                        ImagePath = dr.ImagePath,
                        SpecializationName = dr.SpecializationName
                    }
                ).GroupBy(x => x.DoctorId).Select(x => x.First()).ToList();

                // Get biodata schedule
                List<VMVWDoctor>? schedule = (
                    from dr in biodataRaw
                    select new VMVWDoctor
                    {
                        DoctorId = dr.DoctorId,
                        Day = dr.Day,
                        TimeScheduleStart = dr.TimeScheduleStart,
                        TimeScheduleEnd = dr.TimeScheduleEnd,
                        MedicalFacilityCategoryId = dr.MedicalFacilityCategoryId
                    }
                ).GroupBy(x => x.DoctorId).Select(x => new VMVWDoctor
                {
                    DoctorId = x.Key,
                    Day = String.Join(",", x.Select(y => y.Day)),
                    TimeScheduleStart = String.Join(",", x.Select(y => y.TimeScheduleStart)),
                    TimeScheduleEnd = String.Join(",", x.Select(y => y.TimeScheduleEnd)),
                    MedicalFacilityCategoryId = String.Join(",", x.Select(y => y.MedicalFacilityCategoryId))
                }).ToList();

                // Get Location
                List<VMVWDoctor>? location = (
                    from dr in dataRaw
                    select new VMVWDoctor
                    {
                        DoctorId = dr.DoctorId,
                        MedicalFacilityName = dr.MedicalFacilityName,
                        LocationName = dr.LocationName
                    }
                ).GroupBy(x => new
                {
                    x.DoctorId,
                    x.MedicalFacilityName,
                    x.LocationName
                }).Select(x => x.First()).GroupBy(x => x.DoctorId).Select(x => new VMVWDoctor
                {
                    DoctorId = x.Key,
                    MedicalFacilityName = String.Join(",", x.Select(y => y.MedicalFacilityName)),
                    LocationName = String.Join(",", x.Select(y => y.LocationName)),
                }).ToList();

                // Get Treatment
                List<VMVWDoctor>? treatment = (
                    from dr in dataRaw
                    select new VMVWDoctor
                    {
                        DoctorId = dr.DoctorId,
                        TreatmentName = dr.TreatmentName
                    }
                ).GroupBy(x => new
                {
                    x.DoctorId,
                    x.TreatmentName
                }).Select(x => x.First()).GroupBy(x => x.DoctorId).Select(x => new VMVWDoctor
                {
                    DoctorId = x.Key,
                    TreatmentName = String.Join(",", x.Select(y => y.TreatmentName))
                }).ToList();

                // Insert into data
                int j = 0, years;
                DateTime zeroTime = new DateTime(1, 1, 1);
                TimeSpan yearDiff = new TimeSpan();
                long? temp = 0;
                int maxDoctor = (int)db.MDoctors.Max(x => x.Id);
				for (int i = 1; i <= maxDoctor; i++)
                {
                    if (j >= biodata.Count) break;
                    temp = biodata[j].DoctorId;
                    if (i == temp)
                    {
                        yearDiff = DateTime.Now - DateTime.Parse(startYear[j].StartDate);
                        years = (zeroTime + yearDiff).Year - 1;
                        data.Add(new VMVWDoctor
                        {
                            DoctorId = biodata[j].DoctorId,
                            Fullname = biodata[j].Fullname,
                            ImagePath = biodata[j].ImagePath,
                            SpecializationName = biodata[j].SpecializationName,
                            YearExperience = years.ToString(),
                            MedicalFacilityName = location[j].MedicalFacilityName,
                            LocationName = location[j].LocationName,
                            TreatmentName = treatment[j].TreatmentName,
                            Day = schedule[j].Day,
                            TimeScheduleStart = schedule[j].TimeScheduleStart,
                            TimeScheduleEnd = schedule[j].TimeScheduleEnd,
                            MedicalFacilityCategoryId = schedule[j].MedicalFacilityCategoryId
                        });
                        j++;
                    }
                }

                // Coba
                data = (
                    from d in data
                    where d.LocationName.Contains(search.LocationName ?? "") && d.Fullname.Contains(search.Fullname ?? "") &&
                    d.TreatmentName.Contains(search.TreatmentName ?? "") && d.SpecializationName.Contains(search.SpecializationName.Substring(6))
                    select new VMVWDoctor
                    {
                        DoctorId = d.DoctorId,
                        ImagePath = d.ImagePath,
                        Fullname = d.Fullname,
                        SpecializationName = d.SpecializationName,
                        YearExperience = d.YearExperience,
                        StartDate = d.StartDate,
                        EndDate = d.EndDate,
                        MedicalFacilityName = d.MedicalFacilityName,
                        LocationName = d.LocationName,
                        TreatmentName = d.TreatmentName,
                        MedicalFacilityCategoryId = d.MedicalFacilityCategoryId,
                        Day = d.Day,
                        TimeScheduleStart = d.TimeScheduleStart,
                        TimeScheduleEnd = d.TimeScheduleEnd
                    }
                ).ToList();

                if (data.Count < 1)
                {
                    response.message = $"Doctor data with specialization = {search.SpecializationName}";
                    if (search.LocationName != null) response.message += $", location = {search.LocationName}";
                    if (search.Fullname != null) response.message += $", name contains '{search.Fullname}'";
                    if (search.TreatmentName != null) response.message += $" and treatment = {search.TreatmentName}";
                    response.message += " doesn't exist!";
                    response.success = false;
                }
                else
                {
                    response.data = data;
                    response.message = "Doctor data successfully fetched!";
                }
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }

            return response;
        }

        public VMResponse CreateUpdateSpecialization(VMMSpecialization input)
		{
			try
			{
                MSpecialization data = new MSpecialization();
                VMResponse duplicate = new VMResponse();

				data.Name = input.Name;

				// Check if it's a new or updated data
				if (input.Id < 1)
				{
                    duplicate = CheckDuplicateSpecialization(input.Name);
                    if(duplicate.success)
                    {
                        // Process insert new data
                        data.CreatedBy = input.CreatedBy;
                        data.CreatedOn = DateTime.Now;

                        db.Add(data);
                        db.SaveChanges();
                        response.message = "New Doctor Specialization data successfully added!";
                    }
                    else
                    {
                        response.message = duplicate.message;
                        response.success = false;
                    }
				}
				else
				{
                    duplicate = CheckDuplicateSpecialization(input.Name);
                    if (duplicate.success)
                    {
                        VMMSpecialization? specialization = GetByIdSpecialization(input.Id);

                        // Check if data exist
                        if (specialization == null)
                        {
                            response.message = $"Doctor Specialization table with ID {input.Id} doesn't exist!";
                            response.success = false;
                        }
                        // Process update data
                        else
                        {
                            data.Id = specialization.Id;
                            data.CreatedBy = specialization.CreatedBy;
                            data.CreatedOn = specialization.CreatedOn;
                            data.DeletedBy = specialization.DeletedBy;
                            data.DeletedOn = specialization.DeletedOn;

                            data.ModifiedBy = input.ModifiedBy;
                            data.ModifiedByName = (
                               from u in db.MUsers
                               join b in db.MBiodata on u.BiodataId equals b.Id
                               where u.Id == input.ModifiedBy && u.IsDelete == false && b.IsDelete == false
                               select new MBiodatum
                               {
                                   Fullname = b.Fullname
                               }
                            ).ToString();
                            data.ModifiedOn = DateTime.Now;

                            db.Update(data);
                            db.SaveChanges();
                            response.message = $"Doctor Specialization data with ID {data.Id} successfully updated!";
                        }
                    }
                    else
                    {
                        response.message = duplicate.message;
                        response.success = false;
                    }
				}

				response.data = data;
			}
			catch (Exception ex)
			{
				response.message = "Create Update Specialization failed to run!";
				response.success = false;
			}

			return response;
		}

		public VMResponse CreateUpdateProfile(VMDoctorProfile input)
		{
			try
			{
				MBiodatum data = new MBiodatum();
                MBiodatum? biodata = (
                    from b in db.MBiodata
                    join u in db.MUsers on b.Id equals u.BiodataId
                    where u.Id == input.UserId && b.IsDelete == false && u.IsDelete == false
                    select new MBiodatum
                    {
                        Id = b.Id,
                        Fullname = b.Fullname,
                        MobilePhone = b.MobilePhone,
                        Image = b.Image,
                        ImagePath = b.ImagePath,
                        CreatedBy = b.CreatedBy,
                        CreatedOn = b.CreatedOn,
                        ModifiedBy = b.ModifiedBy,
                        ModifiedOn = b.ModifiedOn,
                        DeletedBy = b.DeletedBy,
                        DeletedOn = b.DeletedOn,
                        IsDelete = b.IsDelete
                    }
                ).FirstOrDefault();

				// Check if data exist
				if (biodata == null)
				{
					response.message = $"Doctor Profile data with User ID {input.UserId} doesn't exist!";
					response.success = false;
				}
				// Process update data
				else
				{
                    data.Id = biodata.Id;
					data.Fullname = biodata.Fullname;
					data.MobilePhone = biodata.MobilePhone;
					data.Image = biodata.Image;
					data.ImagePath = input.ImagePath;
					data.CreatedBy = biodata.CreatedBy;
					data.CreatedOn = biodata.CreatedOn;
					data.ModifiedBy = input.UserId;
					data.ModifiedOn = DateTime.Now;
					data.DeletedBy = biodata.DeletedBy;
					data.DeletedOn = biodata.DeletedOn;
					data.IsDelete = biodata.IsDelete;

					db.Update(data);
					db.SaveChanges();
					response.message = $"Doctor Profile image with ID {data.Id} successfully updated!";
				}

				response.data = data;
			}
			catch (Exception ex)
			{
				response.message = "Create Update Profile failed to run!";
				response.success = false;
			}

			return response;
		}

		public VMResponse DeleteSpecialization(long id, long userId)
		{
			try
			{
				if (id < 1)
				{
					response.message = "No Doctor Specialization ID was submitted!";
					response.success = false;
					return response;
				}

				MSpecialization data = new MSpecialization();
				VMMSpecialization? specialization = GetByIdSpecialization(id);

				data.Id = specialization.Id;
				data.Name = specialization.Name;
				data.CreatedBy = specialization.CreatedBy;
				data.CreatedOn = specialization.CreatedOn;
				data.ModifiedBy = specialization.ModifiedBy;
				data.ModifiedOn = specialization.ModifiedOn;

				// Process Soft-Delete
				data.IsDelete = true;
				data.DeletedBy = userId;
				data.DeletedOn = DateTime.Now;

				db.Update(data);
				db.SaveChanges();

				response.data = data;
				response.message = "New Doctor Specialization data successfully deleted!";
			}
			catch (Exception ex)
			{
				response.message = ex.Message;
				response.success = false;
			}

			return response;
		}

        public VMResponse GetModalOption()
        {
            VMVWDoctor data = new VMVWDoctor();
            try
            {
                // Get all Doctor Location & Specialization data
                List<VMVWDoctor>? dataLocation = GetAllDoctorLocation();
                List<VMVWDoctor>? dataSpecialization = GetAllDoctorSpecialization();

                if (dataLocation == null)
                {
                    response.message = "Doctor Specialization has no data!";
                    response.success = false;
                }
                else if (dataSpecialization == null)
                {
                    response.message = "Doctor Location has no data!";
                    response.success = false;
                }
                else
                {
                    data.LocationName = string.Join(",", dataLocation.Select(x => x.LocationName));
                    data.SpecializationName = string.Join(",", dataSpecialization.Select(x => x.SpecializationName));
                    
                    response.data = data;
                    response.message = "Doctor Location and Specialization data successfully fetched!";
                }
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }

        public VMResponse GetAllDoctorTreatment(string specialization)
        {
            try
            {
                // Get all Doctor Treatment data
                List<VMVWDoctor> data = (
                    from d in db.MDoctors
                    join dof in db.TDoctorOffices on d.Id equals dof.DoctorId
                    join dot in db.TDoctorOfficeTreatments on dof.Id equals dot.DoctorOfficeId
                    join dt in db.TDoctorTreatments on dot.DoctorTreatmentId equals dt.Id
                    where dof.Specialization == specialization && d.IsDelete == false && dof.IsDelete == false && dot.IsDelete == false  && dt.IsDelete == false
                    select new VMVWDoctor
                    {
                        TreatmentName = dt.Name
                    }
                ).GroupBy(x => x.TreatmentName).Select(x => x.First()).ToList();

                if (data.Count < 1)
                {
                    response.message = "Doctor Treatment has no data!";
                    response.success = false;
                }
                else
                {
                    response.data = data;
                    response.message = "Doctor Treatment successfully fetched!";
                }
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }

        
    }
}
