﻿using Med322a.DataModels;
using Med322a.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Med322a.DataAccess
{
    public class DADoctorTreatment
	{
		private readonly DB_MEDContext db;
		private VMResponse response = new VMResponse();

		public DADoctorTreatment(DB_MEDContext db) => this.db = db;

		public VMResponse GetAllTreatment(int id)
		{
			try
			{
				List<VMDoctorTreatment> data = (
					from dt in db.TDoctorTreatments
					where dt.DoctorId == id
					select new VMDoctorTreatment
					{
						Id = dt.Id,
						DoctorId = dt.DoctorId,
						Name = dt.Name,
						CreatedBy = dt.CreatedBy,
						CreatedOn = dt.CreatedOn,
						ModifiedBy = dt.ModifiedBy,
						ModifiedOn = dt.ModifiedOn,
						DeletedBy = dt.DeletedBy,
						DeletedOn = dt.DeletedOn,
						IsDelete = dt.IsDelete
					}).ToList();

				response.data = data;
				response.message = "data successfully fetched!";
			}
			catch (Exception ex)
			{
				response.message = ex.Message;
				response.success = false;
			}
			return response;
		}
	}
}
