﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Med322a.DataModels;

namespace Med322a.DataAccess
{
    public class DAEmail
    {
        public void SendEmailAfterEdit(string email, string otp)
        {
            //string otp = GenerateRandomOTP();
            // Buat objek emailRequest berdasarkan perubahan yang Anda butuhkan
            DMEmail emailRequest = new DMEmail
            {
                To = email,
                Subject = "Email verification",
                Body = $"Your OTP: {otp}"
            };

            // Buat HttpClient untuk berkomunikasi dengan API EmailController
            using (HttpClient client = new HttpClient())
            {
                // Sesuaikan URL sesuai dengan route yang Anda tentukan di EmailController
                string apiUrl = "https://localhost:7210/api/Email"; // Ganti dengan URL yang sesuai

                // Kirim permintaan HTTP POST ke API EmailController
                HttpResponseMessage response = client.PostAsJsonAsync(apiUrl, emailRequest).Result;

                // Lakukan penanganan respons jika diperlukan
                //if (response.IsSuccessStatusCode)
                //{
                //    // Berhasil mengirim email
                //    // Lakukan tindakan lain jika diperlukan
                //}
                //else
                //{
                //    // Gagal mengirim email
                //    // Lakukan penanganan kesalahan jika diperlukan
                //}
            }
        }

        public string GenerateRandomOTP()
        {
            // Generate a 6-digit random OTP
            int otpLength = 6;
            RandomNumberGenerator rng = new RNGCryptoServiceProvider();
            byte[] otpBytes = new byte[otpLength];
            rng.GetBytes(otpBytes);
            int otpValue = BitConverter.ToInt32(otpBytes, 0);
            int otpInRange = Math.Abs(otpValue % (int)Math.Pow(10, otpLength));
            return otpInRange.ToString("D6"); // Format as 6-digit string
        }
    }
}
