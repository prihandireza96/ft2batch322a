﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Med322a.DataModels;
using Med322a.ViewModels;

namespace Med322a.DataAccess
{
    public class DAForgotPassword
    {
        private readonly DB_MEDContext db;
        private VMResponse response = new VMResponse();
        public DAForgotPassword(DB_MEDContext _db)
        {
            db = _db;
        }
        public VMResponse TokenForgetPass(string Email)
        {
            try
            {
                TToken data = new TToken();
                DAEmail emailPro = new DAEmail();

                MUser? vrt = db.MUsers
                        .Where(o => o.Email == Email)
                        .FirstOrDefault();

                var token = emailPro.GenerateRandomOTP();

                data.Email = Email;

                data.UserId = vrt.Id;
                data.Token = token;

                data.ExpiredOn = DateTime.Now.AddMinutes(10);
                data.IsExpired = false;
                data.UsedFor = "Forget_Password";
                data.CreatedBy = vrt.Id;
                data.CreatedOn = DateTime.Now;
                data.IsDelete = false;

                db.Add(data);
                db.SaveChanges();
                emailPro.SendEmailAfterEdit(Email, token);
                response.data = data;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }
        public VMResponse ForgetPassword(VMMUser inputdata)
        {
            VMResponse response = new VMResponse();
            try
            {
                TResetPassword resetPassword = new TResetPassword();

                MUser? user = db.MUsers
                    .Where(t => t.Id == inputdata.Id)
                    .FirstOrDefault();

                resetPassword.OldPassword = user.Password;
                resetPassword.NewPassword = inputdata.Password;

                resetPassword.ResetFor = inputdata.Email;
                resetPassword.CreatedBy = inputdata.BiodataId;
                resetPassword.CreatedOn = DateTime.Now;

                resetPassword.IsDelete = false;

                db.Add(resetPassword);
                db.SaveChanges();

                TToken? otp = db.TTokens
                    .OrderByDescending(to => to.Id)
                    .Where(t => t.UsedFor == "Forget_Password" && t.UserId == inputdata.Id && t.IsExpired == false)
                    .FirstOrDefault();

                if (otp != null)
                {
                    otp.IsExpired = true;
                    db.SaveChanges();
                }

                response.success = true;
                response.message = "Data reset password successfully updated";
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }
    }
}
