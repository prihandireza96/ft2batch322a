﻿using Med322a.DataModels;
using Med322a.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Med322a.DataAccess
{
    public class DAMenu
    {
        private readonly DB_MEDContext db;
        private VMResponse response = new VMResponse();

        public DAMenu(DB_MEDContext _db)
        {
            db = _db;
        }

        private VMMMenu? GetById(long id)
        {
            return (
                    from mm in db.MMenus
                    where mm.Id == id && mm.IsDelete == false
                    select new VMMMenu
                    {
                        Id = mm.Id,
                        Name = mm.Name,
                        Url = mm.Url,
                        ParentId = mm.ParentId,
                        BigIcon = mm.BigIcon,
                        SmallIcon = mm.SmallIcon,

                        IsDelete = mm.IsDelete,

                        CreatedBy = mm.CreatedBy,
                        CreatedOn = mm.CreatedOn,
                        ModifiedBy = mm.ModifiedBy,
                        ModifiedOn = mm.ModifiedOn,
                        DeletedBy = mm.DeletedBy,
                        DeletedOn = mm.DeletedOn,
                    }
                ).FirstOrDefault();
        }
        public VMResponse GetAll()
        {
            try
            {
                List<VMMMenu> data = (
                    from mm in db.MMenus
                    where mm.IsDelete == false
                    select new VMMMenu
                    {
                        Id = mm.Id,                        
                        Name = mm.Name,
                        Url = mm.Url,
                        ParentId = mm.ParentId,
                        BigIcon = mm.BigIcon,
                        SmallIcon = mm.SmallIcon,

                        IsDelete = mm.IsDelete,

                        CreatedBy = mm.CreatedBy,
                        CreatedOn = mm.CreatedOn,
                        ModifiedBy = mm.ModifiedBy,
                        ModifiedOn = mm.ModifiedOn,
                        DeletedBy = mm.DeletedBy,
                        DeletedOn = mm.DeletedOn,
                    }
                ).ToList();
                if (data.Count < 1)
                {
                    response.message = "Menu table has no data!";
                    response.success = false;
                    return response;
                }

                response.data = data;
                response.message = "Menu data successsfully fetched!";
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }

        public VMResponse Get(long id)
        {
            try
            {
                if (id < 1)
                {
                    response.success = false;
                    response.message = "No Menu ID was submitted!";

                    return response;
                }

                response.data = GetById(id);

                if (response.data == null)
                {
                    response.message = $"Menu data with ID:{id} is not exist!";
                    response.success = false;
                }
                else
                    response.message = $"Menu data with ID:{id} is successfully fetched!";
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }

        public VMResponse GetByFilter(string filter)
        {
            try
            {
                List<VMMMenu> role = (
               from mm in db.MMenus
               where mm.IsDelete == false &&
               mm.Name.Contains(filter)
               select new VMMMenu
               {
                   Id = mm.Id,
                   Name = mm.Name,
                   Url = mm.Url,
                   ParentId = mm.ParentId,
                   BigIcon = mm.BigIcon,
                   SmallIcon = mm.SmallIcon,

                   IsDelete = mm.IsDelete,

                   CreatedBy = mm.CreatedBy,
                   CreatedOn = mm.CreatedOn,
                   ModifiedBy = mm.ModifiedBy,
                   ModifiedOn = mm.ModifiedOn,
                   DeletedBy = mm.DeletedBy,
                   DeletedOn = mm.DeletedOn,
               }
               ).ToList();

                response.message = (role.Count > 0)
                    ? "Menu data successfully fetched!"
                    : "No Menu found!";

                response.success = (role.Count > 0) ? true : false;
                response.data = role;
            }
            catch (Exception ex)
            {
                response.success = false;
                response.message = "Failed to fetched Menu data!" + ex.Message;
            }
            return response;
        }


        public VMResponse CreateUpdate(VMMMenu inputrole)
        {
            try
            {
                MMenu data = new MMenu();

                data.Name = inputrole.Name;
                data.Url = inputrole.Url;
                data.ParentId = inputrole.ParentId;


                if (inputrole.Id < 1)
                {

                    data.CreatedBy = inputrole.CreatedBy;
                    data.CreatedOn = DateTime.Now;

                    db.Add(data);

                    response.message = "New Akses data successfully Inserted!";
                }
                else
                {
                    VMMMenu? akses = GetById(inputrole.Id);

                    if (akses == null)
                    {
                        response.success = false;
                        response.message = "Akses data doesn't exist!";

                        return response;
                    }

                    else
                    {
                        data.Id = akses.Id;

                        data.CreatedBy = akses.CreatedBy;
                        data.CreatedOn = akses.CreatedOn;

                        data.ModifiedBy = inputrole.ModifiedBy;
                        data.ModifiedOn = DateTime.Now;

                        db.Update(data);
                        response.message = "Akses Data Successfully Updated!";
                    }
                }

                db.SaveChanges();

                response.data = data;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }
    }
}
