﻿using Med322a.DataModels;
using Med322a.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Med322a.DataAccess
{
	public class DAMenuRole
	{
		private readonly DB_MEDContext db;
		private VMResponse response = new VMResponse();

		public DAMenuRole(DB_MEDContext _db)
		{
			db = _db;
		}

		public VMResponse GetAll()
		{
			try
			{
				List<VMMMenuRole> dataAturAkses = (
					from mr in db.MRoles
					join mmr in db.MMenuRoles
					 on mr.Id equals mmr.RoleId
					join mm in db.MMenus
					on mmr.MenuId equals mm.Id

					where mmr.IsDelete == false
					select new VMMMenuRole
					{
						Id = mr.Id,
						Name = mr.Name,
						Code = mr.Code,

						RoleId = mmr.RoleId,
						MenuId = mmr.MenuId,
						MenuName = mm.Name,

						IsDelete = mmr.IsDelete,

						CreatedBy = mmr.CreatedBy,
						CreatedOn = mmr.CreatedOn,
						ModifiedBy = mmr.ModifiedBy,
						ModifiedOn = mmr.ModifiedOn,
						DeletedBy = mmr.DeletedBy,
						DeletedOn = mmr.DeletedOn,
					}
				).ToList();
				if (dataAturAkses.Count < 1)
				{
					response.message = "MenuRole table has no data!";
					response.success = false;
					return response;
				}

				response.data = dataAturAkses;
				response.message = "MenuRole data successsfully fetched!";
			}
			catch (Exception ex)
			{
				response.message = ex.Message;
				response.success = false;
			}
			return response;
		}

		public VMResponse GetByFilter(string filter)
		{
			try
			{
				List<VMMMenuRole> dataAturAkses = (
					from mmr in db.MMenuRoles
					join mr in db.MRoles
					on mmr.RoleId equals mr.Id
					where mmr.IsDelete == false &&
					mr.Name.Contains(filter)

					select new VMMMenuRole
					{
						Id = mr.Id,
						Name = mr.Name,
						Code = mr.Code,

						RoleId = mmr.RoleId,
						MenuId = mmr.MenuId,


						IsDelete = mmr.IsDelete,

						CreatedBy = mmr.CreatedBy,
						CreatedOn = mmr.CreatedOn,
						ModifiedBy = mmr.ModifiedBy,
						ModifiedOn = mmr.ModifiedOn,
						DeletedBy = mmr.DeletedBy,
						DeletedOn = mmr.DeletedOn,
					}
			   ).ToList();

				response.message = (dataAturAkses.Count > 0)
					? "Role data successfully fetched!"
					: "No Role found!";

				response.success = (dataAturAkses.Count > 0) ? true : false;
				response.data = dataAturAkses;
			}
			catch (Exception ex)
			{
				response.success = false;
				response.message = "Failed to fetched Role data!" + ex.Message;
			}
			return response;
		}

		private VMMMenuRole? GetById(long id)
		{
			return (
					from mr in db.MRoles
					join mmr in db.MMenuRoles
					  on mr.Id equals mmr.RoleId
					join mm in db.MMenus
					  on mmr.MenuId equals mm.Id

					where mmr.IsDelete == false
					select new VMMMenuRole
					{
						Id = mr.Id,
						Name = mr.Name,
						Code = mr.Code,

						RoleId = mmr.RoleId,
						MenuId = mmr.MenuId,



						IsDelete = mmr.IsDelete,

						CreatedBy = mmr.CreatedBy,
						CreatedOn = mmr.CreatedOn,
						ModifiedBy = mmr.ModifiedBy,
						ModifiedOn = mmr.ModifiedOn,
						DeletedBy = mmr.DeletedBy,
						DeletedOn = mmr.DeletedOn,
					}
				).FirstOrDefault();
		}

		public VMResponse Get(long id)
		{
			try
			{
				if (id < 1)
				{
					response.success = false;
					response.message = "No Akses ID was submitted!";

					return response;
				}

				response.data = GetById(id);

				if (response.data == null)
				{
					response.message = $"Akses data with ID:{id} is not exist!";
					response.success = false;
				}
				else
					response.message = $"Akses data with ID:{id} is successfully fetched!";
			}
			catch (Exception ex)
			{
				response.message = ex.Message;
				response.success = false;
			}
			return response;
		}

		public VMResponse CreateUpdate(VMMMenuRole inputakses)
		{
			try
			{

				MMenuRole data = new MMenuRole();

				data.MenuId = inputakses.MenuId;
				data.RoleId = inputakses.RoleId;

				//Check if its new or updated data
				if (inputakses.Id < 1)
				{

					data.CreatedBy = inputakses.CreatedBy;
					data.CreatedOn = DateTime.Now;

					db.Add(data);

					response.message = "New Role data successfully Inserted!";
				}
				else
				{
					VMMMenuRole? akses = GetById(inputakses.Id);

					//Check if data exist
					if (akses == null)
					{
						response.success = false;
						response.message = "Akses data doesn't exist!";

						return response;
					}

					else
					{
						data.Id = akses.Id;

						data.CreatedBy = akses.CreatedBy;
						data.CreatedOn = akses.CreatedOn;

						data.ModifiedBy = inputakses.ModifiedBy;
						data.ModifiedOn = DateTime.Now;

						//proses update data
						db.Update(data);
						response.message = "Akses Data Successfully Updated!";
					}
				}

				db.SaveChanges();

				response.data = data;
			}
			catch (Exception ex)
			{
				response.message = ex.Message;
				response.success = false;
			}
			return response;
		}

	}
}
