﻿using Med322a.DataModels;
using Med322a.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Med322a.DataAccess
{
    public class DANominal
    {
        private readonly DB_MEDContext db;
        private VMResponse response = new VMResponse();
        public DANominal(DB_MEDContext _db) { db = _db; }
        public VMResponse GetAll()
        {
            try
            {
                List<VMMWalletDefaultNominal> data = (
                    from W in db.MWalletDefaultNominals
                    where W.IsDelete == false
                    select new VMMWalletDefaultNominal
                    {
                        Id = W.Id,
                        Nominal = W.Nominal,
                        CreatedBy = W.CreatedBy,
                        CreatedOn = W.CreatedOn,
                        ModifiedBy = W.ModifiedBy,
                        ModifiedOn = W.ModifiedOn,
                        DeletedBy = W.DeletedBy,
                        DeletedOn = W.DeletedOn,
                        IsDelete = W.IsDelete
                    }).ToList();
                if(data.Count < 1 )
                {
                    response.message = "Go To Custom Nominal Please!";
                    response.success = false;
                    return response;
                }
                else
                {
                    response.message = string.Empty;
                    response.success = true;
                    response.data = data;
                }
            }
            catch(Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;        
        }
        public VMResponse GetAmount()
        {
            try
            {
                List<VMTCustomerWalletWithdraw> data = (
                    from A in db.TCustomerWalletWithdraws
                    join C in db.MCustomers
                        on A.CustomerId equals C.Id
                    join B in db.MBiodata
                        on C.BiodataId equals B.Id
                    join N in db.MWalletDefaultNominals
                        on A.WalletDefaultNominalId equals N.Id
                    where A.IsDelete == false
                    select new VMTCustomerWalletWithdraw
                    {
                        Id = A.Id,
                        CustomerId = A.CustomerId,
                        FullName = B.Fullname,
                        WalletDefaultNominalId = A.WalletDefaultNominalId,
                        Nominal = N.Nominal,
                        Amount = A.Amount,
                        BankName = A.BankName,
                        AccountNumber = A.AccountNumber,
                        AccountName = A.AccountName,
                        Otp = A.Otp,

                        CreatedBy = A.CreatedBy,
                        CreatedOn = A.CreatedOn,
                        ModifiedBy = A.ModifiedBy,
                        ModifiedOn = A.ModifiedOn,
                        DeletedBy = A.DeletedBy,
                        DeletedOn = A.DeletedOn,
                        IsDelete = A.IsDelete
                    }).ToList();
                if(data.Count < 1)
                {
                    response.message = "Nominal Not Found!";
                    response.success = false;
                    return response;
                }
                else
                {
                    response.message = string.Empty;
                    response.success = true;
                    response.data = data;
                }
            }
            catch( Exception ex ) 
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }
        public VMResponse CustomNominal(VMTCustomerCustomNominal customNominal)
        {
            try
            {
                TCustomerCustomNominal data = new TCustomerCustomNominal();
                data.CustomerId = customNominal.CustomerId;
                data.Nominal = customNominal.Nominal;
                data.CreatedBy = customNominal.CreatedBy;
                data.CreatedOn = DateTime.Now;

                db.Add(data);
                response.message = "New Custom Nominal Successfully Inserted";
                db.SaveChanges();
                response.data = data;
            }
            catch(Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }
        public VMResponse GetCustNominal()
        {
            try
            {
                List<VMTCustomerCustomNominal> data = (
                    from CN in db.TCustomerCustomNominals
                    where CN.IsDelete == false
                    select new VMTCustomerCustomNominal
                    {
                        Id = CN.Id,
                        CustomerId = CN.CustomerId,
                        Nominal = CN.Nominal,

                        CreatedBy = CN.CreatedBy,
                        CreatedOn = CN.CreatedOn,
                        ModifiedBy = CN.ModifiedBy,
                        ModifiedOn = CN.ModifiedOn,
                        DeletedBy = CN.DeletedBy,
                        DeletedOn = CN.DeletedOn,
                        IsDelete = CN.IsDelete
                    }).ToList();
                if (data.Count < 1)
                {
                    response.message = "Nominal Not Found!";
                    response.success = false;
                    return response;
                }
                else
                {
                    response.message = string.Empty;
                    response.success = true;
                    response.data = data;
                }
            }
            catch(Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }
        public VMResponse PostAmount(VMTCustomerWalletWithdraw withdraw)
        {
            try
            {
                TCustomerWalletWithdraw data = new TCustomerWalletWithdraw();
                data.CustomerId = withdraw.CustomerId;
                data.WalletDefaultNominalId = withdraw.WalletDefaultNominalId;
                data.Amount = withdraw.Amount;
                data.BankName = withdraw.BankName;
                data.AccountNumber = withdraw.AccountNumber;
                data.AccountName = withdraw.AccountName;
                data.Otp = withdraw.Otp;
                
                data.CreatedBy = withdraw.CreatedBy;
                data.CreatedOn = DateTime.Now;

                db.Add(data);
                response.message = "Successfully Withdraw";
                db.SaveChanges();
                response.data = data;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }
    }
}
