﻿using Med322a.DataModels;
using Med322a.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace Med322a.DataAccess
{
	public class DAPatient
	{
		private readonly DB_MEDContext db;
		private VMResponse response = new VMResponse();

		public DAPatient(DB_MEDContext _db) => db = _db;

		private VMMUser? GetIdUser(long id)
		{
			return (
				from u in db.MUsers
				join b in db.MBiodata on u.BiodataId equals b.Id
				join c in db.MCustomers on b.Id equals c.BiodataId
				where u.Id == id && u.IsDelete == false && u.IsLocked == false
				//Jangan lupa tambahin islocked
				select new VMMUser
				{
					Id = u.Id,
					BiodataId = b.Id,
					Fullname = b.Fullname,
					Dob = c.Dob,
					MobilePhone = b.MobilePhone,
					Email = u.Email,
					Password = u.Password,
					LoginAttempt = u.LoginAttempt,
					IsLocked = u.IsLocked,
					LastLogin = u.LastLogin,
					RoleId = u.RoleId,
					ImagePath = b.ImagePath,

					CreatedBy = u.CreatedBy,
					CreatedOn = u.CreatedOn,
					ModifiedBy = u.ModifiedBy,
					ModifiedOn = u.ModifiedOn,
					DeletedBy = u.DeletedBy,
					DeletedOn = u.DeletedOn,
					IsDelete = u.IsDelete,
				}).FirstOrDefault();
		}

		private VMCustomer? GetIdCustomer(long id)
		{
			return (
				from c in db.MCustomers
				join b in db.MBiodata on c.BiodataId equals b.Id
				join u in db.MUsers on b.Id equals u.BiodataId
				where c.IsDelete == false && u.Id == id && u.IsLocked == false
				select new VMCustomer
				{
					Id = c.Id,
					BiodataId = c.BiodataId,
					Dob = c.Dob,
					Gender = c.Gender,
					BloodGroupId = c.BloodGroupId,
					RhesusType = c.RhesusType,
					Height = c.Height,
					Weight = c.Weight,
					CreatedBy = c.CreatedBy,
					CreatedOn = c.CreatedOn,
					ModifiedBy = c.ModifiedBy,
					ModifiedOn = c.ModifiedOn,
					DeletedBy = c.DeletedBy,
					DeletedOn = c.DeletedOn,
					IsDelete = c.IsDelete,
				}).FirstOrDefault();
		}

		private VMBiodata? GetIdBiodata(long id)
		{
			return (
				from b in db.MBiodata
				join u in db.MUsers on b.Id equals u.BiodataId
				where u.Id == id && b.IsDelete == false && u.IsLocked == false
				select new VMBiodata
				{
					Id = b.Id,
					Fullname = b.Fullname,
					MobilePhone = b.MobilePhone,
					Image = b.Image,
					ImagePath = b.ImagePath,

					CreatedBy = b.CreatedBy,
					CreatedOn = b.CreatedOn,
					ModifiedBy = b.ModifiedBy,
					ModifiedOn = b.ModifiedOn,
					DeletedBy = b.DeletedBy,
					DeletedOn = b.DeletedOn,
					IsDelete = b.IsDelete,
				}).FirstOrDefault();
		}

		private VMMRole? GetUserRole(long id)
		{
			return (
				from r in db.MRoles
				join u in db.MUsers on r.Id equals u.RoleId
				join b in db.MBiodata on u.BiodataId equals b.Id
				where u.Id == id && u.IsDelete == false && u.IsLocked == false
				select new VMMRole
				{
					Id = r.Id,
					BiodataId = u.BiodataId,
					Name = r.Name,
					Fullname = b.Fullname,
					Code = r.Code,
					ImagePath = b.ImagePath,

					CreatedBy = u.CreatedBy,
					CreatedOn = u.CreatedOn,
					ModifiedBy = u.ModifiedBy,
					ModifiedOn = u.ModifiedOn,
					DeletedBy = u.DeletedBy,
					DeletedOn = u.DeletedOn,
					IsDelete = u.IsDelete,
				}).FirstOrDefault();
		}

		public List<VMMUser> GetAllUser()
		{
			return (
				from u in db.MUsers where u.IsLocked == false && u.IsDelete == false
				&& u.IsLocked == false
				select new VMMUser
				{
					Id = u.Id,
					BiodataId = u.BiodataId,
					RoleId = u.RoleId,
					Email = u.Email,
					Password = u.Password,
					LoginAttempt = u.LoginAttempt,
					IsLocked = u.IsLocked,
					LastLogin = u.LastLogin,

					CreatedBy = u.CreatedBy,
					CreatedOn = u.CreatedOn,
					DeletedBy = u.DeletedBy,
					DeletedOn = u.DeletedOn,
					ModifiedBy = u.ModifiedBy,
					ModifiedOn = u.ModifiedOn,
					IsDelete = u.IsDelete,
				}).ToList();
		}

		private VMMUser? GetPasswordVerification(long id, string oldPassword)
		{
			return (
				from u in db.MUsers
				where u.Password == oldPassword &&
				u.Id == id && u.IsLocked == false && u.IsDelete == false
				&& u.IsLocked == false
				select new VMMUser
				{
					Id = u.Id,
					BiodataId = u.BiodataId,
					RoleId = u.RoleId,
					Email = u.Email,
					Password = oldPassword,
					LoginAttempt = u.LoginAttempt,
					IsLocked = u.IsLocked,
					LastLogin = u.LastLogin,

					CreatedBy = u.CreatedBy,
					CreatedOn = u.CreatedOn,
					DeletedBy = u.DeletedBy,
					DeletedOn = u.DeletedOn,
					ModifiedBy = u.ModifiedBy,
					ModifiedOn = u.ModifiedOn,
					IsDelete = u.IsDelete,
				}
				).FirstOrDefault();
		}

		public VMResponse GetById(int id)
		{
			try
			{
				VMMUser? data = GetIdUser(id);
				response.data = (data == null) ? null : data;
				response.message = (response.data == null) ? "No Data" : $"Get data ID = {id} successfully fetched!";
			}
			catch (Exception ex)
			{
				response.message = ex.Message;
				response.success = false;
				response.data = null;
			}

			return response;
		}

		public VMResponse GetRoleByUserId(long id)
		{
			try
			{
				VMMRole? data = GetUserRole(id);
				response.data = (data == null) ? null : data;
				response.message = (response.data == null) ? "No Data" : $"Get data ID = {id} successfully fetched!";
			}
			catch (Exception ex)
			{
				response.message = ex.Message;
				response.success = false;
				response.data = null;
			}

			return response;

		}

		public VMResponse EditById(VMMUser inputId)
		{
			try
			{
				MBiodatum dataBiodataKosong = new MBiodatum();
				dataBiodataKosong.Fullname = inputId.Fullname;
				dataBiodataKosong.MobilePhone = inputId.MobilePhone;

				MCustomer dataCustomerKosong = new MCustomer();
				dataCustomerKosong.Dob = inputId.Dob;

				VMMUser userDenganId = GetIdUser(inputId.Id);
				VMBiodata biodataDenganId = GetIdBiodata(inputId.Id);
				VMCustomer customerDenganId = GetIdCustomer(inputId.Id);

				if (userDenganId == null)
				{
					response.success = false;
					response.message = "User data is not Exist!";
					return response;
				}
				else
				{
					//edit biodata
					dataBiodataKosong.Id = biodataDenganId.Id;
					dataBiodataKosong.Image = biodataDenganId.Image;
					dataBiodataKosong.ImagePath = biodataDenganId.ImagePath;

					//edit base Properties biodata
					dataBiodataKosong.CreatedBy = biodataDenganId.CreatedBy;
					dataBiodataKosong.CreatedOn = biodataDenganId.CreatedOn;
					dataBiodataKosong.ModifiedBy = inputId.ModifiedBy;
					dataBiodataKosong.ModifiedOn = DateTime.Now;
					dataBiodataKosong.DeletedBy = biodataDenganId.DeletedBy;
					dataBiodataKosong.DeletedOn = biodataDenganId.DeletedOn;
					dataBiodataKosong.IsDelete = biodataDenganId.IsDelete;

					db.Update(dataBiodataKosong);

					//edit DOB dari customer
					dataCustomerKosong.Id = customerDenganId.Id;
					dataCustomerKosong.BiodataId = customerDenganId.BiodataId;
					dataCustomerKosong.Gender = customerDenganId.Gender;
					dataCustomerKosong.BloodGroupId = customerDenganId.BloodGroupId;
					dataCustomerKosong.RhesusType = customerDenganId.RhesusType;
					dataCustomerKosong.Height = customerDenganId.Height;
					dataCustomerKosong.Weight = customerDenganId.Weight;

					//edit base Properties customer
					dataCustomerKosong.CreatedBy = customerDenganId.CreatedBy;
					dataCustomerKosong.CreatedOn = customerDenganId.CreatedOn;
					dataCustomerKosong.ModifiedBy = inputId.ModifiedBy;
					dataCustomerKosong.ModifiedOn = DateTime.Now;
					dataCustomerKosong.DeletedBy = customerDenganId.DeletedBy;
					dataCustomerKosong.DeletedOn = customerDenganId.DeletedOn;
					dataCustomerKosong.IsDelete = customerDenganId.IsDelete;


					//masukkan semua diatas kedalam view user

					db.Update(dataCustomerKosong);

					response.message = "Biodata berhasil di update";
				}
				db.SaveChanges();
				response.data = inputId;
			}
			catch (Exception ex)
			{
				response.message = ex.Message;
				response.success = false;
				response.data = null;
			}
			return response;
		}

		public VMResponse EditEmailUser(VMMUser inputId)
		{
			try
			{
				MUser dataUserKosong = new MUser();
				dataUserKosong.Email = inputId.Email;
				VMMUser userDenganId = GetIdUser(inputId.Id);

				if (inputId == null)
				{
					response.success = false;
					response.message = "User data is not Exist!";
					return response;
				}
				else
				{
					//if (db.MUsers.Any(bg => bg.Email == inputId.Email && bg.Id != inputId.Id))
					//{
					//	response.success = false;
					//	response.message = "Email already exists!";
					//	return response;
					//}

					dataUserKosong.Id = userDenganId.Id;
					dataUserKosong.BiodataId = userDenganId.BiodataId;
					dataUserKosong.RoleId = userDenganId.RoleId;
					dataUserKosong.Password = userDenganId.Password;
					dataUserKosong.LoginAttempt = userDenganId.LoginAttempt;
					dataUserKosong.IsLocked = userDenganId.IsLocked;
					dataUserKosong.LastLogin = userDenganId.LastLogin;
					//base properties
					dataUserKosong.CreatedBy = userDenganId.CreatedBy;
					dataUserKosong.CreatedOn = userDenganId.CreatedOn;
					dataUserKosong.ModifiedBy = inputId.ModifiedBy;
					dataUserKosong.ModifiedOn = DateTime.Now;
					dataUserKosong.DeletedBy = userDenganId.DeletedBy;
					dataUserKosong.DeletedOn = userDenganId.DeletedOn;
					dataUserKosong.IsDelete = userDenganId.IsDelete;

					//masukkan semua diatas kedalam view user
					SendEmailAfterEdit(inputId.Email);

					db.Update(dataUserKosong);

					response.message = "Biodata berhasil di update";
				}

				db.SaveChanges();
				response.data = inputId;
			}
			catch (Exception ex)
			{
				response.message = ex.Message;
				response.success = false;
				response.data = null;
			}
			return response;
		}

		public VMResponse EditPasswordUser(VMMUser inputId)
		{
			try
			{
				MUser dataUserKosong = new MUser();
				TResetPassword dataResetPasswordKosong = new TResetPassword();
				VMMUser userDenganId = GetIdUser(inputId.Id);
				dataUserKosong.Password = inputId.Password;
				dataResetPasswordKosong.NewPassword = inputId.Password;

				if (inputId == null)
				{
					response.success = false;
					response.message = "User data is not Exist!";
					return response;
				}
				else
				{
					dataUserKosong.Id = userDenganId.Id;
					dataUserKosong.BiodataId = userDenganId.BiodataId;
					dataUserKosong.RoleId = userDenganId.RoleId;
					dataUserKosong.Email = userDenganId.Email;
					dataUserKosong.LoginAttempt = userDenganId.LoginAttempt;
					dataUserKosong.IsLocked = userDenganId.IsLocked;
					dataUserKosong.LastLogin = userDenganId.LastLogin;

					//base properties
					dataUserKosong.CreatedBy = userDenganId.CreatedBy;
					dataUserKosong.CreatedOn = userDenganId.CreatedOn;
					dataUserKosong.ModifiedBy = userDenganId.ModifiedBy;
					dataUserKosong.ModifiedOn = DateTime.Now;
					dataUserKosong.DeletedBy = userDenganId.DeletedBy;
					dataUserKosong.DeletedOn = userDenganId.DeletedOn;
					dataUserKosong.IsDelete = userDenganId.IsDelete;


					db.Update(dataUserKosong);

					//edit Reset Password
					dataResetPasswordKosong.OldPassword = userDenganId.Password;
					dataResetPasswordKosong.ResetFor = userDenganId.Email;

					//base properties
					dataResetPasswordKosong.CreatedBy = userDenganId.CreatedBy;
					dataResetPasswordKosong.CreatedOn = DateTime.Now;
					dataUserKosong.DeletedBy = userDenganId.DeletedBy;
					dataUserKosong.DeletedOn = userDenganId.DeletedOn;
					dataUserKosong.IsDelete = userDenganId.IsDelete;
					//masukkan semua diatas kedalam view user

					db.Update(dataResetPasswordKosong);

					response.message = "Biodata berhasil di update";
				}

				db.SaveChanges();
				response.data = inputId;
			}
			catch (Exception ex)
			{
				response.message = ex.Message;
				response.success = false;
				response.data = null;
			}
			return response;
		}

		public VMResponse EditImagesUser(VMMUser inputId)
		{
			try
			{
				MBiodatum dataBiodataKosong = new MBiodatum();
				dataBiodataKosong.ImagePath = inputId.ImagePath;
				VMBiodata biodataDenganId = GetIdBiodata(inputId.Id);
				if (inputId == null)
				{
					response.success = false;
					response.message = "User data is not Exist!";
					return response;
				}
				else
				{
					//edit biodata
					dataBiodataKosong.Id = biodataDenganId.Id;
					dataBiodataKosong.Fullname = biodataDenganId.Fullname;
					dataBiodataKosong.MobilePhone = biodataDenganId.MobilePhone;
					dataBiodataKosong.Image = biodataDenganId.Image;

					//edit base Properties biodata
					dataBiodataKosong.CreatedBy = biodataDenganId.CreatedBy;
					dataBiodataKosong.CreatedOn = biodataDenganId.CreatedOn;
					dataBiodataKosong.ModifiedBy = inputId.ModifiedBy;
					dataBiodataKosong.ModifiedOn = DateTime.Now;
					dataBiodataKosong.DeletedBy = biodataDenganId.DeletedBy;
					dataBiodataKosong.DeletedOn = biodataDenganId.DeletedOn;
					dataBiodataKosong.IsDelete = biodataDenganId.IsDelete;

					db.Update(dataBiodataKosong);

					response.message = "Biodata berhasil di update";
				}

				db.SaveChanges();
				response.data = inputId;

			}
			catch (Exception ex)
			{
				response.message = ex.Message;
				response.success = false;
				response.data = null;
			}
			return response;
		}

		public VMResponse CheckPassword(long inputId, string password)
		{
			try
			{
				VMMUser? userDenganId = GetPasswordVerification(inputId, password);

				if (userDenganId != null)
				{
					response.success = true;
					response.message = "password lama benar.";
					response.data = null;
				}
				else
				{
					response.message = "password lama salah.";
					response.success = false;
					response.data = null;
				}
			}
			catch (Exception ex)
			{
				response.message = ex.Message;
				response.success = false;
				response.data = null;
			}
			return response;
		}

		private void SendEmailAfterEdit(string email)
		{
            string otp = GenerateRandomOTP();
            // Buat objek emailRequest berdasarkan perubahan yang Anda butuhkan
            DMEmail emailRequest = new DMEmail
			{
				To = email,
				Subject = "Edit Email verification",
				Body = $"Your OTP: {otp}"
            };

			// Buat HttpClient untuk berkomunikasi dengan API EmailController
			using (HttpClient client = new HttpClient())
			{
				// Sesuaikan URL sesuai dengan route yang Anda tentukan di EmailController
				string apiUrl = "https://localhost:7210/api/Email"; // Ganti dengan URL yang sesuai

				// Kirim permintaan HTTP POST ke API EmailController
				HttpResponseMessage response = client.PostAsJsonAsync(apiUrl, emailRequest).Result;

				// Lakukan penanganan respons jika diperlukan
				//if (response.IsSuccessStatusCode)
				//{
				//    // Berhasil mengirim email
				//    // Lakukan tindakan lain jika diperlukan
				//}
				//else
				//{
				//    // Gagal mengirim email
				//    // Lakukan penanganan kesalahan jika diperlukan
				//}
			}
		}

        private string GenerateRandomOTP()
        {
            // Generate a 6-digit random OTP
            int otpLength = 6;
            RandomNumberGenerator rng = new RNGCryptoServiceProvider();
            byte[] otpBytes = new byte[otpLength];
            rng.GetBytes(otpBytes);
            int otpValue = BitConverter.ToInt32(otpBytes, 0);
            int otpInRange = Math.Abs(otpValue % (int)Math.Pow(10, otpLength));
            return otpInRange.ToString("D6"); // Format as 6-digit string
        }

    }
}
