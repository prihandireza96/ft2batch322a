﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Med322a.DataModels;
using Med322a.ViewModels;

namespace Med322a.DataAccess
{
    public class DARegister
    {
        private readonly DB_MEDContext db;
        private VMResponse response = new VMResponse();
        public DARegister(DB_MEDContext _db)
        {
            db = _db;
        }

        public VMResponse GetAllRole()
        {
            try
            {
                List<VMMRole> role = (
                    from r in db.MRoles
                    where r.IsDelete == false
                    select new VMMRole
                    {
                        Id = r.Id,

                        Name = r.Name,
                        Code = r.Code,

                        CreatedBy = r.CreatedBy,
                        CreatedOn = r.CreatedOn,
                        ModifiedBy = r.ModifiedBy,
                        ModifiedOn = r.ModifiedOn,
                        DeletedBy = r.DeletedBy,
                        DeletedOn = r.DeletedOn,
                        IsDelete = r.IsDelete
                    }
                ).ToList();

                if (role.Count < 1)
                {
                    response.message = "Role table has no data!";
                    response.success = false;
                    return response;
                }
                response.data = role;
                response.message = "Role data succesfully fetched!";
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }
        public VMResponse SaveToken(string Email)
        {
            try
            {
                TToken data = new TToken();
                DAEmail emailPro = new DAEmail();
                //VMResponse user = GetUserByEmail(Email);
                //VMMUser? datauser = user.data;
                MUser? lastuser = db.MUsers
                    .OrderByDescending(u => u.Id)
                    .FirstOrDefault();

                var token = emailPro.GenerateRandomOTP();

                data.Email = Email;

                data.Token = token;

                //data.UserId = (lastuser.Id) + 1;
                data.ExpiredOn = DateTime.Now.AddMinutes(10);
                data.IsExpired = false;
                data.UsedFor = "Register";
                //data.CreatedBy = (lastuser.Id) + 1;
                data.CreatedOn = DateTime.Now;
                data.IsDelete = false;

                db.Add(data);
                db.SaveChanges();
                
                emailPro.SendEmailAfterEdit(Email, token);
                response.data = data;
            }
            catch(Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }
        
        public VMResponse GetTokenByEmail(string email)
        {
            try
            {
                if (string.IsNullOrEmpty(email))
                {
                    response.success = false;
                    response.message = "No User Data was fetched";
                    return response;
                }

                VMTToken? data = (
                    from t in db.TTokens
                    where t.IsDelete == false
                        && t.IsExpired == false
                        && t.Email == email
                    orderby t.Id
                    select new VMTToken
                    {
                        Id = t.Id,
                        Email = t.Email,
                        UserId = t.UserId,
                        Token = t.Token,
                        
                        ExpiredOn = t.ExpiredOn,
                        IsExpired = t.IsExpired,
                        UsedFor = t.UsedFor,

                        CreatedBy = t.CreatedBy,
                        CreatedOn = t.CreatedOn,
                        ModifiedBy = t.ModifiedBy,
                        ModifiedOn = t.ModifiedOn,
                        DeletedBy = t.DeletedBy,
                        DeletedOn = t.DeletedOn,
                        IsDelete = t.IsDelete
                    }
                ).LastOrDefault();

                response.data = data;

                if (response.data == null)
                {
                    response.message = $"There's No User with email {email}";
                    response.success = false;
                }
                else
                {
                    response.message = $"User with email {email} successfully fetched!";
                }
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }
        public VMTToken? GetTokenById(long Id)
        {
            return (
                from t in db.TTokens
                where t.IsDelete == false
                    && t.IsExpired == false
                    && t.Id == Id
                select new VMTToken
                {
                    Id = t.Id,
                    Email = t.Email,
                    UserId = t.UserId,
                    Token = t.Token,

                    ExpiredOn = t.ExpiredOn,
                    IsExpired = t.IsExpired,
                    UsedFor = t.UsedFor,

                    CreatedBy = t.CreatedBy,
                    CreatedOn = t.CreatedOn,
                    ModifiedBy = t.ModifiedBy,
                    ModifiedOn = t.ModifiedOn,
                    DeletedBy = t.DeletedBy,
                    DeletedOn = t.DeletedOn,
                    IsDelete = t.IsDelete
                }
            ).FirstOrDefault();
        }
        public VMResponse ResendToken(VMTToken token)
        {
            try
            {
                TToken data = new TToken();
                DAEmail emailPro = new DAEmail();

                if (token == null)
                {
                    response.success = false;
                    response.message = "Token Data is Not Exist!";
                    return response;
                }
                else
                {
                    var otp = emailPro.GenerateRandomOTP();

                    data.Id = token.Id;
                    data.Email = token.Email;

                    data.Token = otp;

                    data.UserId = token.UserId;
                    data.ExpiredOn = DateTime.Now.AddMinutes(10);
                    data.IsExpired = false;
                    data.UsedFor = token.UsedFor;
                    data.CreatedBy = token.CreatedBy;
                    data.CreatedOn = DateTime.Now;
                    data.IsDelete = false;

                    db.Update(data);
                    emailPro.SendEmailAfterEdit(token.Email, otp);
                    response.message = "Token Data Successfully Updated!";
                }
                db.SaveChanges();
                
                response.data = data;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }
        public VMResponse UpdateToken(VMTToken inputdata)
        {
            try
            {
                TToken data = new TToken();

                VMTToken? token = GetTokenById(inputdata.Id);

                if (token == null)
                {
                    response.success = false;
                    response.message = "Token Data is Not Exist!";
                    return response;
                }
                else
                {
                    data.Id = token.Id;

                    data.Email = token.Email;
                    data.UserId = token.UserId;
                    data.Token = token.Token;
                    data.ExpiredOn = token.ExpiredOn;

                    data.IsExpired = inputdata.IsExpired;

                    data.UsedFor = token.UsedFor;

                    data.CreatedBy = token.CreatedBy;
                    data.CreatedOn = token.CreatedOn;
                    data.ModifiedBy = token.ModifiedBy;
                    data.ModifiedOn = token.ModifiedOn;
                    data.DeletedBy = token.DeletedBy;
                    data.DeletedOn = token.DeletedOn;
                    data.IsDelete = token.IsDelete;

                    db.Update(data);
                    response.message = "Token Data Successfully Updated!";
                }
                db.SaveChanges();

                response.data = data;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }

        public VMResponse Registry(VMMUser inputdata)
        {
            VMResponse response = new VMResponse();
            //VMMUser dataUser = new VMMUser();
            
            try
            {
                MBiodatum bio = new MBiodatum();

                bio.Fullname = inputdata.Fullname;
                bio.MobilePhone = inputdata.MobilePhone;

                bio.CreatedBy = 0;
                bio.CreatedOn = DateTime.Now;

                db.MBiodata.Add(bio);
                db.SaveChanges();

                MBiodatum? biod = db.MBiodata
                    .OrderByDescending(b => b.Id)
                    .FirstOrDefault();
                if(bio.CreatedBy == 0)
                {
                    bio.CreatedBy = biod.Id;
                    db.SaveChanges();
                }

                MUser user = new MUser();
                user.BiodataId = biod.Id;
                user.RoleId = inputdata.RoleId;
                user.Email = inputdata.Email;
                user.Password = inputdata.Password;
                user.LoginAttempt = 0;
                user.IsLocked = false;

                user.CreatedBy = biod.Id;
                user.CreatedOn = DateTime.Now;

                db.MUsers.Add(user);
                db.SaveChanges();

                TToken ? otp = db.TTokens
                    .OrderByDescending(to => to.Id)
                    .Where(t => t.UserId == null && t.UsedFor == "Register")
                    .FirstOrDefault();

                MUser? lastUser = db.MUsers
                    .OrderByDescending(u => u.Id)
                    .FirstOrDefault();

                if(otp != null)
                {
                    otp.UserId = lastUser.Id;
                    otp.IsExpired = true;
                    otp.CreatedBy = lastUser.Id;
                    db.SaveChanges();
                }

                if (inputdata.RoleId == 1)
                {
                    MAdmin adm = new MAdmin();
                    adm.BiodataId = lastUser.BiodataId;

                    adm.CreatedBy = biod.Id;
                    adm.CreatedOn = DateTime.Now;

                    db.MAdmins.Add(adm);
                    db.SaveChanges();
                }
                else if(inputdata.RoleId == 2)
                {
                    MCustomer cust = new MCustomer();
                    cust.BiodataId = lastUser.BiodataId;

                    cust.CreatedBy = biod.Id;
                    cust.CreatedOn = DateTime.Now;

                    db.MCustomers.Add(cust);
                    db.SaveChanges();
                }
                else if(inputdata.RoleId == 3)
                {
                    MDoctor doct = new MDoctor();
                    doct.BiodataId = lastUser.BiodataId;

                    doct.CreatedBy = biod.Id;
                    doct.CreatedOn = DateTime.Now;

                    db.MDoctors.Add(doct);
                    db.SaveChanges();
                }

                VMMUser? userdata = (
                    from u in db.MUsers
                    join b in db.MBiodata
                        on u.BiodataId equals b.Id
                    where u.IsDelete == false
                        && u.Id == lastUser.Id
                        && u.IsLocked == false
                    select new VMMUser
                    {
                        Id = u.Id,
                        BiodataId = u.BiodataId,
                        Fullname = b.Fullname,
                        MobilePhone = b.MobilePhone,
                        RoleId = u.RoleId,
                        Email = u.Email,
                        Password = u.Password,

                        LoginAttempt = u.LoginAttempt,
                        IsLocked = u.IsLocked,
                        LastLogin = u.LastLogin,

                        CreatedBy = u.CreatedBy,
                        CreatedOn = u.CreatedOn,
                        ModifiedBy = u.ModifiedBy,
                        ModifiedOn = u.ModifiedOn,
                        DeletedBy = u.DeletedBy,
                        DeletedOn = u.DeletedOn,
                        IsDelete = u.IsDelete
                    }
                ).FirstOrDefault();

                response.data = userdata;
            }
            catch(Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }
    }
}
