﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Numerics;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Med322a.DataModels;
using Med322a.ViewModels;

namespace Med322a.DataAccess
{
    public class DAUser
    {
        private readonly DB_MEDContext db;
        private VMResponse response = new VMResponse();
        public DAUser(DB_MEDContext _db)
        {
            db = _db;
        }
        public VMResponse GetMenuView()
        {
            VMResponse response = new VMResponse();
            try
            {
                List<VMMenuForView> menu = (
                    from m in db.MMenus
                    join r in db.MMenuRoles
                        on m.Id equals r.MenuId
                    join rl in db.MRoles
                        on r.RoleId equals rl.Id
                    where m.IsDelete == false
                    select new VMMenuForView
                    {
                        Id = m.Id,
                        RoleId = rl.Id,
                        Name = m.Name,
                        RoleName = rl.Name,
                        Url = m.Url,
                        ParentId = m.ParentId,
                        BigIcon = m.BigIcon,
                        SmallIcon = m.SmallIcon,

                        CreatedBy = m.CreatedBy,
                        CreatedOn = m.CreatedOn,
                        ModifiedBy = m.ModifiedBy,
                        ModifiedOn = m.ModifiedOn,
                        DeletedBy = m.DeletedBy,
                        DeletedOn = m.DeletedOn,
                        IsDelete = m.IsDelete
                    }
                ).ToList();

                response.data = menu;
                response.message = (menu.Count > 0) ? $"{menu.Count} Menu avaiable" : "Menu isn't avaiable";
                response.success = (menu.Count > 0) ? true : false;
            }
            catch(Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }
        public VMResponse GetAll()
        {
            try
            {
                List<VMMUser> user = (
                    from u in db.MUsers
                    join b in db.MBiodata
                        on u.BiodataId equals b.Id
                    join c in db.MCustomers 
                        on b.Id equals c.BiodataId
                    where u.IsDelete == false
                        && u.IsLocked == false
                    select new VMMUser {
                        Id = u.Id,
                        BiodataId = u.BiodataId,
                        Fullname = b.Fullname,
                        MobilePhone = b.MobilePhone,
                        Dob = c.Dob,                        
                        RoleId = u.RoleId,
                        Email = u.Email,
                        Password = u.Password,

                        LoginAttempt = u.LoginAttempt,
                        IsLocked = u.IsLocked,
                        LastLogin = u.LastLogin,

                        CreatedBy = u.CreatedBy,
                        CreatedOn = u.CreatedOn,
                        ModifiedBy = u.ModifiedBy,
                        ModifiedOn = u.ModifiedOn,
                        DeletedBy = u.DeletedBy,
                        DeletedOn = u.DeletedOn,
                        IsDelete = u.IsDelete
                    }

                ).ToList();

                if(user.Count < 1)
                {
                    response.message = "User table has no data!";
                    response.success = false;
                    return response;
                }
                response.data = user;
                response.message = "User data succesfully fetched!";
            }
            catch(Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }
        public VMResponse GetByEmail(string email)
        {
            try
            {
                if (string.IsNullOrEmpty(email))
                {
                    response.success = false;
                    response.message = "No User Data was fetched";
                    return response;
                }

                VMMUser? data = (
                    from u in db.MUsers
                    join b in db.MBiodata
                        on u.BiodataId equals b.Id
                    join c in db.MCustomers
                        on b.Id equals c.BiodataId
                    where u.IsDelete == false
                        && u.Email == email
                        && u.IsLocked == false
                    select new VMMUser
                    {
                        Id = u.Id,
                        BiodataId = u.BiodataId,
                        Fullname = b.Fullname,
                        MobilePhone = b.MobilePhone,
                        Dob = c.Dob,
                        RoleId = u.RoleId,
                        Email = u.Email,
                        Password = u.Password,

                        LoginAttempt = u.LoginAttempt,
                        IsLocked = u.IsLocked,
                        LastLogin = u.LastLogin,

                        CreatedBy = u.CreatedBy,
                        CreatedOn = u.CreatedOn,
                        ModifiedBy = u.ModifiedBy,
                        ModifiedOn = u.ModifiedOn,
                        DeletedBy = u.DeletedBy,
                        DeletedOn = u.DeletedOn,
                        IsDelete = u.IsDelete
                    }
                ).FirstOrDefault();

                response.data = data;

                if (response.data == null)
                {
                    response.message = $"There's No User with email {email}";
                    response.success = false;
                }
                else
                {
                    response.message = $"User with email {email} successfully fetched!";
                }
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }
        public VMResponse GetByEmailAll(string email)
        {
            try
            {
                if (string.IsNullOrEmpty(email))
                {
                    response.success = false;
                    response.message = "No User Data was fetched";
                    return response;
                }

                VMMUser? data = (
                    from u in db.MUsers
                    join b in db.MBiodata
                        on u.BiodataId equals b.Id
                    where u.Email == email
                        && u.IsDelete == false
                    select new VMMUser
                    {
                        Id = u.Id,
                        BiodataId = u.BiodataId,
                        Fullname = b.Fullname,
                        MobilePhone = b.MobilePhone,
                        RoleId = u.RoleId,
                        Email = u.Email,
                        Password = u.Password,

                        LoginAttempt = u.LoginAttempt,
                        IsLocked = u.IsLocked,
                        LastLogin = u.LastLogin,

                        CreatedBy = u.CreatedBy,
                        CreatedOn = u.CreatedOn,
                        ModifiedBy = u.ModifiedBy,
                        ModifiedOn = u.ModifiedOn,
                        DeletedBy = u.DeletedBy,
                        DeletedOn = u.DeletedOn,
                        IsDelete = u.IsDelete
                    }
                ).FirstOrDefault();

                response.data = data;

                if (response.data == null)
                {
                    response.message = $"There's No User with email {email}";
                    response.success = false;
                }
                else
                {
                    response.message = $"User with email {email} successfully fetched!";
                }
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }
        public VMMUser? GetById(long id)
        {
            return (
                    from u in db.MUsers
                    join b in db.MBiodata
                     on u.BiodataId equals b.Id
                    join c in db.MCustomers
                       on b.Id equals c.BiodataId
                    where u.IsDelete == false
                        && u.IsLocked == false
                        && u.Id == id
                    select new VMMUser
                    {
                        Id = u.Id,
                        BiodataId = u.BiodataId,
                        Fullname = b.Fullname,
                        MobilePhone = b.MobilePhone,
                        Dob = c.Dob,
                        RoleId = u.RoleId, 
                        Email = u.Email,
                        Password = u.Password,

                        LoginAttempt = u.LoginAttempt,
                        IsLocked = u.IsLocked,
                        LastLogin = u.LastLogin,

                        CreatedBy = u.CreatedBy,
                        CreatedOn = u.CreatedOn,
                        ModifiedBy = u.ModifiedBy,
                        ModifiedOn = u.ModifiedOn,
                        DeletedBy = u.DeletedBy,
                        DeletedOn = u.DeletedOn,
                        IsDelete = u.IsDelete
                    }
                ).FirstOrDefault();
        }
        public VMResponse Get(long id)
        {
            try
            {
                if (id < 1)
                {
                    response.success = false;
                    response.message = "No User Data was fetched";
                    return response;
                }
                
                response.data = GetById(id);

                if(response.data == null)
                {
                    response.message = $"There's No User with ID = {id}";
                    response.success = false;
                }
                else
                {
                    response.message = $"User with ID = {id} successfully fetched!";
                }
            }
            catch(Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }
        public VMResponse CreateUpdate(VMMUser inputuser)
        {
            try
            {
                MUser data = new MUser();
                
                data.Email = inputuser.Email;
                data.Password = inputuser.Password;

                if (inputuser.Id < 1)
                {
                    data.BiodataId = inputuser.BiodataId;
                    data.RoleId = inputuser.RoleId;

                    data.LoginAttempt = 0;
                    data.IsLocked = false;
                    data.LastLogin = DateTime.Now;

                    data.CreatedBy = inputuser.CreatedBy;
                    data.CreatedOn = DateTime.Now;

                    db.Add(data);
                    response.message = "New User Data Successfully Inserted!";
                }
                else
                {
                    VMMUser? user = GetById(inputuser.Id);

                    if(user == null)
                    {
                        response.success = false;
                        response.message = "User Data is Not Exist!";
                        return response;
                    }
                    else
                    {
                        data.Id = user.Id;

                        data.BiodataId = user.BiodataId;
                        data.RoleId = user.RoleId;

                        data.LoginAttempt = 0;
                        data.IsLocked = inputuser.IsLocked;
                        data.LastLogin = DateTime.Now;

                        data.CreatedBy = user.CreatedBy;
                        data.CreatedOn = user.CreatedOn;

                        data.ModifiedBy = inputuser.ModifiedBy;
                        data.ModifiedOn = DateTime.Now;

                        db.Update(data);
                        response.message = "User Data Successfully Updated!";
                    }
                }
                db.SaveChanges();

                response.data = data;
            }
            catch(Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }
        public VMResponse UpdatePasswordSalah(VMMUser inputuser)
        {
            try
            {
                MUser data = new MUser();

                data.Email = inputuser.Email;
                data.Password = inputuser.Password;

                VMMUser? user = GetById(inputuser.Id);

                if (user == null)
                {
                    response.success = false;
                    response.message = "User Data is Not Exist!";
                    return response;
                }
                else
                {
                    data.Id = user.Id;

                    data.BiodataId = user.BiodataId;
                    data.RoleId = user.RoleId;

                    data.LoginAttempt = inputuser.LoginAttempt;
                    data.IsLocked = inputuser.IsLocked;
                    data.LastLogin = user.LastLogin;

                    data.CreatedBy = user.CreatedBy;
                    data.CreatedOn = user.CreatedOn;

                    data.ModifiedBy = inputuser.BiodataId;
                    data.ModifiedOn = DateTime.Now;

                    db.Update(data);
                    response.message = "User Data Successfully Updated!";
                    db.SaveChanges();
                }

                response.data = data;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }
        public VMResponse UpdateForgetPassword(VMMUser inputuser)
        {
            try
            {
                MUser data = new MUser();

                data.Email = inputuser.Email;
                data.Password = inputuser.Password;

                VMMUser? user = GetById(inputuser.Id);

                if (user == null)
                {
                    response.success = false;
                    response.message = "User Data is Not Exist!";
                    return response;
                }
                else
                {
                    data.Id = user.Id;

                    data.BiodataId = user.BiodataId;
                    data.RoleId = user.RoleId;

                    data.LoginAttempt = user.LoginAttempt;
                    data.IsLocked = user.IsLocked;
                    data.LastLogin = user.LastLogin;

                    data.CreatedBy = user.CreatedBy;
                    data.CreatedOn = user.CreatedOn;

                    data.ModifiedBy = inputuser.BiodataId;
                    data.ModifiedOn = DateTime.Now;

                    db.Update(data);
                    response.message = "User Data Successfully Updated!";
                    db.SaveChanges();
                }

                response.data = data;
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            }
            return response;
        }
        public VMResponse Delete(long id, int userId)
        {
            try
            {
                if (id < 1)
                {
                    response.success = false;
                    response.message = "User Data is Not Exist!";
                    return response;
                }

                MUser data = new MUser();
                VMMUser? user = GetById(id);

                if (user == null)
                {
                    response.success = false;
                    response.message = "User Data is Not Exist!";
                    return response;
                }
                else
                {
                    data.Id = user.Id;
                    data.BiodataId = user.BiodataId;
                    data.RoleId = user.RoleId;

                    data.Email = user.Email;
                    data.Password = user.Password;

                    data.LoginAttempt = user.LoginAttempt;
                    data.IsLocked = user.IsLocked;
                    data.LastLogin = user.LastLogin;

                    data.CreatedBy = user.CreatedBy;
                    data.CreatedOn = user.CreatedOn;

                    data.ModifiedBy = userId;
                    data.ModifiedOn = DateTime.Now;

                    data.DeletedBy = userId;
                    data.DeletedOn = DateTime.Now;
                    data.IsDelete = true;
                }
            }
            catch(Exception ex)
            {
                response.message = ex.Message;
                response.success = false;
            } 
            return response;
        }
    }
}
