﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Med322a.DataModels
{
    [Keyless]
    public class DMPatientMemberDetail
    {

        public long UserId { get; set; }

        public long parentId { get; set; }

		public long customerId { get; set; }

		public long memberId { get; set; }

		public string Fullname { get; set; }

		public DateTime? Dob { get; set; }

		public string? Gender { get; set; }

		public long? BloodGroupId { get; set; }

		public string? RhesusType { get; set; }

		public decimal? Height { get; set; }

		public decimal? Weight { get; set; }

		public int Age { get; set; }

		public int? Appointment { get; set; }

		public int? chat { get; set; }

        //    // m customer member
        public long? CustomerMemberId { get; set; }

        public long? CostumerRelationId { get; set; }

		public string? RelationName { get; set; }

		public long CreatedBy { get; set; }

		public DateTime CreatedOn { get; set; }

		public long? ModifiedBy { get; set; }

		public DateTime? ModifiedOn { get; set; }

		public long? DeletedBy { get; set; }

		public DateTime? DeletedOn { get; set; }

		public bool IsDelete { get; set; }

	}
}
