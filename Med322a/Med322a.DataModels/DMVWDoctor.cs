﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Med322a.DataModels
{
	[Keyless]
	public class DMVWDoctor
	{
		public long? DoctorId { get; set; }

        public string? ImagePath { get; set; }

        public string? Fullname { get; set; }

		public string? SpecializationName { get; set; }

        public string? StartDate { get; set; }

        public string? EndDate { get; set; }

        public string? YearExperience { get; set; }

		public string? MedicalFacilityName { get; set; }

		public string? LocationName { get; set; }

		public string? TreatmentName { get; set; }
	}
}
