﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Med322a.ViewModels
{
    public class SelectedData
    {
        public int modifiedBy { get; set; }
        public int customerId { get; set; }
        public int memberId { get; set; }
        public int customerMemberId { get; set; }
        public int parentId { get; set; }
        public int userId { get; set; }
    }
}
