﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Med322a.ViewModels
{
    public class VMCariObat
    {
        public long Id { get; set; }
        [StringLength(50)]
        public string? Name { get; set; }

        public long? MedicalItemCategoryId { get; set; }
        public string? MedicalItemCategoryName { get; set; }

        public string? Composition { get; set; }

        public long? MedicalItemSegmentationId { get; set; }
        public string? MedicalItemSegmentationName { get; set; }

        [StringLength(100)]
        public string? Manufacturer { get; set; }
        public string? Indication { get; set; }
        public string? Dosage { get; set; }
        public string? Directions { get; set; }
        public string? Contraindication { get; set; }
        public string? Caution { get; set; }
        [StringLength(50)]
        public string? Packaging { get; set; }
        public long? PriceMax { get; set; }
        public long? PriceMin { get; set; }
        public byte[]? Image { get; set; }
        [StringLength(100)]
        public string? ImagePath { get; set; }

        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? DeletedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDelete { get; set; }
    }
}
