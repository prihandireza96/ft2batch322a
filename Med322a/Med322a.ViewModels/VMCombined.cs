﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Med322a.ViewModels
{
	public class VMCombined
	{
		public VMMRole RoleModel { get; set; }
		public List<VMPatientMember> PatientMembers { get; set; }
		public List<VMCancelAppointment> CancelAppointment { get; set; }
	}
}
