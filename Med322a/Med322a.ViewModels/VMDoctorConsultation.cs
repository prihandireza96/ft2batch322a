﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Med322a.ViewModels
{
	public class VMDoctorConsultation
	{
		public long UserId { get; set; }

		public long? DoctorId { get; set; }
	}
}
