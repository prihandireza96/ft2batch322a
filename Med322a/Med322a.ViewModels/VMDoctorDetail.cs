﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;

namespace Med322a.ViewModels
{
	public class VMDoctorDetail
	{
		public long? DoctorId { get; set; }

		[StringLength(255)]
		public string? ImagePath { get; set; }

		public IFormFile? ImageFile { get; set; }

		[StringLength(255)]
		public string? Fullname { get; set; }

		[StringLength(50)]
		public string? SpecializationName { get; set; }

        public DateTime? StartDate { get; set; }
  
        public DateTime? EndDate { get; set; }

        public string? YearExperience { get; set; }

        public string? TreatmentName { get; set; }

		public string? HistoryMedicalFacilityName { get; set; }

		public string? HistoryLocationName { get; set; }

		public string? MedicalFacilitySpecialization { get; set; }

		public string? StartYearWorkHistory { get; set; }

		public string? EndYearWorkHistory { get; set; }

		public string? InstitutionName { get; set; }

		public string? Major { get; set; }

		public string? EndYearEducation { get; set; }

        public string? MedicalFacilityId { get; set; }

        [StringLength(50)]
        public string? CurrentMedicalFacilityName { get; set; }

        [StringLength(50)]
        public string? CurrentMedicalFacilityCategory { get; set; }

        public string? FullAddress { get; set; }

        public decimal? ConsultationPrice { get; set; }

        public decimal? AppointmentPriceStartFrom { get; set; }

        [StringLength(10)]
        public string? Day { get; set; }

        [StringLength(10)]
        public string? TimeScheduleStart { get; set; }
 
        [StringLength(10)]
        public string? TimeScheduleEnd { get; set; }

		public string? MedicalFacilityCategoryId { get; set; }
	}
}
