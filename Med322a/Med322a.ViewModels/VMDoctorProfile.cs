﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;

namespace Med322a.ViewModels
{
	public class VMDoctorProfile
	{
		public long UserId { get; set; }

        public long DoctorId { get; set; }

        [StringLength(255)]
		public string? ImagePath { get; set; }

		public IFormFile? ImageFile { get; set; }

		[StringLength(255)]
		public string? Fullname { get; set; }

		[StringLength(50)]
		public string? SpecializationName { get; set; }

		[StringLength(50)]
		public string? Str { get; set; }

		public int? AppointmentCount { get; set; }

		public int? ConsultationCount { get; set; }

		public string? TreatmentName { get; set; }

		public string? MedicalFacilityName { get; set; }

		public string? LocationName { get; set; }

		public string? MedicalFacilitySpecialization { get; set; }

		public string? StartYearWorkHistory { get; set; }

		public string? EndYearWorkHistory { get; set; }

		public string? InstitutionName { get; set; }

		public string? Major { get; set; }

		public string? EndYearEducation { get; set; }
	}
}
