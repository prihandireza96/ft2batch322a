﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Med322a.ViewModels
{
	public class VMDoctorWorkHistory
	{
		public long UserId { get; set; }

		[StringLength(50)]
		public string? MedicalFacilityName { get; set; }

		[StringLength(100)]
		public string? LocationName { get; set; }

		[StringLength(100)]
		public string? MedicalFacilitySpecialization { get; set; }

		public DateTime? StartDateWorkHistory { get; set; }
		
		public DateTime? EndDateWorkHistory { get; set; }
	}
}
