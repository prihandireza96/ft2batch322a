﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Med322a.ViewModels
{
    public class VMMMenu
    {
        

        public long Id { get; set; }

        [StringLength(20)]
        public string? Name { get; set; } = null!;

        [StringLength(50)]
        public string? Url { get; set; } = null!;

        public long? ParentId { get; set; } = null!;

        [StringLength(100)]
        public string? BigIcon { get; set; }

        [StringLength(100)]
        public string? SmallIcon { get; set; }

        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? DeletedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDelete { get; set; }
    }
}
