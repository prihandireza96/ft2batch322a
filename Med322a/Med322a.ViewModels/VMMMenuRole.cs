﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Med322a.ViewModels
{
    public class VMMMenuRole
    {
        public long Id { get; set; }
        public long? MenuId { get; set; }
        public long? RoleId { get; set; }

        [StringLength(20)]
        public string? Name { get; set; }

        [StringLength(20)]
        public string? Code { get; set; }

		[StringLength(20)]
		public string? MenuName { get; set; }

		public long? ParentId { get; set; }

		public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? DeletedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDelete { get; set; }
    }
}
