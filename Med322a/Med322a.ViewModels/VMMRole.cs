﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Med322a.ViewModels
{
	public class VMMRole
	{

		public long Id { get; set; }

        public long? BiodataId { get; set; }

        [StringLength(20)]
		public string? Name { get; set; }

        public string? Fullname { get; set; }

        [StringLength(255)]
        public string? ImagePath { get; set; }

        [StringLength(20)]
		public string? Code { get; set; }

		public long CreatedBy { get; set; }
		public DateTime CreatedOn { get; set; }
		public long? ModifiedBy { get; set; }
		public DateTime? ModifiedOn { get; set; }
		public long? DeletedBy { get; set; }
		public DateTime? DeletedOn { get; set; }
		public bool IsDelete { get; set; }
	}
}
