﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Med322a.ViewModels
{
	public class VMPaging
	{
		public string orderby { get; set; }
		public string filter { get; set; }
		public int? pageNumber { get; set; }
		public int? showData { get; set; }
	}
}
