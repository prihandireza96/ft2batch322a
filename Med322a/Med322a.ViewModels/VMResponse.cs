﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Med322a.ViewModels
{
    public class VMResponse
    {
        public bool success { get; set; }
        public string message { get; set; }
        public object? data { get; set; }
        public VMResponse()
        {
            success = true;
            message = string.Empty;
            data = new object();
        }
    }
}
