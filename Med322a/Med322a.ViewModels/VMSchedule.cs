﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Med322a.ViewModels
{
	public class VMSchedule
	{
		public List<string> day { get; set; }
		public List<string> startTime { get; set; }
		public List<string> endTime { get; set; }

	}
}
