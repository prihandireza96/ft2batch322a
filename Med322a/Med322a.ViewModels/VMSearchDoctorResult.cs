﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Med322a.ViewModels
{
	public class VMSearchDoctorResult
	{
		public string specialization { get; set; }
		public string? location { get; set; }
		public string? fullname { get; set; }
		public string? treatment { get; set; }
	}
}
