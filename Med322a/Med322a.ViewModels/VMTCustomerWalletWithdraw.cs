﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Med322a.ViewModels
{
    public class VMTCustomerWalletWithdraw
    {
        public long Id { get; set; }
        public long? CustomerId { get; set; }
        public string? FullName { get; set; }
        public long? WalletDefaultNominalId { get; set; }
        public long? Nominal { get; set; }  
        public int? Amount { get; set; }
        [StringLength(50)]
        public string? BankName { get; set; }
        [StringLength(50)]
        public string? AccountNumber { get; set; }
        [StringLength(255)]
        public string? AccountName { get; set; }
        public int? Otp { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? DeletedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDelete { get; set; }
    }
}
