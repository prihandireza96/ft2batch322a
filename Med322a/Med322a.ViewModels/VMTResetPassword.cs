﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Med322a.ViewModels
{
    public class VMTResetPassword
    {
        public long Id { get; set; }

        [StringLength(255)]

        public string? OldPassword { get; set; }

        [StringLength(255)]
        [Unicode(false)]
        public string? NewPassword { get; set; }

        [StringLength(100)]
        [Unicode(false)]
        public string? ResetFor { get; set; }

        public long? CreatedBy { get; set; }
        [Column("created_on", TypeName = "datetime")]
        public DateTime CreatedOn { get; set; }

        public long? ModifiedBy { get; set; }
        [Column("modified_on", TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }

        public long? DeletedBy { get; set; }
        [Column("deleted_on", TypeName = "datetime")]
        public DateTime? DeletedOn { get; set; }

        public bool IsDelete { get; set; }
    }
}
