﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Med322a.ViewModels
{
	public class VMVWDoctor
	{
		public long? DoctorId { get; set; }

        public string? ImagePath { get; set; }

        public string? Fullname { get; set; }

		public string? SpecializationName { get; set; }

        public string? StartDate { get; set; }

        public string? EndDate { get; set; }

        public string? YearExperience { get; set; }

		public string? MedicalFacilityName { get; set; }

		public string? LocationName { get; set; }

		public string? TreatmentName { get; set; }

        [StringLength(10)]
        public string? Day { get; set; }

        [StringLength(10)]
        public string? TimeScheduleStart { get; set; }

        [StringLength(10)]
        public string? TimeScheduleEnd { get; set; }

        public string? MedicalFacilityCategoryId { get; set; }
    }
}
