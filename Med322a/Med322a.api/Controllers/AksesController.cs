﻿using Med322a.DataAccess;
using Med322a.DataModels;
using Med322a.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Med322a.api.Controllers
{

	[ApiController]
	[Route("api/[controller]")]
	public class AksesController : Controller
	{
		private DAAkses akses;

		public AksesController(DB_MEDContext _db)
		{
			akses = new DAAkses(_db);
		}

		[HttpGet("[action]")]
		public VMResponse GetAll() => akses.GetAll();

		[HttpGet("{id}")]
		public VMResponse Get(long id) => akses.Get(id);

		[HttpPost]
		public VMResponse Post(VMMRole role) => akses.CreateUpdate(role);

		[HttpPut]
		public VMResponse Put(VMMRole role) => akses.CreateUpdate(role);

		[HttpDelete("{id}/{userId}")]
		public VMResponse Delete(long id, long userId) => akses.Delete(id, userId);

        [HttpGet("[action]/{filter?}")]
        public VMResponse Get(string filter) => akses.GetByFilter(filter);
    }
}
