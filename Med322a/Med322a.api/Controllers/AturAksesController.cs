﻿using Med322a.DataAccess;
using Med322a.DataModels;
using Med322a.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Med322a.api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AturAksesController : Controller
	{
        private DAAturAkses aturakses;

        public AturAksesController(DB_MEDContext _db)
        {
            aturakses = new DAAturAkses(_db);
        }

        [HttpGet("[action]")]
        public VMResponse GetAll() => aturakses.GetAll();

        [HttpGet("[action]/{filter?}")]
        public VMResponse Get(string filter) => aturakses.GetByFilter(filter);

		[HttpGet("{id}")]
		public VMResponse Get(long id) => aturakses.Get(id);

		//[HttpPost]
		//public VMResponse Post(VMMMenuRole inputakses) => aturakses.CreateUpdate(inputakses);

		//[HttpPut]
		//public VMResponse Put(VMMMenuRole inputakses) => aturakses.CreateUpdate(inputakses);

	}
}
