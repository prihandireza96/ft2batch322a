﻿using Med322a.DataAccess;
using Med322a.DataModels;
using Med322a.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Med322a.api.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class BankController : Controller
	{
		private DABank bank;
		public BankController(DB_MEDContext _db) { bank = new DABank(_db); }
		[HttpGet("[action]/{id}")]
		public VMResponse Get(long id) => bank.Get(id);
		[HttpGet("{filter}")]
		public VMResponse Get(string filter) => bank.GetByFilter(filter);
		[HttpGet("[action]/{nBank}")]
		public VMResponse GetName(string nBank) => bank.GetByName(nBank);
		[HttpGet("[action]")]
		public VMResponse GetAll() => bank.GetAll();
		[HttpPost]
		public VMResponse post(VMMBank iBank) => bank.CreateUpdate(iBank);
		[HttpPut]
		public VMResponse put(VMMBank uBank) => bank.CreateUpdate(uBank);
		[HttpDelete("{id}/{bankID}")]
		public VMResponse Delete(long id, int bankID) => bank.Delete(id, bankID);
	}
}
