﻿using Med322a.DataAccess;
using Med322a.DataModels;
using Med322a.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Med322a.api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CancelAppointmentController : Controller
    {
        private readonly DACancelAppointment cancelAppointment;

        public CancelAppointmentController(DB_MEDContext db) => cancelAppointment = new DACancelAppointment(db);

        [HttpGet("{userId}")]
        public VMResponse? GetAll(int userId) => cancelAppointment.GetAll(userId);

		[HttpGet("[action]/{appointId}")]
		public VMResponse GetAppointmentId(int appointId) => cancelAppointment.GetAppointmentId(appointId);

		[HttpGet("[action]/{filter}/{userId}")]
        public VMResponse? GetFilter(string filter, int userId) => cancelAppointment.GetByFilter(filter, userId);

        [HttpDelete("[action]/{appoinid?}")]
        public VMResponse Delete(int appoinid) => cancelAppointment.Delete(appoinid);
    }
}
