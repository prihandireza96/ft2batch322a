﻿using Med322a.DataAccess;
using Med322a.DataModels;
using Med322a.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Med322a.api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CustomerRelationController : Controller
    {
        private readonly DACustomerRelation customerRelation;

        public CustomerRelationController(DB_MEDContext context) => customerRelation = new DACustomerRelation(context);

        [HttpGet]
        public VMResponse? GetALlRelation() => customerRelation.GetAll();
    }
}
