﻿using Med322a.DataAccess;
using Med322a.DataModels;
using Med322a.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Med322a.api.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class DoctorController : Controller
	{
		private DADoctor doctors;

		public DoctorController(DB_MEDContext _db) => doctors = new DADoctor(_db);

		[HttpGet("[action]")]
		public VMResponse GetAllSpecialization() => doctors.GetAllSpecialization();

        [HttpGet("[action]")]
        public VMResponse GetModalOption() => doctors.GetModalOption();

        [HttpGet("[action]/{specialization?}")]
        public VMResponse GetAllDoctorTreatment(string specialization) => doctors.GetAllDoctorTreatment(specialization);

        [HttpGet("{id?}")]
        public VMResponse GetSpecialization(long id) => doctors.GetSpecialization(id);

        [HttpGet("[action]/{filter?}")]
		public VMResponse GetSpecialization(string filter) => doctors.GetSpecialization(filter);

		[HttpGet("[action]/{userId?}")]
		public VMResponse GetProfile(long userId) => doctors.GetProfile(userId);

        [HttpGet("[action]/{doctorId?}")]
        public VMResponse GetDetail(long doctorId) => doctors.GetDetail(doctorId);

		[HttpPost]
		public VMResponse PostSpecialization(VMMSpecialization specialization) => doctors.CreateUpdateSpecialization(specialization);

        [HttpPost("[action]")]
        public VMResponse SearchDoctor(VMVWDoctor search) => doctors.SearchDoctor(search);

        [HttpPut]
		public VMResponse PutSpecialization(VMMSpecialization specialization) => doctors.CreateUpdateSpecialization(specialization);

		[HttpPut("[action]")]
		public VMResponse PutProfile(VMDoctorProfile profile) => doctors.CreateUpdateProfile(profile);

		[HttpDelete("{id}/{userId}")]
		public VMResponse DeleteSpecialization(long id, long userId) => doctors.DeleteSpecialization(id, userId);

	}
}
