﻿using Med322a.DataAccess;
using Med322a.DataModels;
using Med322a.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Med322a.api.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class DoctorTreatmentController : Controller
	{
		private readonly DADoctorTreatment treatment;

		public DoctorTreatmentController(DB_MEDContext db) => treatment = new DADoctorTreatment(db);

		[HttpGet("[action]/{doctorId}")]
		public VMResponse GetAllTreatment(int doctorId) => treatment.GetAllTreatment(doctorId);
	}
}
