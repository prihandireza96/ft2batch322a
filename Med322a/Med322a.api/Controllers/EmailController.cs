﻿using MailKit.Net.Smtp;
using MailKit.Security;
using Med322a.api.Services.EmailService;
using Med322a.DataModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MimeKit;
using MimeKit.Text;

namespace Med322a.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmailController : ControllerBase
    {
        private readonly IEmailService _emailService;
        public EmailController(IEmailService emailService)
        {
            _emailService = emailService;
        }

        [HttpPost]
        public IActionResult SendEmail (DMEmail emailRequest)
        {
            _emailService.SendEmail(emailRequest);
            return Ok();
        }
    }
}
