﻿using Med322a.DataAccess;
using Med322a.DataModels;
using Med322a.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Med322a.api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ForgotPasswordController : Controller
    {
        private DAForgotPassword forgotPassword;
        public ForgotPasswordController(DB_MEDContext _db)
        {
            forgotPassword = new DAForgotPassword(_db);
        }
        [HttpPost("[action]/{email?}")]
        public VMResponse TokenForgetPass(string email) => forgotPassword.TokenForgetPass(email);
        [HttpPost("[action]")]
        public VMResponse ForgetPassword(VMMUser inputdata) => forgotPassword.ForgetPassword(inputdata);
    }
}
