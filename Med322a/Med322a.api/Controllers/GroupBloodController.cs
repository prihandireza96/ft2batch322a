﻿using Med322a.DataAccess;
using Med322a.DataModels;
using Med322a.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Med322a.api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class GroupBloodController : Controller
    {
        private DABloodGroup DABloodGroup;

        public GroupBloodController(DB_MEDContext _db) => DABloodGroup = new DABloodGroup(_db);

        [HttpGet]
        public VMResponse GetAllBlood() => DABloodGroup.GetAllBlood();
    }
}
