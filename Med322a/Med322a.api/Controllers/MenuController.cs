﻿using Med322a.DataAccess;
using Med322a.DataModels;
using Med322a.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Med322a.api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MenuController : Controller
    {
        private DAMenu menu;
        public MenuController(DB_MEDContext _db)
        {
            menu = new DAMenu(_db);
        }

        [HttpGet("[action]")]
        public VMResponse GetAll() => menu.GetAll();

        [HttpGet("{id}")]
        public VMResponse Get(long id) => menu.Get(id);

        [HttpGet("[action]/{filter?}")]
        public VMResponse Get(string filter) => menu.GetByFilter(filter);

        [HttpPost]
        public VMResponse Post(VMMMenu akses) => menu.CreateUpdate(akses);

        [HttpPut]
        public VMResponse Put(VMMMenu akses) => menu.CreateUpdate(akses);
    }
}
