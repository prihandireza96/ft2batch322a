﻿using Med322a.DataAccess;
using Med322a.DataModels;
using Med322a.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Med322a.api.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class MenuRoleController : Controller
	{
		private DAMenuRole menuRole;
		public MenuRoleController(DB_MEDContext _db)
		{
			menuRole = new DAMenuRole(_db);
		}

		[HttpGet("[action]")]
		public VMResponse GetAll() => menuRole.GetAll();

		[HttpGet("{id}")]
		public VMResponse Get(long id) => menuRole.Get(id);

		[HttpGet("[action]/{filter?}")]
		public VMResponse Get(string filter) => menuRole.GetByFilter(filter);

        [HttpPost]
        public VMResponse Post(VMMMenuRole akses) => menuRole.CreateUpdate(akses);

        [HttpPut]
        public VMResponse Put(VMMMenuRole akses) => menuRole.CreateUpdate(akses);


    }
}
