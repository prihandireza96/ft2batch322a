﻿using Med322a.DataAccess;
using Med322a.DataModels;
using Med322a.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Med322a.api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ObatController : Controller
    {
        private DACariObat cariObat;
        public ObatController(DB_MEDContext _db) { cariObat = new DACariObat(_db); }
        [HttpGet("[action]/{id}")]
        public VMResponse Get(long id) => cariObat.Get(id);
        [HttpGet("[action]")]
        public VMResponse GetAll() => cariObat.GetAll();
        [HttpGet("[action]")]
        public VMResponse GetAllCat() => cariObat.GetAllCategory();
        [HttpGet("{filter}")]
        public VMResponse Get(string filter) => cariObat.GetByFilter(filter);
        [HttpPost]
        public VMResponse post(VMCariObat iObat) => cariObat.CreateUpdate(iObat);
        [HttpPut]
        public VMResponse put(VMCariObat uObat) => cariObat.CreateUpdate(uObat);
        [HttpDelete("{id}/{DelID}")]
        public VMResponse Delete(long id, int DelID) => cariObat.Delete(id, DelID);
        //public IActionResult Index()
        //{
        //    return View();
        //}
    }
}
