﻿
using Med322a.DataAccess;
using Med322a.DataModels;
using Med322a.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Med322a.api.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class PatientController : Controller
	{
		private DAPatient patient;
		
		public PatientController (DB_MEDContext _db) => patient = new DAPatient(_db);

		[HttpGet("[action]/{id}")]
		public VMResponse Get(int id) => patient.GetById(id);

        [HttpGet("[action]/{id}")]
        public VMResponse GetRoleByUserId(int id) => patient.GetRoleByUserId(id);

		[HttpGet("[action]/{id}")]
		public List<VMMUser> GetAllUser() => patient.GetAllUser();

		[HttpPut("[action]")]
		public VMResponse Edit(VMMUser user) => patient.EditById(user);

        [HttpPut("[action]")]
        public VMResponse EditEmailUser(VMMUser user) => patient.EditEmailUser(user);

        [HttpPut("[action]")]
        public VMResponse EditPasswordUser(VMMUser user) => patient.EditPasswordUser(user);

        [HttpPut("[action]")]
        public VMResponse EditImagesUser(VMMUser user) => patient.EditImagesUser(user);

        [HttpGet("[action]/{userId}/{oldPassword}")]
        public VMResponse CheckPassword(int userId, string oldPassword) => patient.CheckPassword(userId, oldPassword);
    }
}
