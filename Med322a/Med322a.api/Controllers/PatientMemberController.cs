﻿using Med322a.DataAccess;
using Med322a.DataModels;
using Med322a.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Med322a.api.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class PatientMemberController : Controller
    {
        private DAPatientDetail patientDetail;

        public PatientMemberController(DB_MEDContext _db) => patientDetail = new DAPatientDetail(_db);

        [HttpPost]
        public VMResponse? CreateMember(VMPatientMember patientMember) => patientDetail.Create(patientMember);
		
        [HttpGet("{userId}")]
		public VMResponse? GetPasienMemberByUserId(int userId) => patientDetail.GetPasienMemberByUserId(userId);

        [HttpGet("[action]/{biodataId}")]
        public VMResponse? GetEachPasienMember(int biodataId) => patientDetail.GetEachPasienMember(biodataId);

        [HttpPut("[action]")]
		public VMResponse? EditMember(VMPatientMember vMPatient) => patientDetail.EditPasienMember(vMPatient);

        [HttpDelete("[action]/{memberId}")]
        public VMResponse? Delete(int memberId) => patientDetail.Delete(memberId);

        [HttpGet("[action]/{filter}/{id}")]
        public VMResponse? GetFilter(string filter, int id) => patientDetail.GetByFilter(filter, id);
    }
}
