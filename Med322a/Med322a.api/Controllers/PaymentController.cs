﻿using Med322a.DataAccess;
using Med322a.DataModels;
using Med322a.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Med322a.api.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class PaymentController : Controller
	{
		private DABayar pay;
		public PaymentController(DB_MEDContext _db) { pay = new DABayar(_db); }
		[HttpGet("[action]")]
		public VMResponse GetAll() => pay.GetAll();
		[HttpGet("[action]/{id}")]
		public VMResponse Get(long id) => pay.Get(id);
		[HttpGet("[action]/{filter}")]
		public VMResponse GetByFilter(string filter) => pay.GetByFilter(filter);
        [HttpGet("[action]/{filter}")]
        public VMResponse GetName(string filter) => pay.GetName(filter);
        [HttpPost]
		public VMResponse post(VMMPaymentMethod iPay) => pay.CreateUpdate(iPay);
		[HttpPut]
		public VMResponse put(VMMPaymentMethod uPay) => pay.CreateUpdate(uPay);
		[HttpDelete("{id}/{DelID}")]
		public VMResponse Delete(long id, int DelID) => pay.Delete(id, DelID);
		//public IActionResult Index()
		//{
		//	return View();
		//}
	}
}
