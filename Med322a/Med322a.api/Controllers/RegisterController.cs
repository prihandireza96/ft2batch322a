﻿using Med322a.DataAccess;
using Med322a.DataModels;
using Med322a.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Med322a.api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RegisterController : Controller
    {
        private DARegister registers;
        public RegisterController(DB_MEDContext _db)
        {
            registers = new DARegister(_db);
        }
        [HttpPost("[action]/{email?}")]
        public VMResponse SaveToken(string email) => registers.SaveToken(email);
        [HttpGet("[action]/{email?}")]
        public VMResponse GetTokenByEmail(string email) => registers.GetTokenByEmail(email);
        [HttpPut("[action]")]
        public VMResponse UpdateToken(VMTToken inputdata) => registers.UpdateToken(inputdata);
        [HttpPut("[action]")]
        public VMResponse ResendToken(VMTToken token) => registers.ResendToken(token);
        [HttpGet("[action]")]
        public VMResponse GetAllRole() => registers.GetAllRole();
        [HttpPost("[action]")]
        public VMResponse Registry(VMMUser user) => registers.Registry(user);
    }
}
