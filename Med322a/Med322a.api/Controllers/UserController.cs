﻿using Med322a.DataAccess;
using Med322a.DataModels;
using Med322a.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Med322a.api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private DAUser users;
        public UserController(DB_MEDContext _db)
        {
            users = new DAUser(_db);
        }
        [HttpGet("[action]")]
        public VMResponse GetMenuView() => users.GetMenuView();
        [HttpGet("[action]")]
        public VMResponse GetAll() => users.GetAll();
        [HttpGet("[action]/{id?}")]
        public VMResponse Get(long id) => users.Get(id);
        [HttpGet("[action]/{email?}")]
        public VMResponse GetByEmail(string email) => users.GetByEmail(email);
        [HttpGet("[action]/{email?}")]
        public VMResponse GetByEmailAll(string email) => users.GetByEmailAll(email);
        [HttpPost("[action]")]
        public VMResponse Create(VMMUser user) => users.CreateUpdate(user);
        [HttpPut("[action]")]
        public VMResponse Update(VMMUser user) => users.CreateUpdate(user);
        [HttpPut("[action]")]
        public VMResponse UpdateForgetPassword(VMMUser user) => users.UpdateForgetPassword(user);
        [HttpDelete("[action]")]
        public VMResponse Delete(long id, int userId) => users.Delete(id, userId);
        [HttpPut("[action]")]
        public VMResponse UpdatePasswordSalah(VMMUser inputdata) => users.UpdatePasswordSalah(inputdata);
    }
}
