﻿using Med322a.DataAccess;
using Med322a.DataModels;
using Med322a.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Med322a.api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WalletController : Controller
    {
        private DANominal nom;
        public WalletController(DB_MEDContext _db) { nom = new DANominal(_db); }
        [HttpGet("[action]")]
        public VMResponse GetAll() => nom.GetAll();
        [HttpGet("[action]")]
        public VMResponse GetAmount() => nom.GetAmount();
        [HttpGet("[action]")]
        public VMResponse GetCustNominal() => nom.GetCustNominal();
        [HttpPost("[action]")]
        public VMResponse CustomNominal(VMTCustomerCustomNominal customNominal) => nom.CustomNominal(customNominal);
        [HttpPost("[action]")]
        public VMResponse PostAmount(VMTCustomerWalletWithdraw  withdraw) => nom.PostAmount(withdraw);
        //public IActionResult Index()
        //{
        //    return View();
        //}
    }
}
