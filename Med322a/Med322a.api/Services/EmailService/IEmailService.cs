﻿using Med322a.DataAccess;
using Med322a.DataModels;
using Med322a.ViewModels;
using Microsoft.AspNetCore.Mvc;
namespace Med322a.api.Services.EmailService
{
    public interface IEmailService
    {
        void SendEmail(DMEmail request);
    }
}
