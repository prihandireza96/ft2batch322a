﻿using Med322a.Models;
using Med322a.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;

namespace Med322a.Controllers
{
	public class AksesController : Controller
	{
		private readonly string apiurl;
		private readonly HttpClient httpclient = new HttpClient();
		private readonly int pageSize;

		public AksesController(IConfiguration _config)
		{
			apiurl = _config["ApiUrl"];
			pageSize = int.Parse(_config["pageSize"]);
		}


		public async Task<IActionResult> Index(VMPaging pageParam)
		{
			VMResponse? apiResponse = new VMResponse();

			try
			{
				if (pageParam.filter == null)
				{
					apiResponse = JsonConvert.DeserializeObject<VMResponse>(await httpclient.GetStringAsync(apiurl + "Akses/GetAll"));
				}

				else
				{
					apiResponse = JsonConvert.DeserializeObject<VMResponse>(await httpclient.GetStringAsync(apiurl + "Akses/Get/" + pageParam.filter));
				}


				if (apiResponse == null)
				{
					apiResponse.message = "Akses API Connection Error";
					apiResponse.success = false;

					return RedirectToAction("Index", "Home");
				}

				List<VMMRole> datarole = JsonConvert.DeserializeObject<List<VMMRole>>(apiResponse.data.ToString());

				ViewBag.Title = "Hak Akses";
				ViewBag.OrderId = string.IsNullOrEmpty(pageParam.orderby) ? "id_desc" : "";
				ViewBag.OrderName = (pageParam.orderby == "name") ? "name_desc" : "name";
				ViewBag.OrderCode = (pageParam.orderby == "code") ? "code_desc" : "code";

				ViewBag.orderBy = pageParam.orderby;
				ViewBag.Filter = pageParam.filter;

				switch (pageParam.orderby)
				{
					case "id_desc":
						datarole = datarole.OrderByDescending(role => role.Id).ToList();
						break;
					case "name":
						datarole = datarole.OrderBy(role => role.Name).ToList();
						break;
					case "name_desc":
						datarole = datarole.OrderByDescending(role => role.Name).ToList();
						break;
					case "code":
						datarole = datarole.OrderBy(role => role.Code).ToList();
						break;
					case "code_desc":
						datarole = datarole.OrderByDescending(role => role.Code).ToList();
						break;
					default:
						datarole = datarole.OrderBy(role => role.Id).ToList();
						break;

				}

				//return View(data);
				return View(PaginationModel<VMMRole>.CreateAsync(datarole, pageParam.pageNumber ?? 1, pageSize));
			}

			catch
			{
				apiResponse.message = "Role API Connection Error";
				apiResponse.success = false;

				return RedirectToAction("Index", "Home");
			}
		}


        public async Task<IActionResult> AddRole()
        {
            VMMRole data = new VMMRole();
            ViewBag.Title = "TAMBAH";
            return View(data);
        }


		[HttpPost]
        public async Task<VMResponse?> AddRole(VMMRole data)
		{
			VMResponse? apiresponse = new VMResponse();

            try
            {
                string jsonData = JsonConvert.SerializeObject(data);

                HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                apiresponse = JsonConvert.DeserializeObject<VMResponse>(await (
                    await httpclient.PostAsync($"{apiurl}Akses", content)
                ).Content.ReadAsStringAsync());

                // Check if API is null
                if (apiresponse == null)
                {
                    throw new ArgumentException("No Connection to Role API!");
                }

                // Check API Return Data
                if (!apiresponse.success || apiresponse.data == null)
                {
                    throw new ArgumentNullException("New Role data failed to be added!");
                }
                else
                {
                    HttpContext.Session.SetString("infoMsg", apiresponse.message);
                }
            }
            catch (Exception ex)
            {
                // Jika terjadi kesalahan proses
                apiresponse.success = false;
                apiresponse.message += "\n" + ex.Message;
                HttpContext.Session.SetString("errMsg", apiresponse.message);
            }

            return apiresponse;
        }


        public async Task<IActionResult> EditRole(long id)
        {
            VMMRole? data = new VMMRole();
            VMResponse? apiresponse = new VMResponse();

            try
            {
                 apiresponse = JsonConvert.DeserializeObject<VMResponse>
                    (await httpclient.GetStringAsync($"{apiurl}Akses/" + id)
                    );

				if (apiresponse == null)
				{
					throw new ArgumentException("No connection to Role API");
				}

				if (!apiresponse.success || apiresponse.data == null)
				{
					throw new ArgumentNullException(apiresponse.message);
				}
                else 
                {				
                    data = JsonConvert.DeserializeObject<VMMRole>(apiresponse.data.ToString());
                }
                
            }
            catch (Exception ex)
            {
				apiresponse.success = false;
				HttpContext.Session.SetString("errMsg", ex.Message);
			}
			ViewBag.Title = $"UBAH HAK AKSES";
			return View(data);
		}

        [HttpPost]
        public async Task<VMResponse?> EditRole(VMMRole data)
        {
            VMResponse? apiresponse = new VMResponse();

            try
            {
                string jsonData = JsonConvert.SerializeObject(data);
                HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                apiresponse = JsonConvert.DeserializeObject<VMResponse>
                (await (
                        await httpclient.PutAsync(apiurl + "Akses", content)
                    ).Content.ReadAsStringAsync());

                if (apiresponse == null)
                {
                    throw new ArgumentException($"No Connection to Role API!");
                }
                if (!apiresponse.success || apiresponse.data == null)
                {
                    apiresponse.success = false;
                    throw new ArgumentException($"Role with ID {data.Id} failed to be updated!");
                }
                else
                {
                    HttpContext.Session.SetString("infoMsg", apiresponse.message);
                }
            }
            catch (Exception ex)
            {
                apiresponse.success = false;
                apiresponse.message += "\n" + ex.Message;
                HttpContext.Session.SetString("errMsg", apiresponse.message);
            }
            return apiresponse;
        }

        public async Task<IActionResult> DeleteRole(long id)
        {
            VMMRole? data = new VMMRole();
            VMResponse? apiresponse = new VMResponse();

            try
            {
                apiresponse = JsonConvert.DeserializeObject<VMResponse>(
                    await httpclient.GetStringAsync($"{apiurl}Akses/" + id)
                );

                if (apiresponse == null)
                {
                    throw new ArgumentException("No connection to Role API");
                }

                if (!apiresponse.success || apiresponse.data == null)
                {
                    throw new ArgumentNullException(apiresponse.message);
                }
                else
                {
                    data = JsonConvert.DeserializeObject<VMMRole>(apiresponse.data.ToString());
                }
            }
            catch (Exception ex)
            {
                apiresponse.success = false;
                HttpContext.Session.SetString("errMsg", ex.Message);
            }

            ViewBag.Title = "HAPUS !";
            return View(data);
        }

        [HttpPost]
        public async Task<VMResponse?> DeleteRole(VMMRole data)
        {
            VMResponse? apiresponse = new VMResponse();

            try
            {
                string jsonData = JsonConvert.SerializeObject(data);


                apiresponse = JsonConvert.DeserializeObject<VMResponse>(await (
                    await httpclient.DeleteAsync(apiurl + "Akses/" + data.Id + "/" + data.DeletedBy)
                ).Content.ReadAsStringAsync());

                if (apiresponse == null)
                {
                    throw new ArgumentNullException("No Connection to Role Specialization API!");
                }

                if (!apiresponse.success || apiresponse.data == null)
                {
                    throw new ArgumentNullException($"Role data with id = {data.Id} failed to deleted!");
                }
                else
                {
                    HttpContext.Session.SetString("infoMsg", apiresponse.message);
                }
            }
            catch (Exception ex)
            {
                apiresponse.success = false;
                apiresponse.message += "\n" + ex.Message;
                HttpContext.Session.SetString("errMsg", apiresponse.message);
            }

            return apiresponse;
        }


    }
}
