﻿using Med322a.Models;
using Med322a.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Text;

namespace Med322a.Controllers
{
    public class BankController : Controller
    {
        private readonly string ApiUrl;
        private readonly HttpClient httpClient = new HttpClient();
        private readonly int pageSize;

        public BankController(IConfiguration _con)
        {
            ApiUrl = _con["ApiUrl"];
            pageSize = int.Parse(_con["PageSize"]);
        }
        public async Task<IActionResult> Index(VMPaging page)
        {
            VMResponse? apiResp = new VMResponse();
            List<VMMBank> data = new List<VMMBank>();
            try
            {
                if (page.filter == null)
                {
                    apiResp = JsonConvert.DeserializeObject<VMResponse>(await httpClient.GetStringAsync(ApiUrl + "Bank/GetAll"));
                }
                else
                {
                    apiResp = JsonConvert.DeserializeObject<VMResponse>(await httpClient.GetStringAsync(ApiUrl + "Bank/" + page.filter));
                }
                if (apiResp == null)
                {
                    apiResp.message = "Api Connection Error";
                    apiResp.success = false;

                    return RedirectToAction("Index", "Bank");
                }
                if (apiResp.success || apiResp.data != null)
                {
                    data = JsonConvert.DeserializeObject<List<VMMBank?>>(apiResp.data.ToString());
                    ViewBag.Title = "BANK";
                    //Get OrderBy Value
                    ViewBag.OrderName = (page.orderby == "name") ? "name_desc" : "name";
                    ViewBag.OrderVaCode = (page.orderby == "vacode") ? "vacode_desc" : "vacode";

                    ViewBag.OrderBy = page.orderby;
                    ViewBag.Filter = page.filter;

                    switch (page.orderby)
                    {
                        case "name":
                            data = data.OrderBy(bank => bank.Name).ToList();
                            break;
                        case "name_desc":
                            data = data.OrderByDescending(bank => bank.Name).ToList();
                            break;
                        case "vacode":
                            data = data.OrderBy(bank => bank.VaCode).ToList();
                            break;
                        case "vacode_desc":
                            data = data.OrderByDescending(bank => bank.VaCode).ToList();
                            break;
                    }
                }
                else
                {
                    throw new ArgumentNullException(apiResp.message);
                }
                //return View(data);
                HttpContext.Session.SetString("infoMsg", apiResp.message);
            }
            catch (Exception ex)
            {
                apiResp = new VMResponse(); 
                apiResp.success = false;
                HttpContext.Session.SetString("errMsg", ex.Message);

                return RedirectToAction("Index", "Bank");
            }
            return View(PaginationModel<VMMBank?>.CreateAsync(data, page.pageNumber ?? 1, pageSize));
        }
        public async Task<IActionResult> Add()
        {
            VMMBank? data = new VMMBank();
            ViewBag.Title = "Add New Bank";
            try
            {
                VMResponse? apiResp = JsonConvert.DeserializeObject<VMResponse>(await httpClient.GetStringAsync(ApiUrl + "Bank/GetAll/"));
                if(apiResp == null)
                {
                    throw new ArgumentException("No Connection");
                }
                if(apiResp.success || apiResp.data != null)
                {
                    HttpContext.Session.SetString("infoMsg", apiResp.message);
                    ViewBag.Bank = JsonConvert.DeserializeObject<List<VMMBank>>(apiResp.data.ToString());
                }
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("errMsg", ex.Message);
                return RedirectToAction("Index", "Bank");
            }
            return View(data);
        }
        [HttpPost]
        public async Task<VMResponse?> Add(VMMBank data)
        {
            VMResponse? apiResp = new VMResponse();
            try
            {
                string jsonData = JsonConvert.SerializeObject(data);
                HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                apiResp = JsonConvert.DeserializeObject<VMResponse>(await (await httpClient.PostAsync(ApiUrl + "Bank", content)).Content.ReadAsStringAsync());
                if(apiResp == null)
                {
                    throw new ArgumentException("Bank Api Can't Be Reached");
                }
                if (apiResp.success || apiResp.data != null)
                {
                    HttpContext.Session.SetString("infoMsg", apiResp.message);
                }
                else
                    throw new ArgumentNullException(apiResp.message);
            }
            catch (Exception ex)
            {
                apiResp = new VMResponse();
                HttpContext.Session.SetString("errMsg", ex.Message);
            }
            return apiResp;
        }
        public async Task<IActionResult> Update(long id)
        {
            VMMBank? data = new VMMBank();
            try
            {
                VMResponse? apiResp = JsonConvert.DeserializeObject<VMResponse>(await httpClient.GetStringAsync(ApiUrl + "Bank/Get/" + id));

                if (apiResp == null)
                {
                    throw new ArgumentException("No Connection");
                }

                if (!apiResp.success || apiResp.data == null)
                {
                    throw new ArgumentException("No Data");
                }
                else
                {
                    HttpContext.Session.SetString("infoMsg", apiResp.message);
                    data = JsonConvert.DeserializeObject<VMMBank>(apiResp.data.ToString());
                }
                ViewBag.Title = "Edit Bank";
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("errMsg", ex.Message);
                return RedirectToAction("Index", "Bank");
            }
            return View(data);
        }
        [HttpPost]
        public async Task<VMResponse?> Update(VMMBank data)
        {
            VMResponse? apiResp = new VMResponse();
            try
            {
                string jsonData = JsonConvert.SerializeObject(data);
                HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                apiResp = JsonConvert.DeserializeObject<VMResponse>(await (await httpClient.PutAsync(ApiUrl + "Bank", content)).Content.ReadAsStringAsync());
                if (apiResp == null)
                {
                    throw new ArgumentException("API Can't Be Reached");
                }
                if (apiResp.success || apiResp.data != null)
                {
                    HttpContext.Session.SetString("infoMsg", apiResp.message);
                }
                else
                {
                    throw new ArgumentException(apiResp.message);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("errMsg", ex.Message);
            }
            return apiResp;
        }
        public IActionResult Delete(long id)
        {
            ViewBag.Title = "Delete Bank";
            return View(id);
        }
        [HttpPost]
        public async Task<VMResponse?> Delete(VMMBank data)
        {
            VMResponse? apiResp = new VMResponse();
            try
            {
                string jsonData = JsonConvert.SerializeObject(data);
                HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                apiResp = JsonConvert.DeserializeObject<VMResponse>(await (await httpClient.DeleteAsync($"{ApiUrl}Bank/{data.Id}/{data.DeletedBy}")).Content.ReadAsStringAsync());
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("errMsg", ex.Message);
            }
            return apiResp;
        }
        [HttpPost]
        public async Task<VMResponse> Validasi(string nama)
        {
            try
            {
                // Periksa apakah data dengan nama yang sama sudah ada di dalam database
                VMResponse? response = JsonConvert.DeserializeObject<VMResponse?>(await httpClient.GetStringAsync(ApiUrl + $"Bank/GetName/{nama}"));
                if (response.data == null)
                {
                    return new VMResponse
                    {
                        success = false,
                        message = $"{nama} sudah ada dalam database."
                    };
                }
                else
                {
                    return new VMResponse
                    {
                        success = true,
                        message = $"{nama} belum ada dalam database."
                    };
                }
            }
            catch (Exception ex)
            {
                return new VMResponse
                {
                    success = false,
                    message = ex.Message
                };
            }
        }
            //var exists = _context.YourTableName.Any(item => item.ColumnName == data);
            //return Ok(exists);
            //response = JsonConvert.DeserializeObject<VMResponse?>(await httpClient.GetStringAsync(ApiUrl + $"Bank/GetName/{nama}"));

            //if (response.data == null)
            //{
            //    throw new ArgumentException($"{nama} is exist!");
            //}
            //else
            //{
            //    response.success = true;
            //    response.message = $"{nama} is not exist!";
            //}
        
    }
}
