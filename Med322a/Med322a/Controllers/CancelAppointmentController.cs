﻿using Med322a.DataModels;
using Med322a.ViewModels;
using Med322a.Views.Models.Med322.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net.Http;

namespace Med322a.Controllers
{
    public class CancelAppointmentController : Controller
    {
		private readonly string apiUrl;
		private readonly HttpClient httpClient = new HttpClient();
		private readonly int pageSize;

		public CancelAppointmentController(IConfiguration _config)
		{
			apiUrl = _config["ApiUrl"];
			pageSize = int.Parse(_config["PageSize"]);
		}

		public async Task<IActionResult> Index (VMPageParam vMPageParam)
        {
			ViewBag.biodataId = HttpContext.Session.GetString("userId") ?? String.Empty;
			ViewBag.userId = HttpContext.Session.GetString("userId") ?? String.Empty;
			long userId = long.Parse(HttpContext.Session.GetString("userId"));
			VMResponse? response = new VMResponse();
			VMResponse? appointmentResponse = new VMResponse();
			VMCombined? vMCombined = new VMCombined();
			VMMRole? userData = new VMMRole();

			try
			{
				//dptin userDatanya
				response = JsonConvert.DeserializeObject<VMResponse?>(
					await httpClient.GetStringAsync(apiUrl + "Patient/GetRoleByUserId/" + userId));
				if (response == null)
				{
					throw new ArgumentException($"No Patient with {userId} found!");
				}
				if (!response.success || response.data == null)
				{
					HttpContext.Session.SetString("errMsg", response.message);
					return RedirectToAction("Index", "Home");
				}
				else
				{
					userData = JsonConvert.DeserializeObject<VMMRole>(response.data.ToString());
					vMCombined.RoleModel = userData;
				}

				//dapatkan list appointmentnya
				if(vMPageParam.filter != null)
				{
					appointmentResponse = JsonConvert.DeserializeObject<VMResponse?>(
							await httpClient.GetStringAsync(apiUrl + "CancelAppointment/GetFilter/"
							+ vMPageParam.filter + "/" + userId));
				}
				else
				{
					appointmentResponse = JsonConvert.DeserializeObject<VMResponse?>(
							await httpClient.GetStringAsync(apiUrl + "CancelAppointment/" + userId));
				}

				if (appointmentResponse == null)
				{
					throw new ArgumentException($"No Appointment with {userId} found!");
				}
				if (!appointmentResponse.success || appointmentResponse.data == null)
				{
					HttpContext.Session.SetString("errMsg", appointmentResponse.message);
					return RedirectToAction("Index", "Home");
				}
				else
				{
					List<VMCancelAppointment> cancelAppointment = JsonConvert.DeserializeObject
						<List<VMCancelAppointment>>(appointmentResponse.data.ToString());
					vMCombined.CancelAppointment = cancelAppointment;
				}

				ViewBag.OrderID = string.IsNullOrEmpty(vMPageParam.orderby) ? "id_desc" : "";
				ViewBag.OrderName = (vMPageParam.orderby == "name") ? "name_desc" : "name";

				ViewBag.OrderBy = vMPageParam.orderby;
				ViewBag.Filter = vMPageParam.filter;
				ViewBag.UserRole = vMCombined.RoleModel;
				switch (vMPageParam.orderby)
				{
					case "name":
						vMCombined.CancelAppointment = vMCombined.CancelAppointment.OrderBy(product => product.Fullname).ToList();
						break;
					case "name_desc":
						vMCombined.CancelAppointment = vMCombined.CancelAppointment.OrderByDescending(product => product.Fullname).ToList();
						break;
					default:
						vMCombined.CancelAppointment = vMCombined.CancelAppointment.OrderBy(productdata => productdata.AppointmentId).ToList();
						break;
				}
			}
			catch (Exception ex)
			{
				HttpContext.Session.SetString("errMsg", ex.Message);
				return RedirectToAction("Index", "Home");
			}
			return View(PageView<VMCancelAppointment>.CreateAsync(vMCombined.CancelAppointment, vMPageParam.pageNumber ?? 1, pageSize));
		}

        public async Task<IActionResult> Edit(int id)
        {
            //VMCancelAppointment? apiTreatResponse = JsonConvert.DeserializeObject<VMCancelAppointment?>(await httpClient.GetStringAsync(apiUrl + "CancelAppointment/Get/" + id));
            //HttpContext.Session.SetString("DocID", apiTreatResponse.DoctorId.ToString() ?? "0");

            ////ViewBag.docId = int.Parse(HttpContext.Session.GetString("DocID"));
            List<VMDoctorTreatment> dataDT = new List<VMDoctorTreatment>();

            //VMDoctorTreatment? apiTreatNameResponse = new VMDoctorTreatment();
            try
            {

                VMResponse? apiResponse = JsonConvert.DeserializeObject<VMResponse?>(await httpClient.GetStringAsync(apiUrl + "CancelAppointment/GetAppointmentId/" + id));

                VMCancelAppointment? data = JsonConvert.DeserializeObject<VMCancelAppointment>(apiResponse.data.ToString());

                int x = (int)data.DoctorId;
                VMResponse? apiTreatNameResponse = JsonConvert.DeserializeObject<VMResponse?>(
					await httpClient.GetStringAsync(apiUrl + "DoctorTreatment/GetAllTreatment/" + x));

                dataDT = JsonConvert.DeserializeObject<List<VMDoctorTreatment>>(apiTreatNameResponse.data.ToString());


                //dataTreatment= JsonConvert.DeserializeObject<List<VMDoctorTreatment>>(apiTreatResponse.data.ToString());
                ViewBag.Treatment = JsonConvert.DeserializeObject<List<VMDoctorTreatment?>>(apiTreatNameResponse.data.ToString());


                //foreach (VMDoctorTreatment item in dataDT)
                //{
                //    if (data.DoctorTreatId == item.Id)
                //    {
                //        ViewBag.Treatment = item.Id;
                //    }
                //}

                return View(data);
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("errMsg", ex.Message);
                return View();
            }
        }

        public async Task<IActionResult> Delete(int Id)
        {
            VMResponse? apiResponse;
            apiResponse = JsonConvert.DeserializeObject<VMResponse>(await httpClient.GetStringAsync(apiUrl + "CancelAppointment/GetAppointmentId/" + Id));

            VMCancelAppointment relData = JsonConvert.DeserializeObject<VMCancelAppointment>(apiResponse.data.ToString());
            ViewBag.Title = "Batalkan Rencana Kedatangan";

            return View(relData);
        }

        [HttpPost]
        public async Task<VMResponse> Delete(VMCancelAppointment data)
        {
            VMResponse? apiResponse = new VMResponse();
            try
            {
                apiResponse = JsonConvert.DeserializeObject<VMResponse>(await (
                    await httpClient.DeleteAsync(apiUrl + "CancelAppointment/Delete/" + data.AppointmentId)).Content.ReadAsStringAsync());
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("errMsg", ex.Message);
            }
            HttpContext.Session.SetString("infoMsg", apiResponse.message);

            return apiResponse;
        }
    }
}
