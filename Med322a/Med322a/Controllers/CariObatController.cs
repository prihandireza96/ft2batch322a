﻿using Med322a.Models;
using Med322a.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Text;

namespace Med322a.Controllers
{
    public class CariObatController : Controller
    {
        private readonly string ApiUrl;
        private readonly HttpClient httpClient = new HttpClient();
        private readonly int pageSize;
        public CariObatController(IConfiguration _con)
        {
            ApiUrl = _con["ApiUrl"];
            pageSize = int.Parse(_con["PageSize"]);
        }
        //Stevvvvv
        public async Task<IActionResult> CariObat(VMPaging page)
        { 
            ViewBag.Title = "Cari Obat & Alat Kesehatan"; 
            VMCariObat data = new VMCariObat();
            ViewBag.Filter = page.filter;
            try
            {
                VMResponse? apiCatResp = JsonConvert.DeserializeObject<VMResponse>(await httpClient.GetStringAsync(ApiUrl + "Obat/GetAllCat"));
                if (apiCatResp == null)
                {
                    throw new ArgumentNullException("No Connnection");
                }
                if (apiCatResp.success || apiCatResp.data != null)
                {
                    HttpContext.Session.SetString("infoMsg", apiCatResp.message);
                    ViewBag.Kategori = JsonConvert.DeserializeObject<List<VMCatObat>>(apiCatResp.data.ToString());
                }
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("errMsg", ex.Message);
            }
            return View(data);
        }
        [HttpPost]
        public async Task<VMResponse?> CariObat(VMCariObat data, VMPaging page)
        {
            VMResponse? apiResp = new VMResponse();
            try
            {
                string jsonData = JsonConvert.SerializeObject(data);
                HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");
                apiResp = JsonConvert.DeserializeObject<VMResponse>(await httpClient.GetStringAsync(ApiUrl + "Obat/GetAll"));
                if (apiResp == null)
                {
                    throw new ArgumentException("Product API Can't Be Reached");
                }
                if (apiResp.success || apiResp.data != null)
                {
                    HttpContext.Session.SetString("infoMsg", apiResp.message);
                }
                else
                {
                    throw new ArgumentException(apiResp.message);
                }
            }
            catch (Exception ex)
            {
                apiResp = new VMResponse();
                HttpContext.Session.SetString("errMsg", ex.Message);
            }
            return apiResp;
        }
        public async Task<IActionResult> Index(VMPaging page)
        {
            VMResponse? apiResp = new VMResponse();
            List<VMCariObat> data = new List<VMCariObat>();
            try
            {
                //apiResp = JsonConvert.DeserializeObject<VMResponse>(await httpClient.GetStringAsync(ApiUrl + "Obat/GetAll"));
                //if (apiResp == null)
                //{
                //    apiResp.message = "Api Connection Error";
                //    apiResp.success = false;

                //    return RedirectToAction("Index", "Home");
                //}
                //if (apiResp.success || apiResp.data != null)
                //{
                    data = JsonConvert.DeserializeObject<List<VMCariObat>>(apiResp.data.ToString());
                    ViewBag.Title = "Hasil Pencarian";
                    ViewBag.Filter = page.filter;
                    ViewBag.OrderBy = page.orderby;

                //}
                //else
                //{
                //    throw new ArgumentNullException(apiResp.message);
                //}
                return View(data);
                //HttpContext.Session.SetString("infoMsg", apiResp.message);
                //return View(PaginationModel<VMCariObat>.CreateAsync(data, page.pageNumber ?? 1, pageSize));
            }
            catch (Exception ex)
            {
                apiResp = new VMResponse();
                apiResp.success = false;
                HttpContext.Session.SetString("errMsg", ex.Message);

                return RedirectToAction("Index", "CariObat");
            }
        }
    }
}
