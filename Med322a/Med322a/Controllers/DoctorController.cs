﻿using Med322a.Models;
using Med322a.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Drawing;
using System.Text;

namespace Med322a.Controllers
{
	public class DoctorController : Controller
	{
		private readonly string apiUrl;
		private readonly HttpClient httpClient = new HttpClient();
		private readonly IWebHostEnvironment webHostEnvironment;
		private readonly string imgFolder;
		private readonly int pageSize;

		public DoctorController(IConfiguration _config, IWebHostEnvironment _webHostEnvironment)
		{
			apiUrl = _config["ApiUrl"];
			imgFolder = _config["ImageFolder"];
			pageSize = int.Parse(_config["PageSize"]);

			webHostEnvironment = _webHostEnvironment;
		}

		public string? UploadFile(IFormFile file)
		{
			string? uniqueFileName = null;

			if (file != null)
			{
				uniqueFileName = string.Empty;
				// Generate unique ID for filename
				uniqueFileName = Guid.NewGuid().ToString() + "-" + file.FileName;

				string uploadFolder = webHostEnvironment.WebRootPath + "\\" + imgFolder + "\\" + uniqueFileName;

				// Upload Process
				using (FileStream fileStream = new FileStream(uploadFolder, FileMode.CreateNew))
				{
					file.CopyTo(fileStream);
				}
			}

			return uniqueFileName;
		}

		private void DeleteOldImage(string OldImageFileName)
		{
			try
			{
				string uploadFolder = webHostEnvironment.WebRootPath + "\\" + imgFolder + "\\" + OldImageFileName;
				if (System.IO.File.Exists(uploadFolder))
				{
					System.IO.File.Delete(uploadFolder);
				}
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		public async Task<IActionResult> SpecializationAsync(VMPaging pageParam, int pgSize = 5)
		{
			// Get Doctor Specialization Data
			VMResponse? apiResponse;
			List<VMMSpecialization>? data = new List<VMMSpecialization>();

            try
			{
				// Call Doctor Specialization API
				if (pageParam.filter == null)
					apiResponse = JsonConvert.DeserializeObject<VMResponse>(await httpClient.GetStringAsync($"{apiUrl}Doctor/GetAllSpecialization"));
				else
					apiResponse = JsonConvert.DeserializeObject<VMResponse>(await httpClient.GetStringAsync($"{apiUrl}Doctor/GetSpecialization/" + pageParam.filter));

				// Check if API is null
				if (apiResponse == null)
                {
                    apiResponse.success = false;
                    apiResponse.message = "No Connection to Doctor API!";
                    HttpContext.Session.SetString("errMsg", apiResponse.message);
                    return RedirectToAction("Index", "Home");
				}

				if (!apiResponse.success || apiResponse.data == null)
				{
                    apiResponse.success = false;
                    HttpContext.Session.SetString("errMsg", apiResponse.message);
                }
				else
				{
					data = JsonConvert.DeserializeObject<List<VMMSpecialization>>(apiResponse.data.ToString());
					ViewBag.Title = "SPESIALISASI";

                    // Get orderby value
                    ViewBag.OrderName = string.IsNullOrEmpty(pageParam.orderby) ? "name_desc" : "";
					ViewBag.Filter = pageParam.filter;
					ViewBag.OrderBy = pageParam.orderby;
					ViewBag.PgSize = pgSize;

					switch (pageParam.orderby)
					{
						case null:
							break;
						case "name_desc":
							data = data.OrderByDescending(x => x.Name).ToList();
							break;
						default:
                        {
                            apiResponse.success = false;
                            apiResponse.message = "Orderby error!";
                            HttpContext.Session.SetString("errMsg", apiResponse.message);
                            break;
                        }
					}

                    //return View(productData);
                    //HttpContext.Session.SetString("infoMsg", apiResponse.message);
				}
			}
			catch (Exception ex)
			{
				apiResponse = new VMResponse();
                apiResponse.success = false;
                apiResponse.message = "Doctor Specialization data failed to fetched!";
                HttpContext.Session.SetString("errMsg", apiResponse.message);
            }

            return View(PaginationModel<VMMSpecialization>.CreateAsync(data, pageParam.pageNumber ?? 1, pgSize));
        }

		public IActionResult AddSpecialization()
		{
			VMMSpecialization data = new VMMSpecialization();
			ViewBag.Title = "TAMBAH";
			return View(data);
		}

		[HttpPost]
		public async Task<VMResponse?> AddSpecializationAsync(VMMSpecialization data)
		{
			VMResponse? apiResponse = new VMResponse();
            VMResponse? apiResponseDuplicate = new VMResponse();
            bool duplicate = false;

            try
			{
				string jsonData = JsonConvert.SerializeObject(data);

				HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

				apiResponse = JsonConvert.DeserializeObject<VMResponse>(await (
					await httpClient.PostAsync($"{apiUrl}Doctor", content)
				).Content.ReadAsStringAsync());

				// Check if API is null
				if (apiResponse == null)
				{
                    apiResponse.success = false;
                    apiResponse.message = "No Connection to Doctor API!";
                    HttpContext.Session.SetString("errMsg", apiResponse.message);
                }

				// Check API Return Data
				if (!apiResponse.success || apiResponse.data == null)
				{
                    apiResponse.success = false;
                    HttpContext.Session.SetString("errMsg", apiResponse.message);
                }
				else
                {
                    HttpContext.Session.SetString("infoMsg", apiResponse.message);
                }
			}
			catch (Exception ex)
			{
				// Jika terjadi kesalahan proses
				apiResponse.success = false;
                apiResponse.message = (duplicate) ? $"Doctor Specialization data '{data.Name}' already exist!" : "Add Specialization failed to run!" ;
                HttpContext.Session.SetString("errMsg", apiResponse.message);
            }

			return apiResponse;
		}

		public async Task<IActionResult> EditSpecializationAsync(long id)
		{
			VMMSpecialization? data = new VMMSpecialization();
			VMResponse? apiResponse = new VMResponse();

			try
			{
				apiResponse = JsonConvert.DeserializeObject<VMResponse>(
					await httpClient.GetStringAsync($"{apiUrl}Doctor/" + id)
				);

				if (apiResponse == null)
				{
                    apiResponse.success = false;
                    apiResponse.message = "No Connection to Doctor API!";
                    HttpContext.Session.SetString("errMsg", apiResponse.message);
                }

				if (!apiResponse.success || apiResponse.data == null)
				{
                    apiResponse.success = false;
                    HttpContext.Session.SetString("errMsg", apiResponse.message);
                }
				else
				{
                    data = JsonConvert.DeserializeObject<VMMSpecialization>(apiResponse.data.ToString());
                }
			}
			catch (Exception ex)
			{
				// Jika terjadi kesalahan proses
                apiResponse.success = false;
                apiResponse.message = "Edit Specialization modal failed to fetched!";
                HttpContext.Session.SetString("errMsg", apiResponse.message);
            }

			ViewBag.Title = "UBAH";
			return View(data);
		}

		[HttpPost]
		public async Task<VMResponse?> EditSpecializationAsync(VMMSpecialization data)
		{
			VMResponse? apiResponse = new VMResponse();
			VMResponse? apiResponseDuplicate = new VMResponse();

			try
			{
				string jsonData = JsonConvert.SerializeObject(data);

				HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

				apiResponse = JsonConvert.DeserializeObject<VMResponse>(await (
					await httpClient.PutAsync(apiUrl + "Doctor", content)
				).Content.ReadAsStringAsync());

				if (apiResponse == null)
				{
                    apiResponse.success = false;
                    apiResponse.message = "No Connection to Doctor API!";
                    HttpContext.Session.SetString("errMsg", apiResponse.message);
                }

				if (!apiResponse.success || apiResponse.data == null)
				{
                    apiResponse.success = false;
                    HttpContext.Session.SetString("errMsg", apiResponse.message);
                }
				else
				{
					HttpContext.Session.SetString("infoMsg", apiResponse.message);
				}
			}
			catch (Exception ex)
			{
				// Jika terjadi kesalahan proses
				apiResponse.success = false;
				apiResponse.message = "Edit Specialization failed to run!";
				HttpContext.Session.SetString("errMsg", apiResponse.message);
			}
			return apiResponse;
		}

		public async Task<IActionResult> DeleteSpecializationAsync(long id)
		{
            VMMSpecialization? data = new VMMSpecialization();
            VMResponse? apiResponse = new VMResponse();

            try
            {
                apiResponse = JsonConvert.DeserializeObject<VMResponse>(
                    await httpClient.GetStringAsync($"{apiUrl}Doctor/" + id)
                );

                if (apiResponse == null)
                {
                    apiResponse.success = false;
                    apiResponse.message = "No Connection to Doctor API!";
                    HttpContext.Session.SetString("errMsg", apiResponse.message);
                }

                if (!apiResponse.success || apiResponse.data == null)
                {
                    apiResponse.success = false;
                    HttpContext.Session.SetString("errMsg", apiResponse.message);
                }
                else
                {
                    data = JsonConvert.DeserializeObject<VMMSpecialization>(apiResponse.data.ToString());
                }
            }
            catch (Exception ex)
            {
                // Jika terjadi kesalahan proses
                apiResponse.success = false;
                apiResponse.message = "Delete Specialization modal failed to fetched!";
                HttpContext.Session.SetString("errMsg", apiResponse.message);
            }

            ViewBag.Title = "HAPUS !";
			return View(data);
		}

		[HttpPost]
		public async Task<VMResponse?> DeleteSpecializationAsync(VMMSpecialization data)
		{
			VMResponse? apiResponse = new VMResponse();

			try
			{
				string jsonData = JsonConvert.SerializeObject(data);

				//HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

				apiResponse = JsonConvert.DeserializeObject<VMResponse>(await (
					await httpClient.DeleteAsync(apiUrl + "Doctor/" + data.Id + "/" + data.DeletedBy)
				).Content.ReadAsStringAsync());

				if (apiResponse == null)
				{
                    apiResponse.success = false;
                    apiResponse.message = "No Connection to Doctor API!";
                    HttpContext.Session.SetString("errMsg", apiResponse.message);
                }

				if (!apiResponse.success || apiResponse.data == null)
				{
                    apiResponse.success = false;
                    apiResponse.message = $"Doctor Specialization data with id = {data.Id} failed to deleted!";
                    HttpContext.Session.SetString("errMsg", apiResponse.message);
                }
				else
				{
					HttpContext.Session.SetString("infoMsg", apiResponse.message);
				}
			}
			catch (Exception ex)
			{
				// Jika terjadi kesalahan proses
				apiResponse.success = false;
				apiResponse.message = "Delete Specialization failed to run!";
				HttpContext.Session.SetString("errMsg", apiResponse.message);
			}

			return apiResponse;
		}

		public async Task<IActionResult> ProfileAsync()
		{
			VMResponse? apiResponse = new VMResponse();
			try
			{
				long userId, userRoleId;
				//long userId = long.Parse(HttpContext.Session.GetString("userRoleId"));
				try
				{
					userId = long.Parse(HttpContext.Session.GetString("userId"));
					userRoleId = long.Parse(HttpContext.Session.GetString("userRoleId"));
					if(userRoleId != 3)
					{
                        apiResponse.success = false;
                        apiResponse.message = "Please Login as Doctor First!";

                        HttpContext.Session.SetString("errMsg", apiResponse.message);
                    }
				}
				catch
                {
                    apiResponse.success = false;
                    HttpContext.Session.SetString("errMsg", "User Role ID failed to fetched!");
					return RedirectToAction("Index", "Home");
				}

				// Call Doctor Profile API
				apiResponse = JsonConvert.DeserializeObject<VMResponse>(await httpClient.GetStringAsync($"{apiUrl}Doctor/GetProfile/{userId}"));

				// Check if API is null
				if (apiResponse == null)
				{
					apiResponse.success = false;
					apiResponse.message = "Doctor API Connection Error!";
					HttpContext.Session.SetString("errMsg", apiResponse.message);
					return RedirectToAction("Index", "Home");
				}

				VMDoctorProfile? data = JsonConvert.DeserializeObject<VMDoctorProfile>(apiResponse.data.ToString());
				ViewBag.ImgFolder = imgFolder;
                ViewBag.UserId = userId;

				return View("_DokterProfileSharedLayout", data);
			}
			catch (Exception ex)
			{
				apiResponse.success = false;
				apiResponse.message += "Doctor Profile data failed to fetched!";
				HttpContext.Session.SetString("errMsg", apiResponse.message);
				return RedirectToAction("Index", "Home");
			}
		}

		public async Task<IActionResult> EditPhoto()
        {
            VMDoctorProfile? data = new VMDoctorProfile();
            VMResponse? apiResponse = new VMResponse();

            try
            {
				long userId = long.Parse(HttpContext.Session.GetString("userId")); ;
                apiResponse = JsonConvert.DeserializeObject<VMResponse>(
                    await httpClient.GetStringAsync($"{apiUrl}Doctor/GetProfile/" + userId)
                );

                if (apiResponse == null)
                {
                    apiResponse.success = false;
                    apiResponse.message = "No connection to Doctor API";

                    HttpContext.Session.SetString("errMsg", apiResponse.message);
                }

                if (!apiResponse.success || apiResponse.data == null)
                {
                    apiResponse.success = false;
                    HttpContext.Session.SetString("errMsg", apiResponse.message);
                }
                else
                {
                    data = JsonConvert.DeserializeObject<VMDoctorProfile>(apiResponse.data.ToString());
                }
            }
            catch (Exception ex)
            {
                // Jika terjadi kesalahan proses
                apiResponse.success = false;
                apiResponse.message = "Doctor Profile Photo failed to fetched!";

                HttpContext.Session.SetString("errMsg", apiResponse.message);
            }

            ViewBag.Title = "UBAH";
            ViewBag.ImgFolder = imgFolder;
			ViewBag.ImagePath = data.ImagePath;
            return View(data);
        }

		[HttpPost]
		public async Task<VMResponse?> EditPhoto(VMDoctorProfile biodata)
		{
			VMResponse? apiResponse = new VMResponse();

			try
			{
				biodata.UserId = long.Parse(HttpContext.Session.GetString("userId"));
				if (biodata.ImageFile != null)
				{
					// Delete Old Image file
					DeleteOldImage(biodata.ImagePath);

					// Upload Image File
					biodata.ImagePath = UploadFile(biodata.ImageFile);
					biodata.ImageFile = null;
				}

				// Convert Form Data to Json
				string jsonData = JsonConvert.SerializeObject(biodata);

				// Wrapping Json inside a HTML Body to be send as HTTP Put Request method
				HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

				// Process updated Product via API
				apiResponse = JsonConvert.DeserializeObject<VMResponse>(await (
					await httpClient.PutAsync($"{apiUrl}Doctor/PutProfile", content)
				).Content.ReadAsStringAsync());

				// Check API Connection
				if (apiResponse == null)
				{
					throw new ArgumentException("No connection to Doctor API");
				}

				// Check API Response
				if (!apiResponse.success || apiResponse.data == null)
				{
					throw new ArgumentNullException($"Doctor data with user id {biodata.UserId} is not exist!");
				}
				else
				{
					HttpContext.Session.SetString("infoMsg", $"Doctor data with user id {biodata.UserId} successfully edited!");
				}
			}
			catch (Exception ex)
			{
				// If there is error in process
				apiResponse.success = false;
				apiResponse.message = "Edit Photo Failed";
				HttpContext.Session.SetString("errMsg", apiResponse.message);
			}

			// Return API Response including Success, Message & Data
			return apiResponse;
		}

		public async Task<VMResponse?> GetAllDoctorTreatment(string specializationName)
        {
            VMResponse? apiResponse = new VMResponse();

            try
            {
                if (specializationName == null)
                {
                    apiResponse.success = false;
                    apiResponse.message = "No Specialization Name was found!";

                    HttpContext.Session.SetString("errMsg", apiResponse.message);
                }

                apiResponse = JsonConvert.DeserializeObject<VMResponse>(
                    await httpClient.GetStringAsync($"{apiUrl}Doctor/GetAllDoctorTreatment/{specializationName}")
                );

                if (apiResponse == null)
                {
                    apiResponse.success = false;
                    apiResponse.message = "Doctor API cannot be reached!";

                    HttpContext.Session.SetString("errMsg", apiResponse.message);
                }

                if (!apiResponse.success || apiResponse.data == null)
                {
                    apiResponse.success = false;
                    apiResponse.message = $"No Doctor Treatment data with Specialization Name = {specializationName} was found!";

                    HttpContext.Session.SetString("errMsg", apiResponse.message);
                }
                else
                {
                    apiResponse.data = JsonConvert.DeserializeObject<List<VMVWDoctor>>(
                        apiResponse.data.ToString()
                    );
                }
            }
            catch (Exception ex)
            {
                apiResponse.message = "Doctor Treatment data failed to fetched!";
                apiResponse.success = false;

                HttpContext.Session.SetString("errMsg", ex.Message);
            }

            return apiResponse;
        }

        public async Task<IActionResult> SearchDoctor()
        {
            VMVWDoctor data = new VMVWDoctor();

            try
            {
                VMResponse? apiModalResponse = JsonConvert.DeserializeObject<VMResponse>(await httpClient.GetStringAsync(apiUrl + "Doctor/GetModalOption"));

                if (apiModalResponse == null)
                {
                    apiModalResponse.success = false;
                    apiModalResponse.message = "No Connection to Doctor API!";
                    HttpContext.Session.SetString("errMsg", apiModalResponse.message);
                }

                if (!apiModalResponse.success || apiModalResponse.data == null)
                {
                    apiModalResponse.success = false;
                    HttpContext.Session.SetString("errMsg", apiModalResponse.message);
                }
                else
                {
                    ViewBag.Option = JsonConvert.DeserializeObject<VMVWDoctor?>(apiModalResponse.data.ToString());
                }
            }
            catch
            {
                HttpContext.Session.SetString("errMsg", "Search Doctor modal failed to fetched!");
            }
            ViewBag.Title = "Cari Dokter";
            return View(data);
        }

		[HttpPost]
		public async Task<IActionResult> SearchDoctor(VMVWDoctor doctor)
		{
			VMResponse? apiResponse = new VMResponse();
			try
			{
				// Convert Form Data to Json
				string jsonData = JsonConvert.SerializeObject(doctor);

				// Wrapping JSon inside an HTML Body to be send as HTTP Put Request method 
				HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

				// Process updated Product via API
				apiResponse = JsonConvert.DeserializeObject<VMResponse?>(
					await (await httpClient.PostAsync($"{apiUrl}Doctor/SearchDoctor", content)).Content.ReadAsStringAsync()
				);

				// Check API Connection
				if (apiResponse == null)
				{
                    apiResponse.success = false;
                    apiResponse.message = "No Connection to Doctor API!";
                    HttpContext.Session.SetString("errMsg", apiResponse.message);
					return RedirectToAction("Index", "Home");
				}

				// Check API Response
				if (!apiResponse.success || apiResponse.data == null)
				{
                    apiResponse.success = false;
                    HttpContext.Session.SetString("errMsg", apiResponse.message);
					return RedirectToAction("Index", "Home");
				}
			}
			catch (Exception ex)
			{
				apiResponse.success = false;
				apiResponse.message = "Doctor data failed to fetch!";
				HttpContext.Session.SetString("errMsg", ex.Message);
				return RedirectToAction("Index", "Home");
			}

			List<VMVWDoctor>? data = JsonConvert.DeserializeObject<List<VMVWDoctor>?>(apiResponse.data.ToString());

            // Pass all data into TempData
            VMPassData pass = new VMPassData();
            pass.pagination = new VMPaging();
            pass.searchKey = doctor;
            pass.listDoctor = data;
            pass.sort = "asc";
            pass.pageSize = 5;
            var passData = JsonConvert.SerializeObject(pass);
            TempData["passData"] = passData;

            return RedirectToAction("SearchDoctorResult", "Doctor");
		}

		public async Task<IActionResult> SearchDoctorResult()
        {
            VMPassData? data = JsonConvert.DeserializeObject<VMPassData>((string)TempData["passData"]);

            VMVWDoctor? doctor = data.searchKey;
            List<VMVWDoctor>? dataDoctor = data.listDoctor;
            VMPaging? pageParam = data.pagination;
            string sort = data.sort;
            int pgSize = data.pageSize;

            ViewBag.ImgFolder = imgFolder;
            ViewBag.Keyword = "Spesialisasi/Sub-spesialisasi : " + doctor.SpecializationName.Substring(7);
            if (doctor.LocationName != null)
                ViewBag.Keyword += ", Lokasi : " + doctor.LocationName;
            if (doctor.Fullname != null)
                ViewBag.Keyword += ", Nama Dokter mengandung kata '" + doctor.Fullname + "'";
            if (doctor.TreatmentName != null)
                ViewBag.Keyword += ", Tindakan Medis : " + doctor.TreatmentName;

            ViewBag.SpecializationName = doctor.SpecializationName;
            ViewBag.LocationName = doctor.LocationName;
            ViewBag.Fullname = doctor.Fullname;
            ViewBag.TreatmentName = doctor.TreatmentName;

            // Get orderby value
            ViewBag.Filter = pageParam.filter;
            ViewBag.OrderBy = pageParam.orderby;
            ViewBag.Sort = sort;
            ViewBag.PgSize = pgSize;

            return View(PaginationModel<VMVWDoctor>.CreateAsync(dataDoctor, pageParam.pageNumber ?? 1, pgSize));
        }

        [HttpPost]
        public async Task<IActionResult> SearchDoctorResult(VMPaging pageParam, VMVWDoctor doctor, string sort, int pgSize = 5)
        {
            // Get Doctor Specialization Data
            VMResponse? apiResponse = new VMResponse();

            try
            {
                // Convert Form Data to Json
                string jsonData = JsonConvert.SerializeObject(doctor);

                // Wrapping JSon inside an HTML Body to be send as HTTP Put Request method 
                HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                // Process updated Product via API
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(
                    await (await httpClient.PostAsync($"{apiUrl}Doctor/SearchDoctor", content)).Content.ReadAsStringAsync()
                );

                // Check API Connection
                if (apiResponse == null)
                {
                    apiResponse.success = false;
                    apiResponse.message = "No Connection to Doctor API!";
                    HttpContext.Session.SetString("errMsg", apiResponse.message);
                    return RedirectToAction("Index", "Home");
                }

                // Check API Response
                if (!apiResponse.success || apiResponse.data == null)
                {
                    apiResponse.success = false;
                    HttpContext.Session.SetString("errMsg", apiResponse.message);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    List<VMVWDoctor>? data = JsonConvert.DeserializeObject<List<VMVWDoctor>?>(apiResponse.data.ToString());
                    ViewBag.ImgFolder = imgFolder;
                    ViewBag.Keyword = "Spesialisasi/Sub-spesialisasi : " + doctor.SpecializationName.Substring(7);
                    if (doctor.LocationName != null)
                        ViewBag.Keyword += ", Lokasi : " + doctor.LocationName;
                    if (doctor.Fullname != null)
                        ViewBag.Keyword += ", Nama Dokter mengandung kata '" + doctor.Fullname + "'";
                    if (doctor.TreatmentName != null)
                        ViewBag.Keyword += ", Tindakan Medis : " + doctor.TreatmentName;

                    ViewBag.SpecializationName = doctor.SpecializationName;
                    ViewBag.LocationName = doctor.LocationName;
                    ViewBag.Fullname = doctor.Fullname;
                    ViewBag.TreatmentName = doctor.TreatmentName;

                    // Get orderby value
                    ViewBag.Filter = pageParam.filter;
                    ViewBag.OrderBy = pageParam.orderby;
                    ViewBag.Sort = sort;
                    ViewBag.PgSize = pgSize;

                    switch (pageParam.orderby)
                    {
                        case null:
                            if (sort == "desc") data = data.OrderByDescending(x => x.DoctorId).ToList();
                            else data = data.OrderBy(x => x.DoctorId).ToList();
                            break;
                        case "spesialisasi":
                            if (sort == "desc") data = data.OrderByDescending(x => x.SpecializationName).ToList();
                            else data = data.OrderBy(x => x.SpecializationName).ToList();
                            break;
                        case "lokasi":
							if(sort == "desc") data = data.OrderByDescending(x => x.LocationName).ToList();
							else data = data.OrderBy(x => x.LocationName).ToList();
                            break;
                        case "nama":
                            if (sort == "desc") data = data.OrderByDescending(x => x.Fullname).ToList();
                            else data = data.OrderBy(x => x.Fullname).ToList();
                            break;
                        case "pengalaman":
                            if (sort == "desc") data = data.OrderByDescending(x => x.YearExperience).ToList();
                            else data = data.OrderBy(x => x.YearExperience).ToList();
                            break;
                        default:
                            apiResponse.success = false;
                            apiResponse.message = "Orderby error!";
                            HttpContext.Session.SetString("errMsg", apiResponse.message);
                            return RedirectToAction("Index", "Home");
                    }

                    //return View(productData);
                    //HttpContext.Session.SetString("infoMsg", apiResponse.message);
                    return View(PaginationModel<VMVWDoctor>.CreateAsync(data, pageParam.pageNumber ?? 1, pgSize));
                }
            }
            catch (Exception ex)
            {
                apiResponse.success = false;
                apiResponse.message = "Doctor data failed to fetch!";
                HttpContext.Session.SetString("errMsg", ex.Message);
                return RedirectToAction("Index", "Home");
            }
        }

        public async Task<IActionResult> DetailsAsync(long doctorid)
        {
            VMResponse? apiResponse = new VMResponse();
            try
            {
				if(doctorid == null || doctorid < 0)
				{
                    apiResponse.message = "Doctor Id must be submitted!";
                    HttpContext.Session.SetString("errMsg", apiResponse.message);
                    return RedirectToAction("Index", "Home");
                }

                // Call Doctor Profile API
                apiResponse = JsonConvert.DeserializeObject<VMResponse>(await httpClient.GetStringAsync($"{apiUrl}Doctor/GetDetail/{doctorid}"));

                // Check if API is null
                if (apiResponse == null)
                {
                    apiResponse.success = false;
                    apiResponse.message = "Doctor API Connection Error!";
                    HttpContext.Session.SetString("errMsg", apiResponse.message);
                    return RedirectToAction("Index", "Home");
                }

                VMDoctorDetail? data = JsonConvert.DeserializeObject<VMDoctorDetail>(apiResponse.data.ToString());
				ViewBag.ImgFolder = imgFolder;

                return View("Details", data);
            }
            catch /*(Exception ex)*/
            {
                apiResponse.success = false;
                apiResponse.message = "Detail Doctor data failed to fetched!";
				//HttpContext.Session.SetString("errMsg", apiResponse.message);
				HttpContext.Session.SetString("errMsg", apiResponse.message);
				return RedirectToAction("Index", "Home");
            }
        }
    }
}
