﻿using System.Diagnostics;
using System.Text;
using Med322a.DataModels;
using Med322a.Models;
using Med322a.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.IO;
using Microsoft.AspNetCore.Http;

namespace Med322a.Controllers
{
    public class HomeController : Controller
    {
        //private readonly ILogger<HomeController> _logger;
        private readonly string apiUrl;
        private readonly HttpClient httpClient = new HttpClient();
        private readonly int pageSize;
        private readonly string imgFolder;

        public HomeController(IConfiguration _config)
        {
            //_logger = logger;
            apiUrl = _config["ApiUrl"];
            pageSize = int.Parse(_config["PageSize"]);
            imgFolder = _config["ImageFolder"];
        }

        [HttpGet]
		public async Task<IActionResult> Index()
        {
            ViewBag.UserRoleId = HttpContext.Session.GetString("userRoleId") ?? String.Empty;

            //List<VMMenuForView> data = new List<VMMenuForView>();
            //ViewBag.MenuForView = new List<VMMenuForView>();
            try
            {
                VMResponse? apiResponse = JsonConvert.DeserializeObject<VMResponse>(await httpClient.GetStringAsync(apiUrl + "User/GetMenuView"));
                //Cek koneksi ke API Menu
                if (apiResponse == null)
                {
                    throw new ArgumentException("Product API connection error!");
                }
                //Cek apakah ada Menu di database
                if (apiResponse.success || apiResponse.data != null)
                {
                    var menuForView = JsonConvert.DeserializeObject<List<VMMenuForView>>(apiResponse.data.ToString());

                    var serializedMenuData = JsonConvert.SerializeObject(menuForView);

                    //Data menu disimpan dalam session
                    HttpContext.Session.SetString("MenuForView", serializedMenuData);
                }
                else
                {
                    throw new ArgumentException(apiResponse.message);
                }
                ViewBag.ImgFolder = imgFolder;
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("errMsg", ex.Message);
            }

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Login()
        {
            try
            {
                VMMUser? data = new VMMUser();

                ViewBag.Title = "Masuk";

                return View(data);
            }
            catch(Exception ex)
            {
                HttpContext.Session.SetString("errMsg", ex.Message);
                return RedirectToAction("Index", "Home");
            }            
        }
        [HttpPost]
        public async Task<VMResponse> Login(VMMUser? data)
        {
            VMResponse? apiResponse = new VMResponse();
            try
            {
                apiResponse = JsonConvert.DeserializeObject<VMResponse>(await httpClient.GetStringAsync(apiUrl + $"User/GetByEmailAll/{data.Email}"));

                if (apiResponse == null)
                {
                    HttpContext.Session.SetString("errMsg", "No Connection to User API");
                    //return RedirectToAction("Index", "Home");
                }

                if (!apiResponse.success || apiResponse.data == null)
                {
                    throw new ArgumentException($"There's no User with Email {data.Email}!");
                }
                else
                {
                    VMMUser? user = JsonConvert.DeserializeObject<VMMUser>(apiResponse.data.ToString());

                    if (user.Email.Equals(data.Email) && user.Password.Equals(data.Password))
                    {
                        HttpContext.Session.SetString("userRoleId", user.RoleId.ToString());
                        HttpContext.Session.SetString("userId", user.Id.ToString());
                        HttpContext.Session.SetString("infoMsg", "Login Successfull");
                        //return RedirectToAction("Index", "Home");

                        HttpContext.Session.SetString("userFullName", user.Fullname.ToString());

                        user.ModifiedBy = user.BiodataId;

                        string jsonData = JsonConvert.SerializeObject(user);
                        HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                        apiResponse = JsonConvert.DeserializeObject<VMResponse>
                            (await (
                                await httpClient.PutAsync(apiUrl + "User/Update", content)
                            ).Content.ReadAsStringAsync());
                    }
                    else
                    {
                        user.LoginAttempt += 1;
                        user.ModifiedBy = user.BiodataId;

                        if (user.LoginAttempt >= 3)
                        {
                            user.IsLocked = true;
                        }

                        //VMResponse? updateresponse = new VMResponse();

                        string jsonData = JsonConvert.SerializeObject(user);
                        HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                        apiResponse = JsonConvert.DeserializeObject<VMResponse>
                            (await (
                                await httpClient.PutAsync(apiUrl + "User/UpdatePasswordSalah", content)
                            ).Content.ReadAsStringAsync());

                        throw new ArgumentException("Password Salah");
                    }
                }
                apiResponse.success = true;
                apiResponse.message = $"User with Email {data.Email} is exist!";
                //return RedirectToAction("Index", "Home");
                //return View(user);

            }
            catch (Exception ex)
            {
                apiResponse.success = false;
                apiResponse.message = ex.Message;
                //HttpContext.Session.SetString("errMsg", ex.Message);
                //return RedirectToAction("Index", "Home");
            }
            
            return apiResponse;
        }

        [HttpPost]
        public async Task<VMResponse> EmailAuth(string Email)
        {
            VMResponse? response = new VMResponse();
            try
            {
                response = JsonConvert.DeserializeObject<VMResponse?>(await httpClient.GetStringAsync(apiUrl + $"User/GetByEmailAll/{Email}"));

                //Cek apakah data dengan email input ada tidak
                if (response.data == null)
                {
                    throw new ArgumentException($"There's no User with Email {Email}");
                }
                else
                {
                    response.success = true;
                    response.message = $"User with Email {Email} is exist!";
                }

                //VMMUser? user = JsonConvert.DeserializeObject<VMMUser>(response.data.ToString());

                //if (user.Email.Equals(Email))
                //{
                //    response.success = true;
                //    response.message = $"User with Email {Email} is exist!";
                //}

            }
            catch (Exception ex)
            {
                response.success = false;
                response.message = ex.Message;
            }
            return response;
        }
        [HttpPost]
        public async Task<VMResponse> EmailAuthLocked(string Email)
        {
            VMResponse? response = new VMResponse();
            try
            {
                response = JsonConvert.DeserializeObject<VMResponse?>(await httpClient.GetStringAsync(apiUrl + $"User/GetByEmailAll/{Email}"));

                VMMUser userData = JsonConvert.DeserializeObject<VMMUser>(response.data.ToString());

                //Cek apakah data dengan email input ada tidak
                if (userData.IsLocked == true)
                {
                    throw new ArgumentException($"User with Email {Email} is locked");
                }
                else
                {
                    response.success = true;
                    response.message = $"User with Email {Email} isn't locked!";
                }

            }
            catch (Exception ex)
            {
                response.success = false;
                response.message = ex.Message;
            }
            return response;
        }

        [HttpPost]
        public async Task<VMResponse> PassAuth(string Email, string Password)
        {
            VMResponse? response = new VMResponse();
            try
            {
                response = JsonConvert.DeserializeObject<VMResponse?>(await httpClient.GetStringAsync(apiUrl + $"User/GetByEmailAll/{Email}"));

                if(response.data == null)
                {
                    throw new ArgumentException($"There's no User with Email {Email}");
                }

                VMMUser? user = JsonConvert.DeserializeObject<VMMUser>(response.data.ToString());

                if (user.Email.Equals(Email) && (/*Password == null || */user.Password.Equals(Password)))
                {
                    response.success = true;
                    response.message = $"Password is rigth";
                }
                else if (user.Email.Equals(Email) && !user.Password.Equals(Password))
                {
                    response.success = false;
                    response.message = "Password salah";

                    if(user.IsLocked == true)
                    {
                        throw new ArgumentException("Akun anda terkunci");
                    }
                }
            }
            catch(Exception ex)
            {
                response.success = false;
                response.message = ex.Message;
            }

            return response;
        }

        public IActionResult Logout()
        {
            HttpContext.Session.SetString("userRoleId", "");
            HttpContext.Session.SetString("userId", "");
            HttpContext.Session.SetString("userFullName", "");
            HttpContext.Session.SetString("biodataId", "");
            HttpContext.Session.SetString("warnMsg", "Logout Successfull");
            return RedirectToAction("Index", "Home");
        }

        public IActionResult Register()
        {
            try
            {
                VMMUser? data = new VMMUser();

                ViewBag.Title = "Daftar";

                return View(data);
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("errMsg", ex.Message);
                return RedirectToAction("Index", "Home");
            }
        }
        [HttpPost]
        public async Task<VMResponse> Register(VMMUser data)
        {
            VMResponse? apiresponse = new VMResponse();
            try
            {
                string jsonData = JsonConvert.SerializeObject(data.Email);

                HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                apiresponse = JsonConvert.DeserializeObject<VMResponse>(await (
                    await httpClient.PostAsync(apiUrl + $"Register/SaveToken/{data.Email}", content)
                ).Content.ReadAsStringAsync());

                // Check if API is null
                if (apiresponse == null)
                {
                    throw new ArgumentException("No Connection to Token API!");
                }

                // Check API Return Data
                if (!apiresponse.success || apiresponse.data == null)
                {
                    throw new ArgumentNullException("Failed to be register!");
                }
                else
                {
                    HttpContext.Session.SetString("infoMsg", apiresponse.message);
                    HttpContext.Session.SetString("email", data.Email.ToString());
                    //return RedirectToAction("TokenVerification", "Home");
                    apiresponse.success = true;
                    apiresponse.message = "Token succecssfully sended";
                }
            }
            catch(Exception ex)
            {
                apiresponse.success = false;
                apiresponse.message = ex.Message;
                //return RedirectToAction("Index", "Home");
            }
            return apiresponse;
        }


        [HttpPost]
        public async Task<VMResponse> EmailAuthRegister(string Email)
        {
            VMResponse? response = new VMResponse();
            try
            {
                response = JsonConvert.DeserializeObject<VMResponse?>(await httpClient.GetStringAsync(apiUrl + $"User/GetByEmailAll/{Email}"));

                if (response.data == null)
                {
                    response.success = true;
                    response.message = $"User with Email {Email} is not exist!";
                }
                else
                {
                    throw new ArgumentException($"Email {Email} is exist!");
                }
            }
            catch (Exception ex)
            {
                response.success = false;
                response.message = ex.Message;
            }
            return response; 
        }

        public IActionResult ForgotPassword()
        {
            try
            {
                VMMUser? data = new VMMUser();

                ViewBag.Title = "Lupa Password";

                return View(data);
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("errMsg", ex.Message);
                return RedirectToAction("Index", "Home");
            }
        }
        [HttpPost]
        public async Task<VMResponse> ForgotPassword(VMMUser data)
        {
            VMResponse? apiresponse = new VMResponse();
            try
            {
                string jsonData = JsonConvert.SerializeObject(data.Email);

                HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                apiresponse = JsonConvert.DeserializeObject<VMResponse>(await (
                    await httpClient.PostAsync(apiUrl + $"ForgotPassword/TokenForgetPass/{data.Email}", content)
                ).Content.ReadAsStringAsync());

                // Check if API is null
                if (apiresponse == null)
                {
                    throw new ArgumentException("No Connection to Token API!");
                }

                // Check API Return Data
                if (!apiresponse.success || apiresponse.data == null)
                {
                    throw new ArgumentNullException("Failed to send Token");
                }
                else
                {
                    HttpContext.Session.SetString("infoMsg", apiresponse.message);
                    HttpContext.Session.SetString("email", data.Email.ToString());
                    //return RedirectToAction("TokenVerification", "Home");
                    apiresponse.success = true;
                    apiresponse.message = "Token succecssfully sended";
                }
            }
            catch (Exception ex)
            {
                apiresponse.success = false;
                apiresponse.message = ex.Message;
                //return RedirectToAction("Index", "Home");
            }
            return apiresponse;
        }

        public IActionResult TokenVerification()
        {
            try
            {
                VMTToken? data = new VMTToken();

                ViewBag.Title = "Verifikasi E-mail";

                return View(data);
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("errMsg", ex.Message);
                return RedirectToAction("Index", "Home");
            }
        }
        [HttpPost]
        public async Task<VMResponse> TokenVerification(VMTToken data)
        {
            VMResponse? apiresponse = new VMResponse();
            try
            {
                apiresponse = JsonConvert.DeserializeObject<VMResponse>(await httpClient.GetStringAsync(apiUrl + $"Register/GetTokenByEmail/{data.Email}"));

                if (apiresponse == null)
                {
                    throw new ArgumentException("No Connection to Token API!");
                }

                if (!apiresponse.success || apiresponse.data == null)
                {
                    throw new ArgumentException($"There's no Token with Email {data.Email}!");
                }
                else
                {
                    VMTToken? token = JsonConvert.DeserializeObject<VMTToken?>(apiresponse.data.ToString());
                    if (token.Token.Equals(data.Token))
                    {
                        apiresponse.success = true;
                        apiresponse.message = "Kode OTP Benar";
                        HttpContext.Session.SetString("infoMsg", apiresponse.message);

                        //token.IsExpired = true;

                        //string jsonData = JsonConvert.SerializeObject(token);
                        //HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                        //apiresponse = JsonConvert.DeserializeObject<VMResponse>
                        //    (await (
                        //        await httpClient.PutAsync(apiUrl + "Register/UpdateToken", content)
                        //    ).Content.ReadAsStringAsync());
                    }
                    else
                    {
                        throw new ArgumentException("Kode OTP salah");
                    }
                }
                apiresponse.success = true;
                apiresponse.message = $"User with Email {data.Email} is exist!";

            }
            catch (Exception ex)
            {
                apiresponse.success = false;
                apiresponse.message = ex.Message;
            }
            return apiresponse;
        }
        public IActionResult TokenVerificationPassword()
        {
            try
            {
                VMTToken? data = new VMTToken();

                ViewBag.Title = "Verifikasi E-mail";

                return View(data);
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("errMsg", ex.Message);
                return RedirectToAction("Index", "Home");
            }
        }
        [HttpPost]
        public async Task<VMResponse> TokenVerificationPassword(VMTToken data)
        {
            VMResponse? apiresponse = new VMResponse();
            try
            {
                apiresponse = JsonConvert.DeserializeObject<VMResponse>(await httpClient.GetStringAsync(apiUrl + $"Register/GetTokenByEmail/{data.Email}"));

                if (apiresponse == null)
                {
                    throw new ArgumentException("No Connection to Token API!");
                }
                if (!apiresponse.success || apiresponse.data == null)
                {
                    throw new ArgumentException($"There's no Token with Email {data.Email}!");
                }
                else
                {
                    VMTToken? token = JsonConvert.DeserializeObject<VMTToken?>(apiresponse.data.ToString());
                    if (token.Token.Equals(data.Token))
                    {
                        apiresponse.success = true;
                        apiresponse.message = "Token successfully sended";
                        HttpContext.Session.SetString("infoMsg", apiresponse.message);
                    }
                    else
                    {
                        throw new ArgumentException("Kode OTP salah");
                    }
                }
                apiresponse.success = true;
                apiresponse.message = $"User with Email {data.Email} is exist!";

            }
            catch (Exception ex)
            {
                apiresponse.success = false;
                apiresponse.message = ex.Message;
            }
            return apiresponse;
        }
        [HttpPost]
        public async Task<VMResponse> TokenExpired()
        {
            VMResponse? response = new VMResponse();
            string Email = HttpContext.Session.GetString("email") ?? String.Empty;
            try
            {
                response = JsonConvert.DeserializeObject<VMResponse?>(await httpClient.GetStringAsync(apiUrl + $"Register/GetTokenByEmail/{Email}"));

                if (response.data == null)
                {
                    throw new ArgumentException($"There's no Token with Email {Email}");
                }
                else
                {
                    VMTToken? token = JsonConvert.DeserializeObject<VMTToken?>(response.data.ToString());
                    DateTime createdDate = token.CreatedOn;
                    //DateTime expiredDate = (DateTime)token.ExpiredOn;
                    DateTime currentDate = DateTime.Now;
                    TimeSpan timeDifference = currentDate - createdDate;
                    double timeDifferenceInMinute = timeDifference.TotalMinutes;

                    if (timeDifferenceInMinute >= 10)
                    {
                        token.IsExpired = true;

                        string jsonData = JsonConvert.SerializeObject(token);
                        HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                        response = JsonConvert.DeserializeObject<VMResponse>
                            (await (
                                await httpClient.PutAsync(apiUrl + "Register/UpdateToken", content)
                            ).Content.ReadAsStringAsync());

                        throw new ArgumentException("Kode OTP Kadaluarsa");
                    }
                    else
                    {
                        if (token.IsExpired == true)
                        {
                            response.success = false;
                            response.message = $"User with Email {Email} is expired!";
                        }
                        else
                        {
                            response.success = true;
                            response.message = $"User with Email {Email} is exist!";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.success = false;
                response.message = ex.Message;
            }
            return response;
        }

        [HttpPost]
        public async Task<VMResponse> ResendToken()
        {
            VMResponse? apiresponse = new VMResponse();
            string resetEmail = HttpContext.Session.GetString("email") ?? String.Empty;

            VMResponse? response = JsonConvert.DeserializeObject<VMResponse>(await httpClient.GetStringAsync(apiUrl + $"Register/GetTokenByEmail/{resetEmail}"));
            VMTToken? token = JsonConvert.DeserializeObject<VMTToken>(response.data.ToString());

            try
            {
                string jsonData = JsonConvert.SerializeObject(token);

                HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                apiresponse = JsonConvert.DeserializeObject<VMResponse>(await (
                    await httpClient.PutAsync(apiUrl + $"Register/ResendToken", content)
                ).Content.ReadAsStringAsync());

                // Check if API is null
                if (apiresponse == null)
                {
                    throw new ArgumentException("No Connection to Token API!");
                }

                // Check API Return Data
                if (!apiresponse.success || apiresponse.data == null)
                {
                    throw new ArgumentNullException("Failed to resend Token!");
                }
                else
                {
                    HttpContext.Session.SetString("infoMsg", apiresponse.message);

                    apiresponse.success = true;
                    apiresponse.message = "Token successfully sended";
                }
            }
            catch (Exception ex)
            {
                apiresponse.success = false;
                apiresponse.message = ex.Message;
                //return RedirectToAction("Index", "Home");
            }
            return apiresponse;
        }

        public IActionResult SetPassword()
        {
            try
            {
                VMMUser? data = new VMMUser();

                ViewBag.Title = "Set Password";

                return View(data);
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("errMsg", ex.Message);
                return RedirectToAction("Index", "Home");
            }
        }
        [HttpPost]
        public VMResponse SetPassword(VMMUser inputdata)
        {
            VMResponse? apiresponse = new VMResponse();
            try
            {
                if (inputdata == null)
                {
                    throw new ArgumentNullException("Failed to set password");
                }
                else
                {
                    apiresponse.success = true;
                    apiresponse.message = "Password successfully being set";

                    HttpContext.Session.SetString("password", inputdata.Password.ToString());
                    HttpContext.Session.SetString("infoMsg", apiresponse.message);

                    apiresponse.data = inputdata;
                }
            }
            catch(Exception ex)
            {
                apiresponse.success = false;
                apiresponse.message = ex.Message;
            }
            return apiresponse;
        }
        public IActionResult SetForgetPassword()
        {
            try
            {
                VMMUser? data = new VMMUser();

                ViewBag.Title = "Set Password";

                return View(data);
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("errMsg", ex.Message);
                return RedirectToAction("Index", "Home");
            }
        }
        [HttpPost]
        public async Task<VMResponse> SetForgetPassword(VMMUser data)
        {
            VMResponse? apiresponse = new VMResponse();
            try
            {
                VMResponse? response = JsonConvert.DeserializeObject<VMResponse?>(await httpClient.GetStringAsync(apiUrl + $"User/GetByEmailAll/{data.Email}"));
                VMMUser? user = JsonConvert.DeserializeObject<VMMUser>(response.data.ToString());

                user.Password = data.Password;

                string jsonData = JsonConvert.SerializeObject(user);

                HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                apiresponse = JsonConvert.DeserializeObject<VMResponse>(await(
                    await httpClient.PostAsync(apiUrl + $"ForgotPassword/ForgetPassword", content)
                ).Content.ReadAsStringAsync());

                if (apiresponse == null)
                {
                    throw new ArgumentException("No Connection to API!");
                } 

                if (!apiresponse.success || apiresponse.data == null)
                {
                    throw new ArgumentNullException("Failed to set password");
                }
                else
                {
                    response = JsonConvert.DeserializeObject<VMResponse>
                        (await (
                            await httpClient.PutAsync(apiUrl + "User/UpdateForgetPassword", content)
                        ).Content.ReadAsStringAsync());

                    apiresponse.success = true;
                    apiresponse.message = "Password successfully being resetted";

                    HttpContext.Session.SetString("email", "");
                    HttpContext.Session.SetString("infoMsg", apiresponse.message);
                }
            }
            catch (Exception ex)
            {
                apiresponse.success = false;
                apiresponse.message = ex.Message;
            }
            return apiresponse;
        }
        public async Task<IActionResult> RegisterAs()
        {
            try
            {
                VMMUser? data = new VMMUser();

                ViewBag.Role = new VMMRole();

                try
                {
                    VMResponse? apiRoleResponse = JsonConvert.DeserializeObject<VMResponse>(await httpClient.GetStringAsync(apiUrl + "Register/GetAllRole"));

                    if (apiRoleResponse == null)
                    {
                        throw new ArgumentException($"No Connection to Role API");
                    }
                    if (!apiRoleResponse.success || apiRoleResponse.data == null)
                    {
                        throw new ArgumentException($"No Role Data");
                    }
                    else
                    {
                        HttpContext.Session.SetString("infoMsg", apiRoleResponse.message);
                        ViewBag.Role = JsonConvert.DeserializeObject<List<VMMRole>>(apiRoleResponse.data.ToString());
                    }
                }
                catch (Exception ex)
                {
                    HttpContext.Session.SetString("errMsg", ex.Message);
                    return RedirectToAction("Index", "Home");
                }

                ViewBag.Title = "Daftar";

                return View(data);
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("errMsg", ex.Message);
                return RedirectToAction("Index", "Home");
            }
        }
        //[HttpPost]
        //public async Task<VMResponse> RegisterAs(VMMUser data)
        //{
        //    VMResponse? apiresponse = new VMResponse();
        //    //VMResponse? response = new VMResponse();
        //    try
        //    {
        //        // Check if API is null
        //        if (data == null)
        //        {
        //            throw new ArgumentException("No data to register!");
        //        }
        //        else
        //        {
        //            //await Task.Run(async () =>
        //            //{
        //            //    await BuatAkun(data);
        //            //    await GetDataByEmail(data.Email);
        //            //});
        //            await BuatAkun(data);
        //            //apiresponse = JsonConvert.DeserializeObject<VMResponse>((await BuatAkun(data)).data.ToString());
        //            var userData = await GetDataByEmail(data.Email);

        //            if (userData.data != null)
        //            {
        //                apiresponse.success = true;
        //                apiresponse.message = "Registration Successful";
        //            }
        //            else
        //            {
        //                apiresponse.success = false;
        //                apiresponse.message = "User data not found by email.";
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        apiresponse.success = false;
        //        apiresponse.message = ex.Message;
        //    }
        //    return apiresponse;
        //}
        [HttpPost]
        public async Task<VMResponse> RegisterAs(VMMUser data)
        {
            VMResponse? apiresponse = new VMResponse();
            //VMResponse? response = new VMResponse();
            try
            {
                string jsonData = JsonConvert.SerializeObject(data);

                HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                apiresponse = JsonConvert.DeserializeObject<VMResponse?>(await (
                    await httpClient.PostAsync(apiUrl + $"Register/Registry", content)
                ).Content.ReadAsStringAsync());

                //apiresponse = JsonConvert.DeserializeObject<VMResponse>((await BuatAkun(data)).data.ToString());  

                // Check if API is null
                if (apiresponse == null)
                {
                    throw new ArgumentException("No Connection to API!");
                }

                // Check API Return Data
                if (!apiresponse.success || apiresponse.data == null)
                {
                    throw new ArgumentNullException("Failed to register!");
                }
                else
                {
                    //VMResponse? response = JsonConvert.DeserializeObject<VMResponse>(await httpClient.GetStringAsync(apiUrl + $"User/GetByEmailAll/{data.Email}"));
                    //response = JsonConvert.DeserializeObject<VMResponse>(await httpClient.GetStringAsync(apiUrl + $"User/GetByEmailAll/{data.Email}"));

                    //VMMUser? userData = JsonConvert.DeserializeObject<VMMUser>(response.data.ToString());

                    //VMMUser? userData = JsonConvert.DeserializeObject<VMMUser?>((await GetDataByEmail(data.Email)).data.ToString());
                    //Bisa juga pakai yg pembuatan sama get dibuat masing-masing fungsi terus dipangggil kesini pakai await, jadi fungsinya ke-run satu-satu, kalau di java pakai Callback

                    VMMUser? userData = JsonConvert.DeserializeObject<VMMUser>(apiresponse.data.ToString());

                    HttpContext.Session.SetString("email", "");
                    HttpContext.Session.SetString("password", "");

                    HttpContext.Session.SetString("userRoleId", userData.RoleId.ToString());
                    HttpContext.Session.SetString("userId", userData.Id.ToString());
                    HttpContext.Session.SetString("userFullName", userData.Fullname.ToString());

                    apiresponse.success = true;
                    apiresponse.message = "Register Successfull";
                    HttpContext.Session.SetString("infoMsg", apiresponse.message);
                }
            }
            catch (Exception ex)
            {
                apiresponse.success = false;
                apiresponse.message = ex.Message;
            }
            return apiresponse;
        }
        [HttpGet]
        public async Task<VMResponse> GetDataByEmail(string email)
        {
            await Task.Delay(3000);
            VMResponse? response = new VMResponse();
            try
            {
                response = JsonConvert.DeserializeObject<VMResponse?>(await httpClient.GetStringAsync(apiUrl + $"User/GetByEmailAll/{email}"));

                if (response == null)
                {
                    throw new ArgumentException($"No Connection to Role API");
                }
                if (!response.success || response.data == null)
                {
                    throw new ArgumentException($"No User Data");
                    //int retries = 5;
                    //int count = 0;
                    //while(response.data == null && count++ < retries)
                    //{
                    //    response = new VMResponse();
                    //    response = JsonConvert.DeserializeObject<VMResponse>(await httpClient.GetStringAsync(apiUrl + $"User/GetByEmailAll/{email}"));
                    //}
                }
                else
                {
                    //VMMUser? userData = JsonConvert.DeserializeObject<VMMUser>(response.data.ToString());

                    //HttpContext.Session.SetString("email", "");
                    //HttpContext.Session.SetString("password", "");

                    //HttpContext.Session.SetString("userRoleId", userData.RoleId.ToString());
                    //HttpContext.Session.SetString("userId", userData.Id.ToString());
                    //HttpContext.Session.SetString("userFullName", userData.Fullname.ToString());

                    response.success = true;
                    response.message = $"Data User dengan email {email} Ada";
                    //HttpContext.Session.SetString("infoMsg", response.message);
                }
            }
            catch (Exception ex)
            {
                response.success = false;
                response.message = ex.Message;
            }
            return response;
        }
        [HttpPost]
        public async Task<VMResponse> BuatAkun(VMMUser data)
        {
            VMResponse? response = new VMResponse();
            try
            {
                string jsonData = JsonConvert.SerializeObject(data);

                HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                response = JsonConvert.DeserializeObject<VMResponse>(await (
                    await httpClient.PostAsync(apiUrl + $"Register/Registry", content)
                ).Content.ReadAsStringAsync());

                if (response == null)
                {
                    throw new ArgumentException($"No Connection to API");
                }
                if (!response.success || response.data == null)
                {
                    throw new ArgumentException($"Register unsuccessfull");
                }
                else
                {
                    response.success = true;
                    response.message = $"Registrasi data masuk ke database";
                }
            }
            catch (Exception ex)
            {
                response.success = false;
                response.message = ex.Message;
            }
            return response;
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public async Task <IActionResult> ProfileSharedLayout()
        {
			//ViewBag.userId = HttpContext.Session.GetString("userId") ?? String.Empty;
            ViewBag.UserId = HttpContext.Session.GetString("userId") ?? String.Empty;
            long userId = long.Parse(HttpContext.Session.GetString("userId"));
			VMResponse response = new VMResponse();
            VMMRole? userData = new VMMRole();
            //VMMenuForView? userData = new VMMenuForView();
            try
			{
				response = JsonConvert.DeserializeObject<VMResponse?>(
					await httpClient.GetStringAsync(apiUrl + "Patient/GetRoleByUserId/" + userId));
				if (response == null)
				{
					throw new ArgumentException($"No Patient with {userId} found!");
				}
				if (!response.success || response.data == null)
				{
					HttpContext.Session.SetString("errMsg", response.message);
					return RedirectToAction("Index", "Home");
				}
				else
				{
					//userData = JsonConvert.DeserializeObject<VMMRole>(response.data.ToString());
                    userData = JsonConvert.DeserializeObject<VMMRole>(response.data.ToString());
                    HttpContext.Session.SetString("biodataId", userData.BiodataId.ToString());
                    ViewBag.biodataId = HttpContext.Session.GetString("biodataId") ?? String.Empty;
                    if (userId == 2)
					{
						return View("_AdminProfileSharedLayout", userData);
					}
					else if (userId == 3)
					{
                        return View("_PatientProfileSharedLayout", userData);
                    }
					else
					{
						return RedirectToAction("Index", "Home");
					}

				}
			}
			catch (Exception ex)
			{
				HttpContext.Session.SetString("errMsg", ex.Message);
				return RedirectToAction("Index", "Home");
			}
		}
	}
}