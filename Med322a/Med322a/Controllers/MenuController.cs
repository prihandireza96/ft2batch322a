﻿using Med322a.Models;
using Med322a.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Med322a.Controllers
{
    public class MenuController : Controller
    {
        private readonly string apiurl;
        private readonly HttpClient httpclient = new HttpClient();
        private readonly int pageSize;

        public MenuController(IConfiguration _config)
        {
            apiurl = _config["ApiUrl"];
            pageSize = int.Parse(_config["pageSize"]);
        }

        public async Task<IActionResult> Index(VMPaging pageParam)
        {
            VMResponse? apiResponse = new VMResponse();

            try
            {
                if (pageParam.filter == null)
                {
                    apiResponse = JsonConvert.DeserializeObject<VMResponse>(await httpclient.GetStringAsync(apiurl + "Menu/GetAll"));
                }

                else
                {
                    apiResponse = JsonConvert.DeserializeObject<VMResponse>(await httpclient.GetStringAsync(apiurl + "Menu/Get/" + pageParam.filter));
                }


                if (apiResponse == null)
                {
                    apiResponse.message = "Menu API Connection Error";
                    apiResponse.success = false;

                    return RedirectToAction("Index", "Home");
                }

                List<VMMMenu> data = JsonConvert.DeserializeObject<List<VMMMenu>>(apiResponse.data.ToString());

                ViewBag.Title = "Menu";
                ViewBag.OrderId = string.IsNullOrEmpty(pageParam.orderby) ? "id_desc" : "";
                ViewBag.OrderName = (pageParam.orderby == "name") ? "name_desc" : "name";

                ViewBag.orderBy = pageParam.orderby;
                ViewBag.Filter = pageParam.filter;

                switch (pageParam.orderby)
                {
                    case "id_desc":
                        data = data.OrderByDescending(role => role.Id).ToList();
                        break;
                    case "name":
                        data = data.OrderBy(role => role.Name).ToList();
                        break;
                    case "name_desc":
                        data = data.OrderByDescending(role => role.Name).ToList();
                        break;
                    case "code":
                    default:
                        data = data.OrderBy(role => role.Id).ToList();
                        break;

                }

                //return View(data);
                return View(PaginationModel<VMMMenu>.CreateAsync(data, pageParam.pageNumber ?? 1, pageSize));
            }

            catch (Exception ex)
            {
                apiResponse.message = "Menu API Connection Error";
                apiResponse.success = false;

                return RedirectToAction("Index", "Home");
            }
        }
    }
}
