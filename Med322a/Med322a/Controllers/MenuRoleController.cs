﻿using Med322a.DataAccess;
using Med322a.Models;
using Med322a.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Text;

namespace Med322a.Controllers
{
	public class MenuRoleController : Controller
	{

        private readonly string apiurl;
        private readonly HttpClient httpclient = new HttpClient();
        private readonly int pageSize;

        public MenuRoleController(IConfiguration _config)
        {
            apiurl = _config["ApiUrl"];
            pageSize = int.Parse(_config["pageSize"]);
        }

        public async Task<IActionResult> IndexAtur(VMPaging pageParam)
        {
            VMResponse? apiResponse = new VMResponse();

            try
            {
                if (pageParam.filter == null)
                {
                    apiResponse = JsonConvert.DeserializeObject<VMResponse>(await httpclient.GetStringAsync(apiurl + "MenuRole/GetAll"));
                }

                else
                {
                    apiResponse = JsonConvert.DeserializeObject<VMResponse>(await httpclient.GetStringAsync(apiurl + "MenuRole/Get/" + pageParam.filter));
                }


                if (apiResponse == null)
                {
                    apiResponse.message = "Akses API Connection Error";
                    apiResponse.success = false;

                    return RedirectToAction("IndexAtur", "Home");
                }

                List<VMMMenuRole> dataAturAkses = JsonConvert.DeserializeObject<List<VMMMenuRole>>(apiResponse.data.ToString());

                ViewBag.Title = "PENGATURAN AKSES";
                ViewBag.OrderId = string.IsNullOrEmpty(pageParam.orderby) ? "id_desc" : "";
                ViewBag.OrderName = (pageParam.orderby == "name") ? "name_desc" : "name";
                ViewBag.OrderCode = (pageParam.orderby == "code") ? "code_desc" : "code";

                ViewBag.orderBy = pageParam.orderby;
                ViewBag.Filter = pageParam.filter;

                switch (pageParam.orderby)
                {
                    case "id_desc":
                        dataAturAkses = dataAturAkses.OrderByDescending(role => role.Id).ToList();
                        break;
                    case "name":
                        dataAturAkses = dataAturAkses.OrderBy(role => role.Name).ToList();
                        break;
                    case "name_desc":
                        dataAturAkses = dataAturAkses.OrderByDescending(role => role.Name).ToList();
                        break;
                    case "code":
                        dataAturAkses = dataAturAkses.OrderBy(role => role.Code).ToList();
                        break;
                    case "code_desc":
                        dataAturAkses = dataAturAkses.OrderByDescending(role => role.Code).ToList();
                        break;
                    default:
                        dataAturAkses = dataAturAkses.OrderBy(role => role.Id).ToList();
                        break;

                }

                //return View(data);
                return View(PaginationModel<VMMMenuRole>.CreateAsync(dataAturAkses, pageParam.pageNumber ?? 1, pageSize));
            }

            catch
            {
                apiResponse.message = "Role API Connection Error";
                apiResponse.success = false;

                return RedirectToAction("IndexAtur", "Home");
            }
        }


		public async Task<IActionResult> AddAkses()
		{
			VMMMenuRole data = new VMMMenuRole();
			return View(data);
		}

		[HttpPost]
		public async Task<VMResponse?> AddAkses(VMMMenuRole data)
		{
			VMResponse? apiresponse = new VMResponse();

			try
			{
				string jsonData = JsonConvert.SerializeObject(data);

				HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

				apiresponse = JsonConvert.DeserializeObject<VMResponse>(await (
					await httpclient.PostAsync($"{apiurl}MenuRole", content)
				).Content.ReadAsStringAsync());

				if (apiresponse == null)
				{
					throw new ArgumentException("No Connection to Akses API!");
				}

				if (!apiresponse.success || apiresponse.data == null)
				{
					throw new ArgumentNullException("New Akses data failed to be added!");
				}
				else
				{
					HttpContext.Session.SetString("infoMsg", apiresponse.message);
				}
			}
			catch (Exception ex)
			{
				apiresponse.success = false;
				apiresponse.message += "\n" + ex.Message;
				HttpContext.Session.SetString("errMsg", apiresponse.message);
			}

			return apiresponse;
		}


		public async Task<IActionResult> EditAkses(long id)
		{
			VMMMenuRole? data = new VMMMenuRole();
			VMResponse? apiresponse = new VMResponse();

			try
			{
				apiresponse = JsonConvert.DeserializeObject<VMResponse>
				   (await httpclient.GetStringAsync($"{apiurl}MenuRole/" + id)
				   );

				if (apiresponse == null)
				{
					throw new ArgumentException("No connection to Akses API");
				}

				if (!apiresponse.success || apiresponse.data == null)
				{
					throw new ArgumentNullException(apiresponse.message);
				}
				else
				{
					data = JsonConvert.DeserializeObject<VMMMenuRole>(apiresponse.data.ToString());
				}

			}
			catch (Exception ex)
			{
				apiresponse.success = false;
				HttpContext.Session.SetString("errMsg", ex.Message);
			}
			ViewBag.Title = $"UBAH AKSES";
			return View(data);
		}

		[HttpPost]
		public async Task<VMResponse?> EditAkses(VMMMenuRole data)
		{
			VMResponse? apiresponse = new VMResponse();

			try
			{
				string jsonData = JsonConvert.SerializeObject(data);
				HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

				apiresponse = JsonConvert.DeserializeObject<VMResponse>
				(await (
						await httpclient.PutAsync(apiurl + "MenuRole", content)
					).Content.ReadAsStringAsync());

				if (apiresponse == null)
				{
					throw new ArgumentException($"No Connection to Akses API!");
				}
				if (!apiresponse.success || apiresponse.data == null)
				{
					apiresponse.success = false;
					throw new ArgumentException($"Akses with ID {data.Id} failed to be updated!");
				}
				else
				{
					HttpContext.Session.SetString("infoMsg", apiresponse.message);
				}
			}
			catch (Exception ex)
			{
				apiresponse.success = false;
				apiresponse.message += "\n" + ex.Message;
				HttpContext.Session.SetString("errMsg", apiresponse.message);
			}
			return apiresponse;
		}
	}
}
