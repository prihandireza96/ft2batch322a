﻿using Med322a.ViewModels;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Newtonsoft.Json;
using System.Globalization;
using System.Net.Http;
using System.Text;
using Med322a.DataAccess;
using Med322a.DataModels;

namespace Med322a.Controllers
{
	public class PatientController : Controller
	{
        private readonly string apiUrl;
        private readonly HttpClient client = new HttpClient();
        private readonly string imgFolder;
        private readonly IWebHostEnvironment env;

        public PatientController(IConfiguration configuration, IWebHostEnvironment env)
        {
            apiUrl = configuration["ApiUrl"];
            imgFolder = configuration["ImageFolder"];
            this.env = env;
        }

        private void DeleteOldImage(string oldImageFileName)
        {
            try
            {
                string uploadFolder = env.WebRootPath + "\\" + imgFolder + "\\" + oldImageFileName;

                if (System.IO.File.Exists(uploadFolder))
                {
                    System.IO.File.Delete(uploadFolder);
                }
                //else
                //{
                //    throw new Exception("File Doesn't Exist!");
                //}
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("errMsg", ex.Message);
            }
        }

        public string UploadFile(IFormFile file)
        {
            string uniquefileName = string.Empty;

            if (file != null)
            {
                //Generate unique ID for filename
                uniquefileName = Guid.NewGuid().ToString() + "-" + file.FileName;
                //where to save the image ? worry not.. here is the place
                string uploadFolder = env.WebRootPath + "\\" + imgFolder + "\\" + uniquefileName;

                //uploading
                using (FileStream fileStream = new FileStream(uploadFolder, FileMode.CreateNew))
                {
                    file.CopyTo(fileStream);
                }
            }

            return uniquefileName;
        }

        public IActionResult Index()
		{
			return View();
		}

		public async Task <IActionResult> ProfileTab(long id)
        {
            ViewBag.userId = HttpContext.Session.GetString("userId") ?? String.Empty;
            VMResponse response = new VMResponse();
			VMMUser? userData = new VMMUser();

			try
			{
                response = JsonConvert.DeserializeObject<VMResponse?>(
					await client.GetStringAsync(apiUrl + "Patient/Get/" + id));
                if (response == null)
                {
                    throw new ArgumentException($"No Patient with {id} found!");
                }
                if (!response.success || response.data == null)
                {
                    HttpContext.Session.SetString("errMsg", response.message);
                }
                else
                {
                    HttpContext.Session.SetString("infoMsg", $"Patient data with id = {id} successfully fetched!");
                    userData = JsonConvert.DeserializeObject<VMMUser>(response.data.ToString());
                }
            }
            catch (Exception ex)
			{
                HttpContext.Session.SetString("errMsg", ex.Message);
            }
            return View(userData);
		}

        public async Task<IActionResult> Edit(long id)
        {
            ViewBag.userId = HttpContext.Session.GetString("userId") ?? String.Empty;
            VMResponse response = new VMResponse();
            VMMUser? userData = new VMMUser();

            try
            {
                response = JsonConvert.DeserializeObject<VMResponse?>(
                    await client.GetStringAsync(apiUrl + "Patient/Get/" + id));
                if (response == null)
                {
                    throw new ArgumentException($"No Patient with {id} found!");
                }
                if (!response.success || response.data == null)
                {
                    HttpContext.Session.SetString("errMsg", response.message);
                }
                else
                {
                    HttpContext.Session.SetString("infoMsg", $"Patient data with id = {id} successfully fetched!");
                    userData = JsonConvert.DeserializeObject<VMMUser>(response.data.ToString());
                }
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("errMsg", ex.Message);
            }
            return View(userData);
        }

        [HttpPost]
        public async Task<VMResponse?> Edit(VMMUser user)
        {
            VMResponse? apiResponse;
            try
            {
                // Convert Form Data to Json
                string jsonData = JsonConvert.SerializeObject(user);

                // Wrapping JSon inside an HTML Body to be send as HTTP Put Request method 
                HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                // Process updated Product via API
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(
                    await (await client.PutAsync($"{apiUrl}Patient/Edit", content)).Content.ReadAsStringAsync()
                );

                // Check API Connection
                if (apiResponse == null)
                {
                    throw new ArgumentException("No Connection to Product API!");
                }

                // Check API Response
                if (!apiResponse.success || apiResponse.data == null)
                    throw new ArgumentException(apiResponse.message);
            }
            catch (Exception ex)
            {
                apiResponse = new VMResponse();
                apiResponse.success = false;
                apiResponse.message = ex.Message;

                HttpContext.Session.SetString("errMsg", ex.Message);
            }

            return apiResponse;

        }

        public async Task<IActionResult> EditEmailUser(long id)
        {
            ViewBag.userId = HttpContext.Session.GetString("userId") ?? String.Empty;
            VMResponse response = new VMResponse();
            VMMUser? userData = new VMMUser();

            try
            {
                response = JsonConvert.DeserializeObject<VMResponse?>(
                    await client.GetStringAsync(apiUrl + "Patient/Get/" + id));
                if (response == null)
                {
                    throw new ArgumentException($"No Patient with {id} found!");
                }
                if (!response.success || response.data == null)
                {
                    HttpContext.Session.SetString("errMsg", response.message);
                }
                else
                {
                    HttpContext.Session.SetString("infoMsg", $"Patient data with id = {id} successfully fetched!");
                    userData = JsonConvert.DeserializeObject<VMMUser>(response.data.ToString());
                }
			}
            catch (Exception ex)
            {
                HttpContext.Session.SetString("errMsg", ex.Message);
            }
            return View(userData);
        }

        [HttpPost]
        public JsonResult ValidateEmail(string email)
        {
            using (var dbContext = new DB_MEDContext()) // Inisialisasi konteks basis data
            {
                bool isEmailExists = dbContext.MUsers.Any(user => user.Email == email);
                return Json(new { exists = isEmailExists });
            }
        }

        [HttpPost]
        public async Task<VMResponse?> EditEmailUser (VMMUser user)
        {
            VMResponse? apiResponse;
            try
            {
                // Convert Form Data to Json
                string jsonData = JsonConvert.SerializeObject(user);

                // Wrapping JSon inside an HTML Body to be send as HTTP Put Request method 
                HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                // Process updated Product via API
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(
                    await (await client.PutAsync($"{apiUrl}Patient/EditEmailUser", content)).Content.ReadAsStringAsync()
                );

                // Check API Connection
                if (apiResponse == null)
                {
                    throw new ArgumentException("No Connection to Product API!");
                }

                // Check API Response
                if (!apiResponse.success || apiResponse.data == null)
                    throw new ArgumentException(apiResponse.message);
            }
            catch (Exception ex)
            {
                apiResponse = new VMResponse();
                apiResponse.success = false;
                apiResponse.message = ex.Message;

                HttpContext.Session.SetString("errMsg", ex.Message);
            }

            return apiResponse;
        }

        public async Task<IActionResult> EditPasswordUser(int id)
        {
            ViewBag.userId = HttpContext.Session.GetString("userId") ?? String.Empty;
            
            return View(id);
        }

        [HttpPost]
        public async Task<VMResponse?> EditNewPasswordUser(VMMUser user)
        {
            VMResponse? apiResponse;
            try
            {
                // Convert Form Data to Json
                string jsonData = JsonConvert.SerializeObject(user);

                // Wrapping JSon inside an HTML Body to be send as HTTP Put Request method 
                HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                // Process updated Product via API
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(
                    await (await client.PutAsync($"{apiUrl}Patient/EditPasswordUser", content)).Content.ReadAsStringAsync()
                );

                // Check API Connection
                if (apiResponse == null)
                {
                    throw new ArgumentException("No Connection to Product API!");
                }

                // Check API Response
                if (!apiResponse.success || apiResponse.data == null)
                    throw new ArgumentException(apiResponse.message);
            }
            catch (Exception ex)
            {
                apiResponse = new VMResponse();
                apiResponse.success = false;
                apiResponse.message = ex.Message;

                HttpContext.Session.SetString("errMsg", ex.Message);
            }

            return apiResponse;
        }

        public async Task<IActionResult> EditNewPasswordUser (int id)
        {
            ViewBag.userId = HttpContext.Session.GetString("userId") ?? String.Empty;
            VMResponse response = new VMResponse();
            VMMUser? userData = new VMMUser();

            try
            {
                response = JsonConvert.DeserializeObject<VMResponse?>(
                    await client.GetStringAsync(apiUrl + "Patient/Get/" + id));
                if (response == null)
                {
                    throw new ArgumentException($"No Patient with {id} found!");
                }
                if (!response.success || response.data == null)
                {
                    HttpContext.Session.SetString("errMsg", response.message);
                }
                else
                {
                    HttpContext.Session.SetString("infoMsg", $"Patient data with id = {id} successfully fetched!");
                    userData = JsonConvert.DeserializeObject<VMMUser>(response.data.ToString());
                }
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("errMsg", ex.Message);
            }
            return View(userData);
        }

        public async Task <IActionResult> EditImages (long id)
        {
            ViewBag.userId = HttpContext.Session.GetString("userId") ?? String.Empty;
            VMResponse response = new VMResponse();
            VMMUser? userData = new VMMUser();

            try
            {
                response = JsonConvert.DeserializeObject<VMResponse?>(
                    await client.GetStringAsync(apiUrl + "Patient/Get/" + id));
                if (response == null)
                {
                    throw new ArgumentException($"No Patient with {id} found!");
                }
                if (!response.success || response.data == null)
                {
                    HttpContext.Session.SetString("errMsg", response.message);
                }
                else
                {
                    HttpContext.Session.SetString("infoMsg", $"Patient data with id = {id} successfully fetched!");
                    userData = JsonConvert.DeserializeObject<VMMUser>(response.data.ToString());
                }
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("errMsg", ex.Message);
            }
            return View(userData);
        }

        [HttpPost]
        public async Task<VMResponse?> EditImagesUser(VMMUser user)
        {
            VMResponse? apiResponse;
            try
            {
                if (user.imageFile != null)
                {
                    //Delete Old Image file
                    DeleteOldImage(user.ImagePath);

                    //Upload New Image file
                    user.ImagePath = UploadFile(user.imageFile);
                    user.imageFile = null;
                }
                // Convert Form Data to Json
                string jsonData = JsonConvert.SerializeObject(user);

                // Wrapping JSon inside an HTML Body to be send as HTTP Put Request method 
                HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                // Process updated Product via API
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(
                    await (await client.PutAsync($"{apiUrl}Patient/EditImagesUser", content)).Content.ReadAsStringAsync()
                );

                // Check API Connection
                if (apiResponse == null)
                {
                    throw new ArgumentException("No Connection to Product API!");
                }

                // Check API Response
                if (!apiResponse.success || apiResponse.data == null)
                    throw new ArgumentException(apiResponse.message);
            }
            catch (Exception ex)
            {
                apiResponse = new VMResponse();
                apiResponse.success = false;
                apiResponse.message = ex.Message;

                HttpContext.Session.SetString("errMsg", ex.Message);
            }

            return apiResponse;

        }

        [HttpPost]
        public JsonResult CheckOldPassword(long userId, string oldPassword)
        {
            using (var dbContext = new DB_MEDContext()) // Inisialisasi konteks basis data
            {
                bool isPasswordCorrect = dbContext.MUsers.Any(user => user.Id == userId && user.Password == oldPassword);
                return Json(new { exists = isPasswordCorrect });
            }
        }

        //[HttpPost]
        //public async Task<VMResponse?> CheckPassword (string oldPassword)
        //{
        //    ViewBag.userId = HttpContext.Session.GetString("userId") ?? String.Empty;
        //    long userId = long.Parse(HttpContext.Session.GetString("userId"));
        //    VMResponse response = new VMResponse();
        //    VMMUser? userData = new VMMUser();

        //    try
        //    {
        //        response = JsonConvert.DeserializeObject<VMResponse?>(
        //            await client.GetStringAsync(apiUrl + "Patient/CheckPassword/" + userId + "/" + oldPassword));
        //        if (response == null)
        //        {
        //            throw new ArgumentException($"No Patient with {userId} found!");
        //        }
        //        if (!response.success || response.data == null)
        //        {
        //            HttpContext.Session.SetString("errMsg", response.message);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        HttpContext.Session.SetString("errMsg", ex.Message);
        //    }
        //    return (response);
        //}

        //     public async Task<VMResponse?> SearchDoctor ()
        //     {
        //VMTblMCategory data = new VMTblMCategory();
        //ViewBag.Title = "Cari Dokter";
        //return View(data);
        //     }
    }
}
