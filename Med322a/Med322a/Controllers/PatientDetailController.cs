﻿using Med322a.DataAccess;
using Med322a.DataModels;
using Med322a.ViewModels;
using Med322a.Views.Models.Med322.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Net.Http;
using System.Text;

namespace Med322a.Controllers
{
    public class PatientDetailController : Controller
    {
        private readonly string apiUrl;
        private readonly HttpClient httpClient = new HttpClient();
        private readonly int pageSize;

        public PatientDetailController (IConfiguration _config)
        {
            apiUrl = _config["ApiUrl"];
            pageSize = int.Parse(_config["PageSize"]);
        }

        //public async Task<IActionResult> Index()
        //{
        //    ViewBag.biodataId = HttpContext.Session.GetString("userId") ?? String.Empty;
        //    ViewBag.userId = HttpContext.Session.GetString("userId") ?? String.Empty;
        //    long userId = long.Parse(HttpContext.Session.GetString("userId"));
        //    VMResponse response = new VMResponse();
        //    VMMRole? userData = new VMMRole();

        //    try
        //    {
        //        response = JsonConvert.DeserializeObject<VMResponse?>(
        //            await httpClient.GetStringAsync(apiUrl + "Patient/GetRoleByUserId/" + userId));
        //        if (response == null)
        //        {
        //            throw new ArgumentException($"No Patient with {userId} found!");
        //        }
        //        if (!response.success || response.data == null)
        //        {
        //            HttpContext.Session.SetString("errMsg", response.message);
        //            return RedirectToAction("Index", "Home");
        //        }
        //        else
        //        {
        //            userData = JsonConvert.DeserializeObject<VMMRole>(response.data.ToString());
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        HttpContext.Session.SetString("errMsg", ex.Message);
        //        return RedirectToAction("Index", "Home");
        //    }
        //    return View(userData);
        //}

        public async Task<IActionResult> ViewIndex(VMPageParam vMPageParam)
        {
            ViewBag.biodataId = HttpContext.Session.GetString("biodataId") ?? String.Empty;
            ViewBag.userId = HttpContext.Session.GetString("userId") ?? String.Empty;
            int showData = vMPageParam.showData ?? this.pageSize;
            long userId = long.Parse(HttpContext.Session.GetString("userId"));
            VMResponse? response = new VMResponse();
            VMResponse? patientMemberResponse = new VMResponse();
            VMCombined? vMCombined = new VMCombined();
            VMMRole? userData = new VMMRole();

            try
            {
                response = JsonConvert.DeserializeObject<VMResponse?>(
                    await httpClient.GetStringAsync(apiUrl + "Patient/GetRoleByUserId/" + userId));
                //cek apakah response mendapatkan data dari API Role User
                if (response == null)
                {
                    throw new ArgumentException($"No Patient with {userId} found!");
                }
                if (!response.success || response.data == null)
                {
                    HttpContext.Session.SetString("errMsg", response.message);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    userData = JsonConvert.DeserializeObject<VMMRole>(response.data.ToString());
                    vMCombined.RoleModel = userData;
                }

                //cek filter untuk searching. Keadaan awal filter = null
                if (vMPageParam.filter == null)
                {
                    patientMemberResponse = JsonConvert.DeserializeObject<VMResponse?>(
				    	await httpClient.GetStringAsync(apiUrl + "PatientMember/" + userId));
                } else
                {
                    patientMemberResponse = JsonConvert.DeserializeObject<VMResponse?>(
                        await httpClient.GetStringAsync(apiUrl + "PatientMember/GetFilter/" + vMPageParam.filter + "/" + userId));
                }

                //cek apakah patient member data didapatkan
                if (patientMemberResponse == null)
				{
					throw new ArgumentException($"No member with biodata ID = {userId} found!");
				}

				if (!patientMemberResponse.success || patientMemberResponse.data == null)
				{
					HttpContext.Session.SetString("errMsg", patientMemberResponse.message);
					return RedirectToAction("Index", "Home");
				}
				else
				{
					List<VMPatientMember> vMPatientMembers = JsonConvert.DeserializeObject<List<VMPatientMember>>(patientMemberResponse.data.ToString());
                    //vMPatientMember = JsonConvert.DeserializeObject<VMPatientMember>(response.data.ToString());
                    vMCombined.PatientMembers = vMPatientMembers;
                }

                ViewBag.DefaultPageSize = this.pageSize;
                ViewBag.OrderBy = vMPageParam.orderby;
                ViewBag.ShowData = vMPageParam.showData;
                ViewBag.Filter = vMPageParam.filter;
                ViewBag.UserRole = vMCombined.RoleModel;
                ViewBag.OrderDirection = vMPageParam.orderDirection;

                switch (vMPageParam.orderby)
                {
                    case "name":
                        vMCombined.PatientMembers = vMPageParam.orderDirection == "asc"
                            ? vMCombined.PatientMembers.OrderBy(product => product.Fullname).ToList()
                            : vMCombined.PatientMembers.OrderByDescending(product => product.Fullname).ToList();
                        break;
                    case "relation":
                        vMCombined.PatientMembers = vMPageParam.orderDirection == "asc"
                            ? vMCombined.PatientMembers.OrderBy(product => product.RelationName).ToList()
                            : vMCombined.PatientMembers.OrderByDescending(product => product.RelationName).ToList();
                        break;
                    case "age":
                        vMCombined.PatientMembers = vMPageParam.orderDirection == "asc"
                            ? vMCombined.PatientMembers.OrderBy(product => product.Age).ToList()
                            : vMCombined.PatientMembers.OrderByDescending(product => product.Age).ToList();
                        break;
                    case "appointment":
                        vMCombined.PatientMembers = vMPageParam.orderDirection == "asc"
                            ? vMCombined.PatientMembers.OrderBy(product => product.Appointment).ToList()
                            : vMCombined.PatientMembers.OrderByDescending(product => product.Appointment).ToList();
                        break;
                    case "chat":
                        vMCombined.PatientMembers = vMPageParam.orderDirection == "asc"
                            ? vMCombined.PatientMembers.OrderBy(product => product.chat).ToList()
                            : vMCombined.PatientMembers.OrderByDescending(product => product.chat).ToList();
                        break;
                    default:
                        vMCombined.PatientMembers = vMCombined.PatientMembers.OrderBy(productdata => productdata.memberId).ToList();
                        break;
                }

                return View(PageView<VMPatientMember>.CreateAsync(vMCombined.PatientMembers, vMPageParam.pageNumber ?? 1, showData));
            } catch (Exception ex)
            {
				HttpContext.Session.SetString("errMsg", ex.Message);
				return RedirectToAction("Index", "Home");
			}
        }

        public async Task<IActionResult> Add()
        {
            ViewBag.biodataId = HttpContext.Session.GetString("biodataId") ?? String.Empty;
            ViewBag.userId = HttpContext.Session.GetString("userId") ?? String.Empty;

            try
            {
                VMResponse bloodApi = JsonConvert.DeserializeObject<VMResponse?>(
                            await httpClient.GetStringAsync(apiUrl + "GroupBlood"));
                if (bloodApi != null)
                {
                    ViewBag.BloodGroup = (bloodApi.success || bloodApi.data != null) ?
                    JsonConvert.DeserializeObject<List<VMTblMBloodGroup?>>(
                        bloodApi.data.ToString()) : null;
                }
                else
                {
                    HttpContext.Session.SetString("errMsg", bloodApi.message);
                }

                VMResponse? RelationApi = JsonConvert.DeserializeObject<VMResponse?>(
                            await httpClient.GetStringAsync(apiUrl + "CustomerRelation"));
                if( RelationApi != null)
                {
                    ViewBag.Relation = (RelationApi.success || RelationApi.data != null) ?
                    JsonConvert.DeserializeObject<List<VMCustomerRelation?>>(
                        RelationApi.data.ToString()) : null;
                }
                else
                {
                    HttpContext.Session.SetString("errMsg", RelationApi.message);
                }

            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("errMsg", ex.Message);
            }
            return View();
        }

        [HttpPost]
        public async Task<VMResponse> Add(VMPatientMember data)
        {
            VMResponse apiResponse = new VMResponse();
            try
            {
                string jsonData = JsonConvert.SerializeObject(data);
                HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");
                apiResponse = JsonConvert.DeserializeObject<VMResponse>(await (await httpClient.PostAsync(apiUrl + "PatientMember", content)).Content.ReadAsStringAsync());

                //check api connection
                if (apiResponse == null)
                {
                    throw new ArgumentNullException("API cannot be reached!");
                }

                //check API RETURN DATA
                if (apiResponse.success || apiResponse.data != null)
                {
                    HttpContext.Session.SetString("infoMsg", apiResponse.message);

                }
                else
                {
                    throw new ArgumentNullException(apiResponse.message);

                }
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("errMsg", ex.Message);
            }

            return apiResponse;
        }

        public async Task<IActionResult> Edit(int id)
        {
            VMResponse response = new VMResponse();
            VMPatientMember vMPatientMember = new VMPatientMember();
            try
            {
                response = JsonConvert.DeserializeObject<VMResponse?>(
                    await httpClient.GetStringAsync(apiUrl + "PatientMember/GetEachPasienMember/" + id));
                if (response == null)
                {
                    throw new ArgumentException($"No Patient with ID = {id} found!");
                }
                if (!response.success || response.data == null)
                {
                    HttpContext.Session.SetString("errMsg", response.message);
                }
                else
                {
                    HttpContext.Session.SetString("infoMsg", $"Patient data with id = {id} successfully fetched!");
                    vMPatientMember = JsonConvert.DeserializeObject<VMPatientMember>(response.data.ToString());

                    VMResponse bloodApi = JsonConvert.DeserializeObject<VMResponse?>(
                            await httpClient.GetStringAsync(apiUrl + "GroupBlood"));
                    if (bloodApi == null)
                    {
                        HttpContext.Session.SetString("errMsg", "No Connection to Category / Variant API!");
                    }
                    ViewBag.BloodGroup = (bloodApi.success || bloodApi.data != null) ?
                    JsonConvert.DeserializeObject<List<VMTblMBloodGroup?>>(
                        bloodApi.data.ToString()) : null;
                }

                VMResponse? RelationApi = JsonConvert.DeserializeObject<VMResponse?>(
                            await httpClient.GetStringAsync(apiUrl + "CustomerRelation"));
                if (RelationApi != null)
                {
                    ViewBag.Relation = (RelationApi.success || RelationApi.data != null) ?
                    JsonConvert.DeserializeObject<List<VMCustomerRelation?>>(
                        RelationApi.data.ToString()) : null;
                }
                else
                {
                    HttpContext.Session.SetString("errMsg", RelationApi.message);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("errMsg", ex.Message);
            }
            return View(vMPatientMember);
        }

        [HttpPost]
        public async Task<VMResponse?> Edit (VMPatientMember member)
        {
            VMResponse? apiResponse;
            try
            {
                // Convert Form Data to Json
                string jsonData = JsonConvert.SerializeObject(member);

                // Wrapping JSon inside an HTML Body to be send as HTTP Put Request method 
                HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                // Process updated Product via API
                apiResponse = JsonConvert.DeserializeObject<VMResponse?>(
                    await (await httpClient.PutAsync($"{apiUrl}PatientMember/EditMember", content)).Content.ReadAsStringAsync()
                );

                // Check API Connection
                if (apiResponse == null)
                {
                    throw new ArgumentException("No Connection to Product API!");
                }

                // Check API Response
                if (!apiResponse.success || apiResponse.data == null)
                    throw new ArgumentException(apiResponse.message);
            }
            catch (Exception ex)
            {
                apiResponse = new VMResponse();
                apiResponse.success = false;
                apiResponse.message = ex.Message;

                HttpContext.Session.SetString("errMsg", ex.Message);
            }

            return apiResponse;
        }

        public async Task<IActionResult> Delete(int Id)
        {
            VMResponse? apiResponse;
            apiResponse = JsonConvert.DeserializeObject<VMResponse>(await httpClient.GetStringAsync(apiUrl + "PatientMember/GetEachPasienMember/" + Id));
            VMPatientMember relData = JsonConvert.DeserializeObject<VMPatientMember>(apiResponse.data.ToString());
            ViewBag.Title = "Delete Patient Data";

            return View(relData);
        }

        [HttpPost]
        public async Task<VMResponse> Delete(VMPatientMember data)
        {
            VMResponse? apiResponse = new VMResponse();
            try
            {
                apiResponse = JsonConvert.DeserializeObject<VMResponse>(await (
                    await httpClient.DeleteAsync(apiUrl + "PatientMember/Delete/"+ data.customerId)).Content.ReadAsStringAsync());

            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("errMsg", ex.Message);
            }
            HttpContext.Session.SetString("infoMsg", apiResponse.message);

            return apiResponse;
        }

        //      [HttpPost]
        //      public async Task<IActionResult> MultipleDelete(List<VMPatientMember> selectedIds)
        //      {
        //          List<VMPatientMember> selectedData = new List<VMPatientMember>();

        //          foreach (var id in selectedIds)
        //          {
        //              VMResponse apiResponse = JsonConvert.DeserializeObject<VMResponse>(
        //                  httpClient.GetStringAsync(apiUrl + "PatientMember/GetEachPasienMember/" + id).Result);
        //              VMPatientMember data = JsonConvert.DeserializeObject<VMPatientMember>(apiResponse.data.ToString());
        //              selectedData.Add(data);
        //          }
        //	// Mengirim daftar data untuk ditampilkan di View penghapusan
        //	return View("MultipleDelete", selectedData);
        //}

        [HttpPost]
        public async Task<VMResponse> MultipleDelete(List<VMPatientMember> selectedData)
        {
            VMResponse? apiResponse = new VMResponse();

            try
            {
                foreach (var data in selectedData)
                {
                    apiResponse = JsonConvert.DeserializeObject<VMResponse>(await (
                        await httpClient.DeleteAsync(apiUrl + "PatientMember/Delete/" + data.customerId)).Content.ReadAsStringAsync());
                }
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("errMsg", ex.Message);
            }

            HttpContext.Session.SetString("infoMsg", apiResponse.message);
            return apiResponse;
        }

    }
}
