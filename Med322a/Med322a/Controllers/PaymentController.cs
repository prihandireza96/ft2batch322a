﻿using Med322a.Models;
using Med322a.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Text;

namespace Med322a.Controllers
{
	public class PaymentController : Controller
	{
		private readonly string ApiUrl;
		private readonly HttpClient httpClient = new HttpClient();
		private readonly int pageSize;
		public PaymentController(IConfiguration _con)
		{
			ApiUrl = _con["ApiUrl"];
            pageSize = int.Parse(_con["PageSize"]);
            //return View();
        }
        public async Task<IActionResult> Index(VMPaging page)
        {
            VMResponse? apiResp = new VMResponse();
            List<VMMPaymentMethod> data = new List<VMMPaymentMethod>();
            try
            {
                if (page.filter == null)
                {
                    apiResp = JsonConvert.DeserializeObject<VMResponse>(await httpClient.GetStringAsync(ApiUrl + "Payment/GetAll"));
                }
                else
                {
                    apiResp = JsonConvert.DeserializeObject<VMResponse>(await httpClient.GetStringAsync(ApiUrl + "Payment/GetByFilter/" + page.filter));
                }
                if (apiResp == null)
                {
                    apiResp.message = "Api Connection Error";
                    apiResp.success = false;

                    return RedirectToAction("Index", "Payment");
                }
                if (apiResp.success || apiResp.data != null)
                {
                    data = JsonConvert.DeserializeObject<List<VMMPaymentMethod>>(apiResp.data.ToString());
                    ViewBag.Title = "Payment";

                    //Get OrderBy Value
                    ViewBag.OrderName = (page.orderby == "name") ? "name_desc" : "name";

                    ViewBag.OrderBy = page.orderby;
                    ViewBag.Filter = page.filter;

                    switch (page.orderby)
                    {
                        case "name":
                            data = data.OrderBy(payment => payment.Name).ToList();
                            break;
                        case "name_desc":
                            data = data.OrderByDescending(payment => payment.Name).ToList();
                            break;
                    }
                }
                else
                {
                    throw new ArgumentNullException(apiResp.message);
                }
                //return View(data);
                HttpContext.Session.SetString("infoMsg", apiResp.message);
            }
            catch (Exception ex)
            {
                apiResp = new VMResponse();
                apiResp.success = false;
                HttpContext.Session.SetString("errMsg", ex.Message);

                return RedirectToAction("Index", "Home");
            }
            return View(PaginationModel<VMMPaymentMethod>.CreateAsync(data, page.pageNumber ?? 1, pageSize));
        }
        public async Task<IActionResult> Add()
        {
            VMMPaymentMethod? data = new VMMPaymentMethod();
            ViewBag.Title = "Add New Payment Method";
            try
            {
                VMResponse? apiResp = JsonConvert.DeserializeObject<VMResponse>(await httpClient.GetStringAsync(ApiUrl + "Payment/GetAll/"));
                if (apiResp == null)
                {
                    throw new ArgumentException("No Connection");
                }
                if (apiResp.success || apiResp.data != null)
                {
                    HttpContext.Session.SetString("infoMsg", apiResp.message);
                    ViewBag.Bank = JsonConvert.DeserializeObject<List<VMMPaymentMethod>>(apiResp.data.ToString());
                }
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("errMsg", ex.Message);
                return RedirectToAction("Index", "Payment");
            }
            return View(data);
        }
        [HttpPost]
        public async Task<VMResponse?> Add(VMMPaymentMethod data)
        {
            VMResponse? apiResp = new VMResponse();
            try
            {
                string jsonData = JsonConvert.SerializeObject(data);
                HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                apiResp = JsonConvert.DeserializeObject<VMResponse>(await (await httpClient.PostAsync(ApiUrl + "Payment", content)).Content.ReadAsStringAsync());
                if (apiResp == null)
                {
                    throw new ArgumentException("Payment Api Can't Be Reached");
                }
                if (apiResp.success || apiResp.data != null)
                {
                    HttpContext.Session.SetString("infoMsg", apiResp.message);
                }
                else
                    throw new ArgumentNullException(apiResp.message);
            }
            catch (Exception ex)
            {
                apiResp = new VMResponse();
                HttpContext.Session.SetString("errMsg", ex.Message);
            }
            return apiResp;
        }
        public async Task<IActionResult> Update(long id)
        {
            VMMPaymentMethod? data = new VMMPaymentMethod();
            try
            {
                VMResponse? apiResp = JsonConvert.DeserializeObject<VMResponse>(await httpClient.GetStringAsync(ApiUrl + "Payment/Get/" + id));

                if (apiResp == null)
                {
                    throw new ArgumentException("No Connection");
                }

                if (!apiResp.success || apiResp.data == null)
                {
                    throw new ArgumentException("No Data");
                }
                else
                {
                    HttpContext.Session.SetString("infoMsg", apiResp.message);
                    data = JsonConvert.DeserializeObject<VMMPaymentMethod>(apiResp.data.ToString());
                }
                ViewBag.Title = "Edit Payment";
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("errMsg", ex.Message);
                return RedirectToAction("Index", "Payment");
            }
            return View(data);
        }
        [HttpPost]
        public async Task<VMResponse?> Update(VMMPaymentMethod data)
        {
            VMResponse? apiResp = new VMResponse();
            try
            {
                string jsonData = JsonConvert.SerializeObject(data);
                HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                apiResp = JsonConvert.DeserializeObject<VMResponse>(await (await httpClient.PutAsync(ApiUrl + "Payment", content)).Content.ReadAsStringAsync());
                if (apiResp == null)
                {
                    throw new ArgumentException("API Can't Be Reached");
                }
                if (apiResp.success || apiResp.data != null)
                {
                    HttpContext.Session.SetString("infoMsg", apiResp.message);
                }
                else
                {
                    throw new ArgumentException(apiResp.message);
                }
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("errMsg", ex.Message);
            }
            return apiResp;
        }
        public IActionResult Delete(long id)
        {
            ViewBag.Title = "Delete Payment";
            return View(id);
        }
        [HttpPost]
        public async Task<VMResponse?> Delete(VMMPaymentMethod data)
        {
            VMResponse? apiResp = new VMResponse();
            try
            {
                string jsonData = JsonConvert.SerializeObject(data);
                HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                apiResp = JsonConvert.DeserializeObject<VMResponse>(await (await httpClient.DeleteAsync($"{ApiUrl}Payment/{data.Id}/{data.DeletedBy}")).Content.ReadAsStringAsync());
            }
            catch (Exception ex)
            {
                HttpContext.Session.SetString("errMsg", ex.Message);
            }
            return apiResp;
        }
        [HttpPost]
        public async Task<VMResponse> Validasi(VMPaging page)
        {
            VMResponse? response = new VMResponse();
            try
            {
                response = JsonConvert.DeserializeObject<VMResponse?>(await httpClient.GetStringAsync(ApiUrl + $"Payment/GetName/{page.filter}"));

                if (response.data == null)
                {
                    throw new ArgumentException($"{page.filter} is exist!");
                }
                else
                {
                    response.success = true;
                    response.message = $"{page.filter} is not exist!";
                }
            }
            catch (Exception ex)
            {
                response.success = false;
                response.message = ex.Message;
            }
            return response;
        }
    }
}
