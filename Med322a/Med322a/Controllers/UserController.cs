﻿using Med322a.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Med322a.Controllers
{
    public class UserController : Controller
    {
        private readonly string apiUrl;
        private readonly HttpClient httpClient = new HttpClient();
        //HttpContext.Session.Clear();
        private readonly int pageSize;

        public UserController(IConfiguration _config)
        {
            apiUrl = _config["ApiUrl"];
            pageSize = int.Parse(_config["PageSize"]);
        }
        public async Task<IActionResult> UserAsync(long id)
        {
            VMResponse? apiResponse = JsonConvert.DeserializeObject<VMResponse>(
                await httpClient.GetStringAsync($"{apiUrl}User/Get/{id}"));
            try
            {
                // Check if API is null
                if (apiResponse == null)
                {
                    apiResponse.success = false;
                    apiResponse.message += "\nUser API Connection Error!";
                    HttpContext.Session.SetString("errMsg", apiResponse.message);
                    return RedirectToAction("Index", "Home");
                }

                if (!apiResponse.success || apiResponse.data == null)
                {
                    throw new ArgumentNullException(apiResponse.message);
                }
                else
                {
                    VMMUser? data = JsonConvert.DeserializeObject<VMMUser?>(apiResponse.data.ToString());
                    ViewBag.Title = "User";
                    return View(data);
                }
            }
            catch (Exception ex)
            {
                apiResponse.success = false;
                apiResponse.message += "\n" + ex.ToString();
                HttpContext.Session.SetString("errMsg", apiResponse.message);
                return RedirectToAction("Index", "Home");
            }
        }
        public IActionResult Index()
        {
            return View();
        }
    }
}
