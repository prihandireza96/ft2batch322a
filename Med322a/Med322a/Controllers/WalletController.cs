﻿using Med322a.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Med322a.Controllers
{
    public class WalletController : Controller
    {
        private readonly string ApiUrl;
        private readonly HttpClient httpClient = new HttpClient();
        private readonly int pageSize;
        public WalletController(IConfiguration _con)
        {
            ApiUrl = _con["ApiUrl"];
            pageSize = int.Parse(_con["PageSize"]);
            //return View();
        }
        public async Task<IActionResult> Index()
        {
            VMResponse? response = new VMResponse();
            List<VMMWalletDefaultNominal> data = new List<VMMWalletDefaultNominal>();
            try
            {
                response = JsonConvert.DeserializeObject<VMResponse>(await httpClient.GetStringAsync(ApiUrl + "Wallet/GetAll"));
                if (response == null)
                {
                    response.message = "Api Connection Error";
                    response.success = false;

                    return RedirectToAction("Index", "Wallet");
                }
                if(response.success  || response.data != null)
                {
                    ViewBag.Tarik = "Anda akan melakukan penarikan saldo sebesar : ";

                }
            }
            catch (Exception ex)
            {
                response.success = false;
                HttpContext.Session.SetString("errMsg", ex.Message);
            }
            return View();
            
        }
        public IActionResult Nominal()
        {
            ViewBag.Tarik = "Anda akan melakukan penarikan saldo sebesar : ";
            ViewBag.Title = "Isi Nominal Lain ";
            return View();
        }
        public IActionResult InputPIN()
        {
            return View();
        }
        public IActionResult Autentikasi()
        {
            return View();
        }
        public IActionResult OTP()
        {
            return View();
        }
        //public iactionresult index()
        //{
        //    return view();
        //}
    }
}
