﻿namespace Med322a.Models
{
	public class PaginationModel<T> : List<T>
	{
		public int PageIndex { get; private set; }
		public int TotalPages { get; private set; }
		public int TotalData { get; set; }

		public PaginationModel(List<T> data, int dataCount, int pageIndex, int pageSize)
		{
			PageIndex = pageIndex;
			TotalPages = (int)Math.Ceiling(dataCount / (double)pageSize);
			TotalData = data.Count;
			this.AddRange(data);
		}

		public bool HasPreviousPage => PageIndex > 1;
		public bool HasNextPage => PageIndex < TotalPages;

		public static PaginationModel<T> CreateAsync(List<T> dataSource, int pageIndex, int pageSize)
		{
			int dataCount = dataSource.Count;
			List<T> data = dataSource.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
			return new PaginationModel<T>(data, dataCount, pageIndex, pageSize);
		}
	}
}
