﻿namespace Med322a.Views.Models
{
    namespace Med322.Models
    {
        public class PageView<T> : List<T>
        {
            public int PageIndex { get; private set; }
            public int TotalPages { get; private set; }
            public int TotalData { get; set; }

            public PageView(List<T> data, int dataCount, int pageIndex, int pageSize)
            {
                PageIndex = pageIndex;
                TotalPages = (int)Math.Ceiling((double)dataCount / (double)pageSize);
                TotalData = dataCount;

                this.AddRange(data);
            }

            public bool HasPreviousPage => PageIndex > 1;
            public bool HasNextPage => PageIndex < TotalPages;

            public static PageView<T> CreateAsync(List<T> dataSource, int pageIndex, int pageSize)
            {
                int dataCount = dataSource.Count;
                List<T> data = dataSource.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();

                return new PageView<T>(data, dataCount, pageIndex, pageSize);

            }
        }
    }
}
